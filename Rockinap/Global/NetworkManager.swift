//
//  NetworkManager.swift
//  Rockinap
//
//  Created by ORION on 24/09/18.
//  Copyright © 2018 cjpl. All rights reserved.
//

import UIKit

class NetworkManager {
    
    
    // MARK: private composition methods
    
    static func post(request: NSMutableURLRequest, completion: @escaping (_ success: Bool, _ object: AnyObject?) -> ()) {
        dataTask(request: request, method: "POST", completion: completion)
    }
    
    static func put(request: NSMutableURLRequest, completion: @escaping (_ success: Bool, _ object: AnyObject?) -> ()) {
        dataTask(request: request, method: "PUT", completion: completion)
    }
    
    static func get(request: NSMutableURLRequest, completion: @escaping (_ success: Bool, _ object: AnyObject?) -> ()) {
        dataTask(request: request, method: "GET", completion: completion)
    }
    
    static func dataTask(request: NSMutableURLRequest, method: String, completion: @escaping (_ success: Bool, _ object: AnyObject?) -> ()) {
        request.httpMethod = method
        
        let session = URLSession(configuration: URLSessionConfiguration.default)
        
        
        
        session.dataTask(with: request as URLRequest) { (data, response, error) -> Void in
            if let data = data
            {
                _ = try? JSONSerialization.jsonObject(with: data, options: [])
                if let response = response as? HTTPURLResponse, 200...299 ~= response.statusCode {
                    completion(true, data as AnyObject)
                } else {
                    completion(false, data as AnyObject)
                }
            }
            }.resume()
    }
   /* static func getDataTask(request: NSMutableURLRequest, method: String, completion: @escaping (_ success: Bool, _ object: AnyObject?) -> ()) {
        
        //var request = URLRequest(url: URL(string: url)!)
        request.httpMethod = "GET"
        if let userInformation = UserDefaults.standard.dictionary(forKey: "userInformation") {
            request.setValue( userInformation["token"] as? String ?? "", forHTTPHeaderField:"x-access-token" )
        }
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        URLSession.dataTask(with: request) { (data, response, error) -> Void in
            do {
                _ = try? JSONSerialization.jsonObject(with: data, options: [])
                if let response = response as? HTTPURLResponse, 200...299 ~= response.statusCode {
                    completion(true, data as AnyObject)
                } else {
                    completion(false, data as AnyObject)
                }
            } catch {
                print("JSON Serialization error")
            }
        }.resume()
        
    }*/
    
    static func clientURLRequest(path: String, params: Dictionary<String, Any>? = nil) -> NSMutableURLRequest {
        //let mainURL = "https://api.rockinap.com/"
       let mainURL = "http://api.rockinap.site/"
        let request = NSMutableURLRequest(url: NSURL(string: mainURL + path)! as URL)
        
        if let params = params {
            
            let jsonData = try? JSONSerialization.data(withJSONObject: params)
            let jsonString = String.init(data: jsonData!, encoding: String.Encoding.utf8)
            print("url : %@",jsonString ?? "")
            if let userInformation = UserDefaults.standard.dictionary(forKey: "userInformation") {
                request.setValue( userInformation["token"] as? String ?? "", forHTTPHeaderField:"x-access-token" )
            }
            //request.timeoutInterval = 300
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            //let jsonDataBody = try? JSONSerialization.data(withJSONObject: jsonString as Any)
            if params.count != 0{
                request.httpBody = jsonData
            }
            
            
        }
        
        return request
    }
    
}
