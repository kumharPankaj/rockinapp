//
//  APIFunctions.swift
//  Rockinap
//
//  Created by ORION on 24/09/18.
//  Copyright © 2018 cjpl. All rights reserved.
//

import UIKit

let CATALOG_LIST_API = "product-list"
let LOGIN_API = "login"
let REGISTER_API = "signup"
let SOCIAL_LOGIN_API = "social-login"
let OUTLET_LIST_API = "organisations"
let OUTLET_DETAILS_API = "outlet-details"
let OFFERS_LIST_API = "offers"
let OUTLET_BRANCHES = "outlets"
let RESERVE_TABLE = "booking"
let INSTANT_OFFER_API = "get-instant-offer"
let BEACON_LIST_API = "beacons"
let QUIZ_QUES_LIST_API = "quiz-game"
let QUIZ_STATUS_API = "quiz-status"
let TH_STATUS_API = "treasurehunt-status"
let IN_PREMISE_OFFERS = "offers"
let TH_GAME = "treasurehunt-game"
let ALL_NOTIFICAITION = "notifications"
let REFRESH_TOKEN = "refresh-token"
let SEND_OTP_API = "resendotp"
let VERIFY_OTP_API = "otpverification"
let FORGOT_PASSWORD_API = "forgot-password"
let AWARDS_API = "awards"
let RESET_PWD = "reset-password"
let SETTINGS = "get-settings"
let LEAVE_OUTLET = "leave-outlet"
let MEASSAGE_HISTORY = "chat-history"
let CURRENT_USER = "current-users"
let CHAT_BUDDY = "chat-buddy"
let OFFER_HISTORY = "offer-history"
let LOGOUT = "logout"
class APIFunctions: NSObject {

    static func getCatalogList(dictParams: [String: Any], completion: @escaping (_ success: Bool, _ resObj: AnyObject) -> ())
    {

        NetworkManager.post(request: NetworkManager.clientURLRequest(path: CATALOG_LIST_API, params: dictParams )) { (success, object) -> () in
            DispatchQueue.main.async {
                if success {
                    var tempRrooms : ProductModel?
                    guard ((object as? Data) != nil) else {
                        completion(true, "Something went wrong!" as AnyObject)
                        return
                    }
                    do {
                        let jsonObjet = try JSONSerialization.jsonObject(with: object as! Data, options: .mutableContainers) as? [String:AnyObject]
                        let jsonDecoder = JSONDecoder()
                        tempRrooms = try jsonDecoder.decode(ProductModel.self, from: object as! Data)
                    }
                    catch {
                        
                    }
                    
                    completion(true, tempRrooms as AnyObject)
                } else {
                    //var message = "there was an error"
                    if let object = object, let _ = object["message"] as? String {
                        //message = passedMessage
                    }
                    completion(true, object!)
                }
            }
        }
    }
    static func registerUser(dictParams: [String: Any], completion: @escaping (_ success: Bool, _ resObj: AnyObject) -> ())
    {
        
        NetworkManager.post(request: NetworkManager.clientURLRequest(path: REGISTER_API, params: dictParams )) { (success, object) -> () in
            DispatchQueue.main.async {
                if success {
                    completion(true, object!)
                } else {
                    //var message = "there was an error"
                    if let object = object, let _ = object["message"] as? String {
                        //message = passedMessage
                    }
                    completion(true, object!)
                }
            }
        }
    }
    static func socialLogin(dictParams: [String: Any], completion: @escaping (_ success: Bool, _ resObj: AnyObject) -> ())
    {
        
        NetworkManager.post(request: NetworkManager.clientURLRequest(path: SOCIAL_LOGIN_API, params: dictParams )) { (success, object) -> () in
            DispatchQueue.main.async {
                if success {
                    completion(true, object!)
                } else {
                    //var message = "there was an error"
                    if let object = object, let _ = object["message"] as? String {
                        //message = passedMessage
                        
                    }
                    completion(true, object!)
                }
            }
        }
    }
    static func forgotPassword(dictParams: [String: Any], completion: @escaping (_ success: Bool, _ resObj: AnyObject) -> ())
    {
        
        NetworkManager.post(request: NetworkManager.clientURLRequest(path: FORGOT_PASSWORD_API, params: dictParams )) { (success, object) -> () in
            DispatchQueue.main.async {
                if success {
                    completion(true, object!)
                } else {
                    //var message = "there was an error"
                    if let object = object, let _ = object["message"] as? String {
                        //message = passedMessage
                        
                    }
                    completion(true, object!)
                }
            }
        }
    }
    static func verifyOTP(dictParams: [String: Any], completion: @escaping (_ success: Bool, _ resObj: AnyObject) -> ())
    {
        
        NetworkManager.post(request: NetworkManager.clientURLRequest(path: VERIFY_OTP_API, params: dictParams )) { (success, object) -> () in
            DispatchQueue.main.async {
                if success {
                    completion(true, object!)
                } else {
                    //var message = "there was an error"
                    if let object = object, let _ = object["message"] as? String {
                        //message = passedMessage
                        
                    }
                    completion(true, object!)
                }
            }
        }
    }
    static func resetPassword(dictParams: [String: Any], completion: @escaping (_ success: Bool, _ resObj: AnyObject) -> ())
    {
        
        NetworkManager.post(request: NetworkManager.clientURLRequest(path: RESET_PWD, params: dictParams )) { (success, object) -> () in
            DispatchQueue.main.async {
                if success {
                    completion(true, object!)
                } else {
                    //var message = "there was an error"
                    do {
                        let jsonObjet = try JSONSerialization.jsonObject(with: object as! Data, options: .mutableContainers) as? [String:AnyObject]
                        print(jsonObjet)
                    } catch {
                        
                    }
                    //var message = "there was an error"
                    if let object = object, let _ = object["message"] as? String {
                        //message = passedMessage
                        
                    }
                    completion(true, object!)
                }
            }
        }
    }
    static func sendOTP(dictParams: [String: Any], isFromForgotPwd: Bool = false, completion: @escaping (_ success: Bool, _ resObj: AnyObject) -> ())
    {
        var strUrl = SEND_OTP_API
        if isFromForgotPwd {
            strUrl = "forgot-password"
        }
        NetworkManager.post(request: NetworkManager.clientURLRequest(path: strUrl, params: dictParams )) { (success, object) -> () in
            DispatchQueue.main.async {
                if success {
                    completion(true, object!)
                } else {
                    //var message = "there was an error"
                    if let object = object, let _ = object["message"] as? String {
                        //message = passedMessage
                        
                    }
                    completion(true, object!)
                }
            }
        }
    }
    
    static func loginAPICall(dictParams: [String: Any], completion: @escaping (_ success: Bool, _ resObj: AnyObject) -> ())
    {
        
        NetworkManager.post(request: NetworkManager.clientURLRequest(path: LOGIN_API, params: dictParams )) { (success, object) -> () in
            DispatchQueue.main.async {
                if success {
                    completion(true, object!)
                } else {
                    do {
                    let jsonObjet = try JSONSerialization.jsonObject(with: object as! Data, options: .mutableContainers) as? [String:AnyObject]
                        print(jsonObjet)
                    } catch {
                        
                    }
                    //var message = "there was an error"
                    if let object = object, let _ = object["message"] as? String {
                        //message = passedMessage
                    }
                    completion(true, object!)
                }
            }
        }
    }
    static func refresNotificationToken(dictParams: [String: Any], completion: @escaping (_ success: Bool, _ resObj: AnyObject) -> ())
    {
        
        NetworkManager.post(request: NetworkManager.clientURLRequest(path: REFRESH_TOKEN, params: dictParams )) { (success, object) -> () in
            DispatchQueue.main.async {
                if success {
                    completion(true, object!)
                } else {
                    //var message = "there was an error"
                    if let object = object, let _ = object["message"] as? String {
                        //message = passedMessage
                    }
                    completion(true, object!)
                }
            }
        }
    }
    
    static func getOutletList(dictParams: [String: Any], completion: @escaping (_ success: Bool, _ resObj: AnyObject) -> ())
    {
        
        NetworkManager.post(request: NetworkManager.clientURLRequest(path: OUTLET_LIST_API, params: dictParams )) { (success, object) -> () in
            DispatchQueue.main.async {
                if success {
                    var tempRrooms : OutletModel?
                    guard ((object as? Data) != nil) else {
                        completion(true, "Something went wrong!" as AnyObject)
                        return
                    }
                    do {
                        let jsonDecoder = JSONDecoder()
                        tempRrooms = try jsonDecoder.decode(OutletModel.self, from: object as! Data)
                    }
                    catch {
                        
                    }
                    
                    completion(true, tempRrooms as AnyObject)
                } else {
                    var message = "there was an error"
                    if let object = object, let passedMessage = object["message"] as? String {
                        message = passedMessage
                    }
                    completion(true, message as AnyObject)
                }
            }
        }
    }
    
    static func getOutletDetails(dictParams: [String: Any], completion: @escaping (_ success: Bool, _ resObj: AnyObject) -> ())
    {
        
        NetworkManager.post(request: NetworkManager.clientURLRequest(path: OUTLET_DETAILS_API, params: dictParams )) { (success, object) -> () in
            DispatchQueue.main.async {
                if success {
                    completion(true, object!)
                } else {
                    //var message = "there was an error"
                    if let object = object, let _ = object["message"] as? String {
                        //message = passedMessage
                    }
                    completion(true, object!)
                }
            }
        }
    }
    static func getOffersList(dictParams: [String: Any], completion: @escaping (_ success: Bool, _ resObj: AnyObject) -> ())
    {
        
        NetworkManager.post(request: NetworkManager.clientURLRequest(path: OFFERS_LIST_API, params: dictParams )) { (success, object) -> () in
            DispatchQueue.main.async {
                if success {
                    
                    var tempRrooms : OffersModel?
                    guard ((object as? Data) != nil) else {
                        completion(true, "Something went wrong!" as AnyObject)
                        return
                    }
                    do {
                         let jsonObjet = try JSONSerialization.jsonObject(with: object as! Data, options: .mutableContainers) as? [String:AnyObject]
                        let jsonDecoder = JSONDecoder()
                        tempRrooms = try jsonDecoder.decode(OffersModel.self, from: object as! Data)
                    }
                    catch {
                        
                    }
                    
                    completion(true, tempRrooms as AnyObject)
                } else {
                    //var message = "there was an error"
                    do {
                    let jsonObjet = try JSONSerialization.jsonObject(with: object as! Data, options: .mutableContainers) as? [String:AnyObject]
                        print(jsonObjet)
                    } catch {
                        
                    }
                    if let object = object, let _ = object["message"] as? String {
                        //message = passedMessage
                    }
                    completion(true, object!)
                }
            }
        }
    }
    
    static func getBranchesList(dictParams: [String: Any], completion: @escaping (_ success: Bool, _ resObj: AnyObject) -> ())
    {
        //let request = NSMutableURLRequest(url: NSURL(string: mainURL + path)! as URL)
        
        var url : String = "https://test.rockinap.com/outlets?organisation_id=" + (dictParams[API_PARAM_KEYS.ORGANISATION_ID.rawValue] as? String ?? "1") + "&sortType=asc&limit=10"
        //var request : NSMutableURLRequest = NSMutableURLRequest()
        //request.url = NSURL(string: url) as URL?
        //request.httpMethod = "GET"
        
    
        
        /*NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue(), completionHandler:{ (response:URLResponse!, data: NSData!, error: NSError!) -> Void in
            var error: AutoreleasingUnsafeMutablePointer<NSError?> = nil
            let jsonResult: NSDictionary! = NSJSONSerialization.JSONObjectWithData(data, options:NSJSONReadingOptions.MutableContainers, error: error) as? NSDictionary
            
            if (jsonResult != nil) {
                // process jsonResult
            } else {
                // couldn't load JSON, look at error
            }
            
            
        })*/
        
        /*var request = URLRequest(url: URL(string: url)!)
        request.httpMethod = "GET"
        if let userInformation = UserDefaults.standard.dictionary(forKey: "userInformation") {
            request.setValue( userInformation["token"] as? String ?? "", forHTTPHeaderField:"x-access-token" )
        }
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        URLSession.shared.dataTask(with: request, completionHandler: { data, response, error -> Void in
            do {
                let jsonResult: NSDictionary! = try JSONSerialization.jsonObject(with: data!, options:JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary
//                let jsonDecoder = JSONDecoder()
//                let responseModel = try jsonDecoder.decode(CustomDtoClass.self, from: data!)
                print(jsonResult)
            } catch {
                print("JSON Serialization error")
            }
        }).resume()*/
        
        
        NetworkManager.get(request: NetworkManager.clientURLRequest(path: OUTLET_BRANCHES + "?organisation_id=" + (dictParams[API_PARAM_KEYS.ORGANISATION_ID.rawValue] as? String ?? "1") + "&sortType=asc&limit=10&lat=\(dictParams["lat"] ?? "")&lon=\(dictParams["lon"] ?? "")", params: [:] )) { (success, object) -> () in
            DispatchQueue.main.async {
                if success {
                    var tempRrooms : BrancheModel?
                    guard ((object as? Data) != nil) else {
                        completion(true, "Something went wrong!" as AnyObject)
                        return
                    }
                    do {
                        let jsonObjet = try JSONSerialization.jsonObject(with: object as! Data, options: .mutableContainers) as? [String:AnyObject]
                        let jsonDecoder = JSONDecoder()
                        tempRrooms = try jsonDecoder.decode(BrancheModel.self, from: object as! Data)
                    }
                    catch {
                        
                    }
                    
                    completion(true, tempRrooms as AnyObject)
                } else {
                    var tempError : ErrorModel?
                    guard ((object as? Data) != nil) else {
                        completion(true, "Something went wrong!" as AnyObject)
                        return
                    }
                    do {
                        let jsonDecoder = JSONDecoder()
                        tempError = try jsonDecoder.decode(ErrorModel.self, from: object as! Data)
                    }
                    catch {
                        
                    }
                    
                    completion(true, tempError as AnyObject)
                }
            }
        }
    }
    
    static func bookATable(dictParams: [String: Any], completion: @escaping (_ success: Bool, _ resObjs: AnyObject) -> ()){
        NetworkManager.post(request: NetworkManager.clientURLRequest(path: RESERVE_TABLE, params: dictParams )) { (success, object) -> () in
            DispatchQueue.main.async {
                if success {
                    completion(true, object!)
                } else {
                    //var message = "there was an error"
                    if let object = object, let _ = object["message"] as? String {
                        //message = passedMessage
                    }
                    completion(true, object!)
                }
            }
        }
    }
    
    static func callInstantOfferAPI(dictParams: [String: Any], completion: @escaping (_ success: Bool, _ resObjs: AnyObject) -> ()){
        NetworkManager.post(request: NetworkManager.clientURLRequest(path: INSTANT_OFFER_API, params: dictParams )) { (success, object) -> () in
            DispatchQueue.main.async {
                if success {
                    completion(true, object!)
                } else {
                    //var message = "there was an error"
                    if let object = object, let _ = object["message"] as? String {
                        //message = passedMessage
                    }
                    completion(true, object!)
                }
            }
        }
    }
    
    static func offerListAPI_Call(dictParams: [String: Any], completion: @escaping (_ success: Bool, _ resObjs: AnyObject) -> ()){
        NetworkManager.post(request: NetworkManager.clientURLRequest(path: OFFERS_LIST_API, params: dictParams )) { (success, object) -> () in
            DispatchQueue.main.async {
                if success {
                    completion(true, object!)
                } else {
                    //var message = "there was an error"
                    if let object = object, let _ = object["message"] as? String {
                        //message = passedMessage
                    }
                    completion(true, object!)
                }
            }
        }
    }
    //MARK:- Beacon List
    static func beaconListAPI_Call(dictParams: [String: Any], completion: @escaping (_ success: Bool, _ resObjs: AnyObject) -> ()){
        NetworkManager.post(request: NetworkManager.clientURLRequest(path: BEACON_LIST_API, params: dictParams )) { (success, object) -> () in
            DispatchQueue.main.async {
                if success {
                    guard let dictData = object as? Data else
                    {
                        return
                    }
                    
                    do {
                        let jsonObjet = try JSONSerialization.jsonObject(with: dictData, options: .mutableContainers) as? [String:AnyObject]
                        let arrBeacons = jsonObjet?["data"] as! [[String : Any]]
                    var beaconList = [Item]()
                       
                        arrBeacons.forEach({ (objBeacon) in
                            let strUUID : String = objBeacon["beacon_uuid"] as! String
                            /*strUUID.insert("-", at: strUUID.index(strUUID.startIndex, offsetBy: +8))
                            strUUID.insert("-", at: strUUID.index(strUUID.startIndex, offsetBy: +13))
                            strUUID.insert("-", at: strUUID.index(strUUID.startIndex, offsetBy: +18))
                            strUUID.insert("-", at: strUUID.index(strUUID.startIndex, offsetBy: +23))*/
                            if strUUID.length == 36{
                                let minor = UInt8((objBeacon["minor"] as? String ?? "0000"), radix: 16) ?? 0
                                let major = UInt8((objBeacon["major"] as? String ?? "0000"), radix: 16) ?? 0
                                let beacon = Item.init(name: objBeacon["name"] as! String, icon: 1, uuid: UUID(uuidString: strUUID)! , majorValue:major , minorValue:minor )
                                beaconList.append(beacon)
                            }
                            
                        })
                        completion(true, beaconList as AnyObject)
                    } catch {
                        
                    }
                } else {
                    do {
                        let jsonObjet = try JSONSerialization.jsonObject(with: object as! Data, options: .mutableContainers) as? [String:AnyObject]
                        let jsonDecoder = JSONDecoder()
                        print(jsonObjet)
                    }
                    catch {
                        
                    }
                    //var message = "there was an error"
                    if let object = object, let _ = object["message"] as? String {
                        //message = passedMessage
                    }
                    completion(true, object!)
                }
            }
        }
    }
    
    static func quizQuesList_Call(dictParams: [String: Any], completion: @escaping (_ success: Bool, _ resObjs: AnyObject) -> ()){
        NetworkManager.post(request: NetworkManager.clientURLRequest(path: QUIZ_QUES_LIST_API, params: dictParams )) { (success, object) -> () in
            DispatchQueue.main.async {
                if success {
                    
                    var tempRrooms : QuizQuessModel?
                    guard ((object as? Data) != nil) else {
                        completion(true, "Something went wrong!" as AnyObject)
                        return
                    }
                    do {
                         let jsonObjet = try JSONSerialization.jsonObject(with: object as! Data, options: .mutableContainers) as? [String:AnyObject]
                        let jsonDecoder = JSONDecoder()
                        tempRrooms = try jsonDecoder.decode(QuizQuessModel.self, from: object as! Data)
                    }
                    catch {
                        
                    }
                    
                    completion(true, tempRrooms as AnyObject)
                } else {
                    do {
                        let jsonObjet = try JSONSerialization.jsonObject(with: object as! Data, options: .mutableContainers) as? [String:AnyObject]
                        print(jsonObjet)
                    }
                    catch {
                        
                    }
                    //var message = "there was an error"
                    if let object = object, let _ = object["message"] as? String {
                        //message = passedMessage
                    }
                    completion(true, object!)
                }
            }
        }
    }
    static func quizStatus_Call(dictParams: [String: Any], completion: @escaping (_ success: Bool, _ resObjs: AnyObject) -> ()){
        NetworkManager.post(request: NetworkManager.clientURLRequest(path: QUIZ_STATUS_API, params: dictParams )) { (success, object) -> () in
            DispatchQueue.main.async {
                if success {
                    
                    var dictResponse : [String: Any]?
                    guard ((object as? Data) != nil) else {
                        completion(true, "Something went wrong!" as AnyObject)
                        return
                    }
                    do {
                        dictResponse = try JSONSerialization.jsonObject(with: object as! Data, options: .mutableContainers) as? [String:AnyObject]
                       
                    }
                    catch {
                        
                    }
                    
                    completion(true, dictResponse as AnyObject)
                } else {
                    var dictResponse : [String: Any]?
                    do {
                        dictResponse = try JSONSerialization.jsonObject(with: object as! Data, options: .mutableContainers) as? [String:AnyObject]
                        
                    }
                    catch {
                        
                    }
                    if let object = object, let _ = object["message"] as? String {
                        //message = passedMessage
                    }
                    completion(false, dictResponse as AnyObject)
                }
            }
            
        }
    }
    
    static func thStatus_Call(dictParams: [String: Any], completion: @escaping (_ success: Bool, _ resObjs: AnyObject) -> ()){
        NetworkManager.post(request: NetworkManager.clientURLRequest(path: TH_STATUS_API, params: dictParams )) { (success, object) -> () in
            DispatchQueue.main.async {
                if success {
                    
                    var dictResponse : [String: Any]?
                    guard ((object as? Data) != nil) else {
                        completion(true, "Something went wrong!" as AnyObject)
                        return
                    }
                    do {
                        dictResponse = try JSONSerialization.jsonObject(with: object as! Data, options: .mutableContainers) as? [String:AnyObject]
                        
                    }
                    catch {
                        
                    }
                    
                    completion(true, dictResponse as AnyObject)
                } else {
                    var dictResponse : [String: Any]?
                    do {
                        dictResponse = try JSONSerialization.jsonObject(with: object as! Data, options: .mutableContainers) as? [String:AnyObject]
                        
                    }
                    catch {
                        
                    }
                    if let object = object, let _ = object["message"] as? String {
                        //message = passedMessage
                    }
                    completion(false, dictResponse as AnyObject)
                }
            }
            
        }
    }
    
    static func inPremiseUserLis_Call(dictParams: [String: Any], completion: @escaping (_ success: Bool, _ resObjs: AnyObject) -> ()){
        NetworkManager.post(request: NetworkManager.clientURLRequest(path: CURRENT_USER, params: dictParams )) { (success, object) -> () in
            DispatchQueue.main.async {
                if success {
                    
                    var tempRrooms : QuizQuessModel?
                    guard ((object as? Data) != nil) else {
                        completion(true, "Something went wrong!" as AnyObject)
                        return
                    }
                    do {
                        
                        let jsonObjet = try JSONSerialization.jsonObject(with: object as! Data, options: .mutableContainers) as? [String:AnyObject]
                        let jsonDecoder = JSONDecoder()
                        tempRrooms = try jsonDecoder.decode(QuizQuessModel.self, from: object as! Data)
                    }
                    catch {
                        
                    }
                    
                    completion(true, tempRrooms as AnyObject)
                } else {
                    //var message = "there was an error"
                    if let object = object, let _ = object["message"] as? String {
                        //message = passedMessage
                    }
                    completion(true, object!)
                }
            }
            
        }
    }
    
    static func inPremiseOffers(dictParams: [String: Any], completion: @escaping (_ success: Bool, _ resObjs: AnyObject) -> ()){
        NetworkManager.post(request: NetworkManager.clientURLRequest(path: CURRENT_USER, params: dictParams )) { (success, object) -> () in
            DispatchQueue.main.async {
                if success {
                    
                    var tempRrooms : QuizQuessModel?
                    guard ((object as? Data) != nil) else {
                        completion(true, "Something went wrong!" as AnyObject)
                        return
                    }
                    do {
                        let jsonObjet = try JSONSerialization.jsonObject(with: object as! Data, options: .mutableContainers) as? [String:AnyObject]
                        let jsonDecoder = JSONDecoder()
                        tempRrooms = try jsonDecoder.decode(QuizQuessModel.self, from: object as! Data)
                    }
                    catch {
                        
                    }
                    
                    completion(true, tempRrooms as AnyObject)
                } else {
                    //var message = "there was an error"
                    if let object = object, let _ = object["message"] as? String {
                        //message = passedMessage
                    }
                    completion(true, object!)
                }
            }
            
        }
    }
    static func getTHGameWithBeaconDetails(dictParams: [String: Any], completion: @escaping (_ success: Bool, _ resObjs: AnyObject) -> ()){
        NetworkManager.post(request: NetworkManager.clientURLRequest(path: TH_GAME, params: dictParams )) { (success, object) -> () in
            DispatchQueue.main.async {
                if success {
                    
                    var tempRrooms : THModel?
                    guard ((object as? Data) != nil) else {
                        completion(true, "Something went wrong!" as AnyObject)
                        return
                    }
                    do {
                        let jsonObjet = try JSONSerialization.jsonObject(with: object as! Data, options: .mutableContainers) as? [String:AnyObject]
                        let jsonDecoder = JSONDecoder()
                        tempRrooms = try jsonDecoder.decode(THModel.self, from: object as! Data)
                    }
                    catch {
                        
                    }
                    
                    completion(true, tempRrooms as AnyObject)
                } else {
                    //var message = "there was an error"
                    if let object = object, let _ = object["message"] as? String {
                        //message = passedMessage
                    }
                    completion(true, object!)
                }
            }
            
        }
    }
    
    static func getAllNotification(dictParams: [String: Any], completion: @escaping (_ success: Bool, _ resObjs: AnyObject) -> ()){
        NetworkManager.post(request: NetworkManager.clientURLRequest(path: ALL_NOTIFICAITION, params: dictParams )) { (success, object) -> () in
            DispatchQueue.main.async {
                if success {
                    
                    var tempRrooms : NotificationModel?
                    guard ((object as? Data) != nil) else {
                        completion(true, "Something went wrong!" as AnyObject)
                        return
                    }
                    do {
                        let jsonObjet = try JSONSerialization.jsonObject(with: object as! Data, options: .mutableContainers) as? [String:AnyObject]
                        let jsonDecoder = JSONDecoder()
                        tempRrooms = try jsonDecoder.decode(NotificationModel.self, from: object as! Data)
                    }
                    catch {
                        
                    }
                    
                    completion(true, tempRrooms as AnyObject)
                } else {
                    do {
                        let jsonObjet = try JSONSerialization.jsonObject(with: object as! Data, options: .mutableContainers) as? [String:AnyObject]
                        let jsonDecoder = JSONDecoder()
                    print(jsonObjet)
                    }
                    catch {
                        
                    }
                    //var message = "there was an error"
                    if let object = object, let _ = object["message"] as? String {
                        //message = passedMessage
                    }
                    completion(true, object!)
                }
            }
            
        }
    }
    
    static func getAwardsList( completion: @escaping (_ success: Bool, _ resObj: AnyObject) -> ())
    {
        
        NetworkManager.post(request: NetworkManager.clientURLRequest(path: AWARDS_API, params: [:] )) { (success, object) -> () in
            DispatchQueue.main.async {
                if success {
                    var tempRrooms : AwardsModel?
                    guard ((object as? Data) != nil) else {
                        completion(true, "Something went wrong!" as AnyObject)
                        return
                    }
                    do {
                        let jsonObjet = try JSONSerialization.jsonObject(with: object as! Data, options: .mutableContainers) as? [String:AnyObject]
                        let jsonDecoder = JSONDecoder()
                        tempRrooms = try jsonDecoder.decode(AwardsModel.self, from: object as! Data)
                    }
                    catch {
                        
                    }
                    
                    completion(true, tempRrooms as AnyObject)
                } else {
                    //var message = "there was an error"
                    if let object = object, let _ = object["message"] as? String {
                        //message = passedMessage
                    }
                    completion(true, object!)
                }
            }
        }
    }
    static func getSettings(dictParams: [String: Any], completion: @escaping (_ success: Bool, _ resObjs: AnyObject) -> ()){
        NetworkManager.post(request: NetworkManager.clientURLRequest(path: SETTINGS, params: dictParams )) { (success, object) -> () in
            DispatchQueue.main.async {
                if success {
                    
                    var tempRrooms : SettingsModel?
                    guard ((object as? Data) != nil) else {
                        completion(true, "Something went wrong!" as AnyObject)
                        return
                    }
                    do {
                        
                        let jsonDecoder = JSONDecoder()
                        tempRrooms = try jsonDecoder.decode(SettingsModel.self, from: object as! Data)
                    }
                    catch {
                        
                    }
                    
                    completion(true, tempRrooms as AnyObject)
                } else {
                    do {
                        let jsonObjet = try JSONSerialization.jsonObject(with: object as! Data, options: .mutableContainers) as? [String:AnyObject]
                        let jsonDecoder = JSONDecoder()
                        print(jsonObjet)
                    }
                    catch {
                        
                    }
                    //var message = "there was an error"
                    if let object = object, let _ = object["message"] as? String {
                        //message = passedMessage
                    }
                    completion(true, object!)
                }
            }
            
        }
    }
    static func leaveOutlet(dictParams: [String: Any], completion: @escaping (_ success: Bool, _ resObjs: AnyObject) -> ()){
        NetworkManager.post(request: NetworkManager.clientURLRequest(path: LEAVE_OUTLET, params: dictParams )) { (success, object) -> () in
            DispatchQueue.main.async {
                if success {
                    completion(true, object!)
                } else {
                    do {
                        let jsonObjet = try JSONSerialization.jsonObject(with: object as! Data, options: .mutableContainers) as? [String:AnyObject]
                        let jsonDecoder = JSONDecoder()
                        print(jsonObjet)
                    }
                    catch {
                        
                    }
                    //var message = "there was an error"
                    if let object = object, let _ = object["message"] as? String {
                        //message = passedMessage
                    }
                    completion(true, object!)
                }
            }
        }
    }
    static func getHistoryMsgs(dictParams: [String: Any], completion: @escaping (_ success: Bool, _ resObjs: AnyObject) -> ()){
        NetworkManager.post(request: NetworkManager.clientURLRequest(path: MEASSAGE_HISTORY, params: dictParams )) { (success, object) -> () in
            DispatchQueue.main.async {
                if success {
                    var tempRrooms : [String: AnyObject]?
                    guard ((object as? Data) != nil) else {
                        completion(true, "Something went wrong!" as AnyObject)
                        return
                    }
                    do {
                        tempRrooms = try JSONSerialization.jsonObject(with: object as! Data, options: .mutableContainers) as? [String:AnyObject]
                        
                    }
                    catch {
                        
                    }
                    
                    completion(true, tempRrooms as AnyObject)
                } else {
                   
                    //var message = "there was an error"
                    if let object = object, let _ = object["message"] as? String {
                        //message = passedMessage
                    }
                    completion(true, object!)
                }
            }
        }
    }
    static func addInCurrentUser(dictParams: [String: Any], completion: @escaping (_ success: Bool, _ resObjs: AnyObject) -> ()){
        NetworkManager.post(request: NetworkManager.clientURLRequest(path: CURRENT_USER, params: dictParams )) { (success, object) -> () in
            DispatchQueue.main.async {
                if success {
                    var tempRrooms : [String: AnyObject]?
                    guard ((object as? Data) != nil) else {
                        completion(true, "Something went wrong!" as AnyObject)
                        return
                    }
                    do {
                        tempRrooms = try JSONSerialization.jsonObject(with: object as! Data, options: .mutableContainers) as? [String:AnyObject]
                        
                    }
                    catch {
                        
                    }
                    
                    completion(true, tempRrooms as AnyObject)
                } else {
                    var tempRrooms : [String: AnyObject]?
                    do {
                        tempRrooms = try JSONSerialization.jsonObject(with: object as! Data, options: .mutableContainers) as? [String:AnyObject]
                        
                    }
                    catch {
                        
                    }
                    //var message = "there was an error"
                    if let object = object, let _ = object["message"] as? String {
                        //message = passedMessage
                    }
                    completion(true, object!)
                }
            }
        }
    }
    
    static func updateOfferHistory(dictParams: [String: Any], completion: @escaping (_ success: Bool, _ resObjs: AnyObject) -> ()){
        NetworkManager.post(request: NetworkManager.clientURLRequest(path: OFFER_HISTORY, params: dictParams )) { (success, object) -> () in
            DispatchQueue.main.async {
                if success {
                    var tempRrooms : [String: AnyObject]?
                    guard ((object as? Data) != nil) else {
                        completion(true, "Something went wrong!" as AnyObject)
                        return
                    }
                    do {
                        tempRrooms = try JSONSerialization.jsonObject(with: object as! Data, options: .mutableContainers) as? [String:AnyObject]
                        
                    }
                    catch {
                        
                    }
                    
                    completion(true, tempRrooms as AnyObject)
                } else {
                    
                    //var message = "there was an error"
                    do {
                        let tempRrooms = try JSONSerialization.jsonObject(with: object as! Data, options: .mutableContainers) as? [String:AnyObject]
                        print("")
                    }
                    catch {
                        
                    }
                    if let object = object, let _ = object["message"] as? String {
                        //message = passedMessage
                    }
                    completion(true, object!)
                }
            }
        }
    }
    static func chatBuddyAPICall(dictParams: [String: Any], completion: @escaping (_ success: Bool, _ resObjs: AnyObject) -> ()){
        NetworkManager.post(request: NetworkManager.clientURLRequest(path: CHAT_BUDDY, params: dictParams )) { (success, object) -> () in
            DispatchQueue.main.async {
                if success {
                    var tempRrooms : [String: AnyObject]?
                    guard ((object as? Data) != nil) else {
                        completion(true, "Something went wrong!" as AnyObject)
                        return
                    }
                    do {
                        tempRrooms = try JSONSerialization.jsonObject(with: object as! Data, options: .mutableContainers) as? [String:AnyObject]
                        
                    }
                    catch {
                        
                    }
                    
                    completion(true, tempRrooms as AnyObject)
                } else {
                    
                    //var message = "there was an error"
                    do {
                        let tempRrooms = try JSONSerialization.jsonObject(with: object as! Data, options: .mutableContainers) as? [String:AnyObject]
                        print("")
                    }
                    catch {
                        
                    }
                    if let object = object, let _ = object["message"] as? String {
                        //message = passedMessage
                    }
                    completion(true, object!)
                }
            }
        }
    }
    static func logoutAPICall(dictParams: [String: Any], completion: @escaping (_ success: Bool, _ resObjs: AnyObject) -> ()){
        NetworkManager.post(request: NetworkManager.clientURLRequest(path: LOGOUT, params: dictParams )) { (success, object) -> () in
            DispatchQueue.main.async {
                if success {
                    var tempRrooms : [String: AnyObject]?
                    guard ((object as? Data) != nil) else {
                        completion(true, "Something went wrong!" as AnyObject)
                        return
                    }
                    do {
                        tempRrooms = try JSONSerialization.jsonObject(with: object as! Data, options: .mutableContainers) as? [String:AnyObject]
                        
                    }
                    catch {
                        
                    }
                    
                    completion(true, tempRrooms as AnyObject)
                } else {
                    
                    //var message = "there was an error"
                    do {
                        let tempRrooms = try JSONSerialization.jsonObject(with: object as! Data, options: .mutableContainers) as? [String:AnyObject]
                        print("")
                    }
                    catch {
                        
                    }
                    if let object = object, let _ = object["message"] as? String {
                        //message = passedMessage
                    }
                    completion(true, object!)
                }
            }
        }
    }
}
