//
//  SocketIOManager.swift
//  Rockinap
//
//  Created by Pankaj Kumhar on 2/17/19.
//  Copyright © 2019 funndynamix. All rights reserved.
//

import UIKit
import SocketIO
class SocketIOManager: NSObject {
static let sharedInstance = SocketIOManager()
    
    let manager = SocketManager(socketURL: URL(string: "http://api.rockinap.site")!, config: [.log(true), .compress])
//   let manager = SocketManager(socketURL: URL(string: "https://api.rockinap.com")!, config: [.log(true), .compress])
   // var socket: SocketIOClient = SocketIOClient(manager: NSURL(string: "https://api.rockinap.com")! as! SocketManagerSpec, nsp: "")
    override init() {
        super.init()
    }
    
    func establishConnection() {
        manager.defaultSocket.connect()
    }
    
    
    func closeConnection() {
        manager.defaultSocket.disconnect()
    }
    func connectToServerWithUserData(userData: [String: Any], completionHandler: @escaping (_ userList: [[String: AnyObject]]?) -> Void) {
        manager.defaultSocket.emit("add_user", userData)
        manager.defaultSocket.on("current_users") { ( dataArray, ack) -> Void in
            
            completionHandler(dataArray[0] as? [[String: AnyObject]])
        }
    }
    func exitChatWithNickname(nickname: String, completionHandler: () -> Void) {
        manager.defaultSocket.emit("exitUser", nickname)
        completionHandler()
    }
    
    func sendMessage(messageBody: [String: Any]) {
        manager.defaultSocket.emit("user_message", messageBody)
    }
    func receiveMsgAck(messageBody: [String: Any]) {
        manager.defaultSocket.emit("ack", messageBody)
    }
    func backPressed(messageBody: [String: Any]) {
        manager.defaultSocket.emit("back_unread_check", messageBody)
    }
    func getChatMessage(completionHandler: @escaping (_ messageInfo: [String: AnyObject]) -> Void){
        manager.defaultSocket.on("message") { (dataArray, socketAck) in
            completionHandler(dataArray[0] as! [String : AnyObject])
        }
    }
    func emitChatHistory(userData: [String: Any])  {
        manager.defaultSocket.emit("chat_history", userData)
    }
    func getChatHistory(completionHandler: @escaping (_ messageInfo: [[String: AnyObject]]) -> Void){
        
        manager.defaultSocket.on("chat_history") { (dataArray, socketAck) in
            if let dict = dataArray[0] as? [[String : AnyObject]] {
            completionHandler(dict)
            }
        }
    }
    func getUnreadChatMessage(completionHandler: @escaping (_ messageInfo: [String: AnyObject]) -> Void){
        manager.defaultSocket.on("unread_message") { (dataArray, socketAck) in
            completionHandler(dataArray[0] as! [String : AnyObject])
        }
    }
    
    func sendStartTypingMessage(nickname: String) {
        manager.defaultSocket.emit("typing", nickname)
    }
    
    func sendStopTypingMessage(nickname: String) {
        manager.defaultSocket.emit("typing_end", nickname)
    }

}
