//
//  Functions.swift
//  KittyParty
//
//  Created by Payal Patel on 14/07/14.
//  Copyright (c) 2014 Payal Patel. All rights reserved.
//

import UIKit

enum UIUserInterfaceIdiom : Int
{
    case Unspecified
    case Phone
    case Pad
}

struct ScreenSize
{
    static let SCREEN_WIDTH         = UIScreen.main.bounds.size.width
    static let SCREEN_HEIGHT        = UIScreen.main.bounds.size.height
    static let SCREEN_MAX_LENGTH    = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    static let SCREEN_MIN_LENGTH    = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
}

struct AcceptedCharacters {
    static let NUMBERS = "0123456789"
}

struct DeviceType
{
    static let IS_IPHONE_4_OR_LESS  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
    static let IS_IPHONE_5          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
    static let IS_IPHONE_6          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
    static let IS_IPHONE_6P         = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
    static let IS_IPAD              = UIDevice.current.userInterfaceIdiom == .pad && ScreenSize.SCREEN_MAX_LENGTH == 1024.0
    static let IS_IPAD_PRO          = UIDevice.current.userInterfaceIdiom == .pad && ScreenSize.SCREEN_MAX_LENGTH == 1366.0
}
//let synth = AVSpeechSynthesizer()

class Functions: NSObject {
    
//        override init() {
//        super.init()
//        synth.delegate = self
//    }
    
//    class func speak(string: String) {
//        let utterance = AVSpeechUtterance(string: string)
//        synth.speak(utterance)
//    }

    class func Object_TO_Dict(_ myObj:AnyObject)->NSMutableDictionary{
        let dic: NSMutableDictionary = NSMutableDictionary()
        var outCount:CUnsignedInt = 0
        let properties:UnsafeMutablePointer<objc_property_t>! = class_copyPropertyList(myObj.classForCoder, &outCount)
        for i in 0...Int(outCount){
            let property:objc_property_t = properties[i]
            let propName : NSString =  NSString(cString: property_getName(property), encoding: String.Encoding.utf8.rawValue)!
            //let obj:AnyObject = myObj.value(forKey: propName as String)!
            let obj:AnyObject = myObj.object(forKey:propName)!
            if (obj as? String != nil){
                dic.setValue(obj, forKey: propName as String)
            }
            //obj = nil
        }
        free(properties)
        return dic
    }
    
    class func Dict_TO_Object(_ dbData:NSDictionary, myObj:AnyObject)->AnyObject{
        let ob2:AnyObject=myObj
        let dataKeys:NSArray = dbData.allKeys as NSArray
        for i in 0...(dbData.count - 1) {
            //print("\(dbData.value(forKey: dataKeys.object(at: i) as! String))")
            /*if(Foundation.NSNotFound != dbData.valueForKey(dataKeys.objectAtIndex(i) as String) as NSObject){
             var obj : AnyObject = dbData.valueForKey(dataKeys.objectAtIndex(i) as String)
             if(obj as NSObject == NSNull()){
             obj = ""
             }
             ob2.setValue(obj, forKey:dataKeys.objectAtIndex(i) as String)
             println("ob2 is \(ob2)")
             }*/
            // if(ob2.hash(dataKeys.objectAtIndex(i) as String)){
            var obj : AnyObject = dbData.value(forKey: dataKeys.object(at: i) as! String)! as AnyObject
            print("obj is \(obj)" + "Key is \(dataKeys.object(at: i) as! String)")
            if(obj as! NSObject == NSNull()){
                obj = "" as AnyObject
            }
            
            if (dbData.value(forKey: dataKeys.object(at: i) as! String) is NSNumber) {
                if (obj is Double) {
                    //ob2.setValue((obj as! NSNumber).doubleValue, forKey:dataKeys.object(at: i) as! String)
                    ob2.set(obj, forKey: dataKeys.object(at: i) as! String)
                    
                }
                
            }else{
                ob2.setValue(obj, forKey:dataKeys.object(at: i) as! String)
            }
            
            
            //print("ob2 is \(ob2)")
            //}
            
        }
        return ob2
    }
    
    class func IsiPhoneDevice() -> Bool{
        var iPhone: Bool
        if UIDevice.current.userInterfaceIdiom == .phone{
            iPhone = true}
        else{
            iPhone = false}
        return iPhone
    }
    
    class func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    class func emailValidation(_ email:String)->Bool{
        
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let resultPredicate = NSPredicate(format: "SELF MATCHES %@", emailRegex)
        return (resultPredicate.evaluate(with: email))
    }
    
    class func phoneNumberValidate(_ cont_no:String)->Bool{
        
        let phoneRegex = "^[0-9]{2}[0-9]{8,15}$"  //[235689][0-9]{6}([0-9]{3})? //^((\+)|(00))[0-9]{6,14}$
        let resultPredicate = NSPredicate(format: "SELF MATCHES %@", phoneRegex)
        return (resultPredicate.evaluate(with: cont_no))
    }
    
    class func uiqueId()->String{
        let identifierForVendor = UIDevice.current.identifierForVendor!.uuidString
        return identifierForVendor
    }
  
    class func getDocumentsURL() -> URL {
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        return documentsURL
    }
    
    class func fileInDocumentsDirectory(_ filename: String) -> String {
        
        let fileURL = getDocumentsURL().appendingPathComponent(filename)
        return fileURL.path
        
    }
 
    class func spaceCheck(_ value:String)->String{
        var value = value
        value = value.replacingOccurrences(of: "\n", with: "", options: [], range: nil)
        if(value.hasPrefix(" ")){
            return value.replacingOccurrences(of: " ", with: "", options: [], range: nil)
        }else{
            return value
        }
    }
    
    
    class func removeWhiteSpaceAndNewLine(_ value:String)->String{
        return value.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }
    
    class func contactFormat(_ value:String)->String{
        var contact = value
        contact = contact.replacingOccurrences(of: " ", with: "", options: [], range: nil)
        contact = contact.replacingOccurrences(of: "(", with: "", options: [], range: nil)
        contact = contact.replacingOccurrences(of: ")", with: "", options: [], range: nil)
        contact = contact.replacingOccurrences(of: "-", with: "", options: [], range: nil)
        contact = contact.replacingOccurrences(of: " ", with: "", options: [], range: nil)
        contact = contact.replacingOccurrences(of: ".", with: "", options: [], range: nil)
        return contact
    }
    
    class func formatDate(_ date:Date)->String{
        let dateFormatter:DateFormatter = DateFormatter()
        //dateFormatter.dateStyle = NSDateFormatterStyle.ShortStyle
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let formattedDate = dateFormatter.string(from: date)
        return formattedDate
    }
    
    class func formatTime(_ date:Date)->String {
        let dateFormatter:DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm a"
        let formattedDate = dateFormatter.string(from: date)
        return formattedDate
    }
    class func formatTimeForDisplay(_ date:Date)->String {
        let dateFormatter:DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mm a"
        let formattedDate = dateFormatter.string(from: date)
        return formattedDate
    }
    
    class func convertStringToDate(_ strDate:String)->Date{
        //print("\(strDate)")
        let dateFormatter:DateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.none
        dateFormatter.dateFormat = "dd-MM-yyyy HH:mm:ss"
        let date:Date? = dateFormatter.date(from: strDate)
        //print("\(date)")
        return date ?? Date()
    }
    
    class func convertGivenStringToDate(_ strDate:String)->Date{
        //print("\(strDate)")
        let dateFormatter:DateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.none
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.sss'Z'"
        let date:Date = dateFormatter.date(from: strDate)!
        //print("\(date)")
        return date
    }
    class func convertNotificationStringToDate(_ strDate:String)->Date{
        //print("\(strDate)")
        let dateFormatter:DateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.none
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss.SSS"
        let date:Date = dateFormatter.date(from: strDate)!
        //print("\(date)")
        return date
    }
    class func formatDateTime(_ date:Date)->String{
        let dateFormatter:DateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.short
        dateFormatter.dateFormat = "d'-'MMM'-'yy HH:mm " //datetime [ d-m-Y h:i:s a ]
        let formattedDate = dateFormatter.string(from: date)
        return formattedDate
    }
   /* class func convertStringToDateTime(_ strDate:String)->Date{
        //print("\(strDate)")
        let dateFormatter:DateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.short
        
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.timeZone = TimeZone()
        dateFormatter.timeZone = TimeZone()
        dateFormatter.dateFormat = "d'-'MMM'-'yy HH:mm "
        let date:Date = dateFormatter.date(from: strDate)!
        //print("\(date)")
        return date
    }
 */
    class func formatTime(_ date:Date, format: String)->NSString{
        
        let dateFormatter:DateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.short
        dateFormatter.dateFormat = format //datetime [ d-m-Y h:i:s a ]
        let formattedDate:NSString = dateFormatter.string(from: date) as NSString
        return formattedDate
    }
    
    class func formatTime11(_ date:String)->Date{
        
        let dateFormatter:DateFormatter = DateFormatter()
//        dateFormatter.dateStyle = NSDateFormatterStyle.ShortStyle
        dateFormatter.timeStyle = .short
        dateFormatter.dateStyle = .short
        dateFormatter.dateFormat = "HH:mm a" //datetime [ d-m-Y h:i:s a ]
        let formattedDate:Date = dateFormatter.date(from: date)!
        return formattedDate
    }
    
    
    /***** COMBINATIONS(of r out of n) WITH REPETITION *****/
    class func combinationWithRep<T>( objects: [T], n: Int) -> [[T]] {
        var objects = objects
        if n == 0 { return [[]] } else {
            var combos = [[T]]()
            while let element = objects.last {
                combos.append(contentsOf: combinationWithRep(objects: objects, n: n - 1).map{ $0 + [element] })
                objects.removeLast()
            }
            return combos
        }
    }
    /***** COMBINATIONS(of r out of n) WITHOUT REPETITION *****/
    class func addCombination(prevCombo: [Int], pivotList: [Int]) -> [([Int], [Int])] {
        
        var pivotList = pivotList
        return (0..<pivotList.count)
            .map {
                _ -> ([Int], [Int]) in
                (prevCombo + [pivotList.remove(at: 0)], pivotList)
        }
    }
    class func combinationWithoutRepWithLength(n: Int, r: Int) -> [[Int]] {
        
        return [Int](0...r-1)
            .reduce([([Int](), [Int](0...n-1))]) {
                (accum, _) in
                accum.flatMap(addCombination)
            }.map {
                $0.0
        }
    }

    /****** Shuffle an array elements ******/
    class func shuffleArray<T>( array: [T]) -> [T] {
        var array = array
        for index in (0..<array.count).reversed() {
            let randomIndex = Int(arc4random_uniform(UInt32(index)))
            (array[index], array[randomIndex]) = (array[randomIndex], array[index])
        }
        
        return array
    }
    
   class func timeAgoSinceDate(_ date:Date, numericDates:Bool = false) -> String {
        let calendar = NSCalendar.current
        let unitFlags: Set<Calendar.Component> = [.minute, .hour, .day, .weekOfYear, .month, .year, .second]
        let now = Date()
        let earliest = now < date ? now : date
        let latest = (earliest == now) ? date : now
        let components = calendar.dateComponents(unitFlags, from: earliest,  to: latest)
        
        if (components.year! >= 2) {
            return "\(components.year!) years ago"
        } else if (components.year! >= 1){
            if (numericDates){
                return "1 year ago"
            } else {
                return "Last year"
            }
        } else if (components.month! >= 2) {
            return "\(components.month!) months ago"
        } else if (components.month! >= 1){
            if (numericDates){
                return "1 month ago"
            } else {
                return "Last month"
            }
        } else if (components.weekOfYear! >= 2) {
            return "\(components.weekOfYear!) weeks ago"
        } else if (components.weekOfYear! >= 1){
            if (numericDates){
                return "1 week ago"
            } else {
                return "Last week"
            }
        } else if (components.day! >= 2) {
            return "\(components.day!) days ago"
        } else if (components.day! >= 1){
            if (numericDates){
                return "1 day ago"
            } else {
                return "Yesterday"
            }
        } else if (components.hour! >= 2) {
            return "\(components.hour!) hours ago"
        } else if (components.hour! >= 1){
            if (numericDates){
                return "1 hour ago"
            } else {
                return "An hour ago"
            }
        } else if (components.minute! >= 2) {
            return "\(components.minute!) minutes ago"
        } else if (components.minute! >= 1){
            if (numericDates){
                return "1 minute ago"
            } else {
                return "A minute ago"
            }
        } else if (components.second! >= 3) {
            return "\(components.second!) seconds ago"
        } else {
            return "Just now"
        }
        
    }
}



