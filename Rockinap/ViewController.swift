//
//  ViewController.swift
//  Rockinap
//
//  Created by orionspica on 30/09/18.
//  Copyright © 2018 funndynamix. All rights reserved.
//

import UIKit
import GoogleSignIn
import Firebase//Tina change

class ViewController: CommonViewController {

    @IBOutlet weak var txtUserId: TextField!
    @IBOutlet weak var txtPassword: TextField!
    @IBOutlet weak var imgRemeberME: UIImageView!
    @IBOutlet weak var btnRememberME: UIButton!
    var isKeyboardHidden = true
    var activeField: UITextField?
    override func viewDidLoad() {
        super.viewDidLoad()
         btnRememberME.isSelected  = true
        
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardDidShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardDidHideNotification, object: nil)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardDidHideNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardDidShowNotification, object: nil)
        
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        DispatchQueue.main.async {
            self.view.endEditing(true)
        }
    }
    func callLoginApi(withData: NSDictionary, isSocial: Bool) {
        self.ShowLoader()
        
        if isSocial{
            APIFunctions.socialLogin(dictParams: withData as! [String : Any]) { (status, resObj) in
                self.HideLoader()
                
                guard let dictData = resObj as? Data else
                {
                    return
                }
                
                do {
                    let jsonObjet = try JSONSerialization.jsonObject(with: dictData, options: .mutableContainers) as? [String:AnyObject]
                    
                    if(jsonObjet != nil)
                    {
                        if let dictError = jsonObjet!["errors"] as? NSArray{
                            
                            let alert = UIAlertController(title: "", message: "Please enter your mobile number", preferredStyle: UIAlertController.Style.alert)
                            let action = UIAlertAction(title: "OK", style: .default) { (alertAction) in
                                let textField = alert.textFields![0] as UITextField
                                textField.keyboardType = .numberPad
                                if textField.text?.length == 10 {
                                    withData.setValue(textField.text, forKey: "mobile_number")
                                    withData.setValue("91", forKey: "country_code")
                                    self.callLoginApi(withData: withData, isSocial: true)
                                } else {
                                    CRNotifications.showNotification(type: CRNotifications.error, title: "Alert!", message: "Mobile number must have 10 digits", dismissDelay: 3)
                                }
                            }
                            alert.addTextField { (textField) in
                                
                                textField.placeholder = "Mobile number"
                                textField.tag = 222
                                
                            }
                            alert.addAction(action)
                            self.present(alert, animated:true, completion: nil)
                            
                            
                            
                        } else {
                            if jsonObjet!["error"] as? Bool ?? false == false
                            {
                                if jsonObjet!["message"] as? String ?? "" == "otp sent succesfully" {
                                    let reserveVC = self.storyboard?.instantiateViewController(withIdentifier: "OTPVerificationVC") as! OTPVerificationVC
                                    let data = jsonObjet?["data"] as! [String: Any]
                                    reserveVC.strMobileNumber = withData["mobile_number"] as! String
                                    reserveVC.strOtp = data["otp"] as! Int
                                    self.navigationController?.pushViewController(reserveVC, animated: true)
                                } else {
                                    let data1 = jsonObjet?["data"] as! [String: Any]
                                    let data = data1["data"] as! [String: Any]
                                    let userInfo = ["token": data1["token"] as! String, "name": data["name"] as! String, "userID": data["uuid"] as! String, "role": data["role"] as! String,"email": data["email"] as? String ?? "", "mobile_number":data["mobile_number"] as? String ?? ""] as [String : Any]
                                    UserDefaults.standard.set(userInfo, forKey: "userInformation")
                                    UserDefaults.standard.set(self.btnRememberME.isSelected, forKey: defaultsKeys.keepSigned)
                                    DispatchQueue.main.async {
                                        if status == true {
                                            UserDefaults.standard.set(Messaging.messaging().fcmToken, forKey: defaultsKeys.fcmToken)
                                            if let token = Messaging.messaging().fcmToken {
                                                self.callRefreshToken(token: token)
                                            }
                                            let appDelegate = UIApplication.shared.delegate! as! AppDelegate
                                            DrawerRootViewController.shared().updateNavigationBarStatus(true)
                                            
                                            let VC = DrawerRootViewController.shared().setupDrawerController()
                                            appDelegate.window!.rootViewController = VC
                                            
                                        }
                                    }
                                }
                                
                            }
                            else
                            {
                                CRNotifications.showNotification(type: CRNotifications.error, title: "Alert!", message: jsonObjet?["message"] as? String ?? "Login failed!", dismissDelay: 3)
                            }
                        }
                        
                    } else
                    {
                        CRNotifications.showNotification(type: CRNotifications.error, title: "Alert!", message: jsonObjet?["message"] as? String ?? "Login failed!", dismissDelay: 3)
                    }
                    
                }
                catch{
                    print(error.localizedDescription)
                }
                //}
            }
        } else {
            APIFunctions.registerUser(dictParams: withData as! [String : Any]) { (status, resObj) in
                self.HideLoader()
                
                guard let dictData = resObj as? Data else
                {
                    return
                }
                
                do {
                    let jsonObjet = try JSONSerialization.jsonObject(with: dictData, options: .mutableContainers) as? [String:AnyObject]
                    
                    if(jsonObjet != nil)
                    {
                        if jsonObjet!["error"] as? Bool ?? false == false
                        {
                            let data = jsonObjet?["data"] as! [String: Any]
                            let userInfo = ["token": data["token"] as! String, "name": data["name"] as! String, "userID": data["user_id"] as! String, "role": data["role"] as! String,"email": self.txtUserId.text ?? "", "password": self.txtPassword.text ?? "", "mobile_number":data["mobile_number"] as? String ?? ""] as [String : Any]
                            UserDefaults.standard.set(userInfo, forKey: "userInformation")
                            UserDefaults.standard.set(self.btnRememberME.isSelected, forKey: defaultsKeys.keepSigned)
                            DispatchQueue.main.async {
                                if status == true {
                                    let appDelegate = UIApplication.shared.delegate! as! AppDelegate
                                    
                                    //                                    let initialViewController = self.storyboard!.instantiateViewController(withIdentifier: "TabController") as! UITabBarController
                                    //
                                    //                                    self.navigationController?.pushViewController(initialViewController, animated: true)
                                    //let tempNavigationViewController = DrawerRootViewController.shared().loadViewControllerFromStoryBoard("RootNavigatorVC") as? UINavigationController
                                    DrawerRootViewController.shared().updateNavigationBarStatus(true)
                                    
                                    let VC = DrawerRootViewController.shared().setupDrawerController()
                                    appDelegate.window!.rootViewController = VC
                                    //                                    appDelegate.window?.rootViewController = initialViewController
                                    //                                    appDelegate.window?.makeKeyAndVisible()
                                }
                            }
                        }
                        else
                        {
                            CRNotifications.showNotification(type: CRNotifications.error, title: "Alert!", message: jsonObjet?["message"] as? String ?? "Login failed!", dismissDelay: 3)
                        }
                    } else
                    {
                        CRNotifications.showNotification(type: CRNotifications.error, title: "Alert!", message: jsonObjet?["message"] as? String ?? "Login failed!", dismissDelay: 3)
                    }
                    
                }
                catch{
                    print(error.localizedDescription)
                }
            }
        }
        
        
    }

    @IBAction func btnForgotPwdClicked(_ sender: UIButton) {
        let alert = UIAlertController(title: "Forgot Password?", message: "", preferredStyle: UIAlertController.Style.alert)
        let action = UIAlertAction(title: "Send", style: .default) { (alertAction) in
            
            let textField = alert.textFields![0] as UITextField
                textField.keyboardType = .numberPad
                if textField.text?.length == 10 {
                    let reserveVC = self.storyboard?.instantiateViewController(withIdentifier: "OTPVerificationVC") as! OTPVerificationVC
                    reserveVC.isFromForgotPassword = true
                    reserveVC.strMobileNumber = textField.text ?? ""
                    self.navigationController?.pushViewController(reserveVC, animated: true)
                } else {
                    CRNotifications.showNotification(type: CRNotifications.error, title: "Alert!", message: "Mobile number must have 10 digits", dismissDelay: 3)
                }
                
        }
        alert.addTextField { (textField) in
            
            textField.placeholder = "phone number"
            textField.tag = 221
            
        }
        alert.addAction(action)
        
        self.present(alert, animated:true, completion: nil)
    }
    @IBAction func btnRememberMeClicked(_ sender: UIButton) {
        
        btnRememberME.isSelected = !btnRememberME.isSelected
        if (btnRememberME.isSelected) {
            UIView.transition(with: imgRemeberME, duration: 0.5, options: .transitionFlipFromTop, animations: {
                self.imgRemeberME.image = UIImage.init(named: "checkicon-1")
                self.btnRememberME.isSelected = true
                //self.btnKeepSignedIn.setImage(UIImage(named:"Check"), for: .selected)
            }, completion: nil)
        } else {
            UIView.transition(with: imgRemeberME, duration: 0.5, options: .transitionFlipFromTop, animations: {
                self.imgRemeberME.image = nil
                self.btnRememberME.isSelected = false
                //self.btnKeepSignedIn.setImage(UIImage(named:"Uncheck")?.maskWithColor(color: UIColor.white), for: .normal)
            }, completion: { Bool in
                
            })
        }
    }
    private func callRefreshToken(token: String) {
         let id:String = ""
        if let userInformation = UserDefaults.standard.dictionary(forKey: "userInformation") {
            APIFunctions.refresNotificationToken(dictParams: ["refresh_token":token,
                                                              "uuid":userInformation["userID"] as! String,
                                                              "device_type":"ios",
                                                              "device_id":id.getUUID() ?? "",//Tina change
                                                              "mobile_number":userInformation["mobile_number"] as! String]) { (status, resObj) in
             }
        }
        
    }
    @IBAction func btnLoginClicked(_ sender: UIButton) {
        
        if sender.tag == 1 {
            let reserveVC = self.storyboard?.instantiateViewController(withIdentifier: "RegisterVCViewController") as! RegisterVCViewController
            self.navigationController?.pushViewController(reserveVC, animated: true)
        } else {
        
        let (isValid, message) = self.validateForm()
        
        if isValid {
            var fcmId = ""
            if DrawerRootViewController.shared().fcm_token != nil{
                fcmId  = DrawerRootViewController.shared().fcm_token ?? ""
            } else {
                //AppDelegate.shareddelegate().configurePushNotification(UIApplication.init())
                fcmId = DrawerRootViewController.shared().fcm_token ?? ""
            }
            self.ShowLoader()
            APIFunctions.loginAPICall(dictParams: ["mobile_number": self.txtUserId.text!, "password":self.txtPassword.text!, "country_code":"91", "refresh_token":fcmId, "auth_type":"manual"]) { (status, resObj) in
                self.HideLoader()
                
                guard let dictData = resObj as? Data else
                {
                    return
                }
                
                do {
                    let jsonObjet = try JSONSerialization.jsonObject(with: dictData, options: .mutableContainers) as? [String:AnyObject]
                    
                    if(jsonObjet != nil)
                    {
                        if let arr = jsonObjet!["errors"] as? NSArray {
                            self.alert(message: "Login failed!", title: "Error!")
                        } else {
                            if jsonObjet!["error"] as? Bool ?? false == false
                            {
                                let data = jsonObjet?["data"] as! [String: Any]
                                let userData = data["data"] as! [String: Any]
                                let userInfo = ["token": data["token"] as! String, "name": userData["name"] as! String, "userID": userData["uuid"] as! String, "role": userData["role"] as! String,"email": self.txtUserId.text ?? "", "password": self.txtPassword.text ?? "", "mobile_number":userData["mobile_number"] as? String ?? ""] as [String : Any]
                                UserDefaults.standard.set(userInfo, forKey: "userInformation")
                                UserDefaults.standard.set(self.btnRememberME.isSelected, forKey: defaultsKeys.keepSigned)
                                DispatchQueue.main.async {
                                    if status == true {
                                        let appDelegate = UIApplication.shared.delegate! as! AppDelegate
                                        
                                        //                                    let initialViewController = self.storyboard!.instantiateViewController(withIdentifier: "TabController") as! UITabBarController
                                        //
                                        //                                    self.navigationController?.pushViewController(initialViewController, animated: true)
                                        //let tempNavigationViewController = DrawerRootViewController.shared().loadViewControllerFromStoryBoard("RootNavigatorVC") as? UINavigationController
                                        
                                        //Tina change
                                        UserDefaults.standard.set(Messaging.messaging().fcmToken, forKey: defaultsKeys.fcmToken)
                                        if let token = Messaging.messaging().fcmToken {
                                            self.callRefreshToken(token: token)
                                        }
                                        DrawerRootViewController.shared().updateNavigationBarStatus(true)
                                        
                                        let VC = DrawerRootViewController.shared().setupDrawerController()
                                        appDelegate.window!.rootViewController = VC
                                        //                                    appDelegate.window?.rootViewController = initialViewController
                                        //                                    appDelegate.window?.makeKeyAndVisible()
                                    }
                                }
                            }
                            else
                            {
                                if let data: [String: Any] = jsonObjet?["data"] as? [String: Any] {
                                    if  data["is_mobile_verify"] as? Int ?? 1 == 1 {
                                        self.alert(message: jsonObjet?["message"] as? String ?? "Login failed!", title: "Error!")
                                    } else {
                                        let reserveVC = self.storyboard?.instantiateViewController(withIdentifier: "OTPVerificationVC") as! OTPVerificationVC
                                        self.navigationController?.pushViewController(reserveVC, animated: true)
                                    }
                                } else {
                                    self.alert(message: jsonObjet?["message"] as? String ?? "Login failed!", title: "Error!")
                                }
                                
                            }
                        }
                        
                    } else
                    {
                        self.alert(message: jsonObjet?["msg"] as? String ?? "Login failed!", title: "Error!")
                    }
                    
                }
                catch{
                    print(error.localizedDescription)
                }
                
                
            }
        } else {
            self.alert(message: message, title: "Alert")
        }
        }
    }
    
    @IBAction func btnGmailClicked(_ sender: UIButton) {
        GIDSignIn.sharedInstance()?.signOut()
        GIDSignIn.sharedInstance()?.signIn()
    }
    //MARK: - Facebook Button Login Action
    
    @IBAction func facebookDidClicked(_ sender: UIButton) {
        let fbLoginManager = FBSDKLoginManager()
        self.ShowLoader()
        fbLoginManager.logIn(withReadPermissions: ["email"], from: self) { (result, error) -> Void in
            if (error == nil){
                let fbloginresult : FBSDKLoginManagerLoginResult = result!
                // if user cancel the login
                if (result?.isCancelled)!{
                    self.HideLoader()
                    return
                }
                if(fbloginresult.grantedPermissions.contains("email"))
                {
                    self.getFBUserData()
                }
            }
        }
    }
    func validateForm() -> (Bool, String) {
        var isValid = true
        var message = ""
        
        if txtUserId.text!.length < 10 {
            isValid = false
            message = "Please enter Mobile Number"
        } else {
            if txtPassword.text?.length == 0 {
                isValid = false
                message = "Please enter Password!"
            }
        }
       
        
        return (isValid, message)
    }
    //Mark:- View according to keyboard
    // MARK: Keyboard Notifications
    @objc private func keyboardWillShow(notification: NSNotification) {
        
        let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue
        if isKeyboardHidden {
            if CGFloat(activeField?.frame.origin.y ?? 75.0) <= CGFloat(keyboardSize?.height ?? 0.0){
                UIView.animate(withDuration: 0.1) {
                    if Functions.IsiPhoneDevice() {
                        self.view.frame.origin.y -= 80.0
                        self.isKeyboardHidden = false
                    } else {
                        self.view.frame.origin.y -= 100.0
                        self.isKeyboardHidden = false
                    }
                }
            }
        }
    }
    
    @objc private func keyboardWillHide(notification: NSNotification) {
        self.view.window?.backgroundColor = UIColor.black
        let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue
        if isKeyboardHidden == false {
            if CGFloat(activeField?.frame.origin.y ?? 75.0) <= CGFloat(keyboardSize?.height ?? 0.0){
                UIView.animate(withDuration: 0.1) {
                    if Functions.IsiPhoneDevice() {
                        self.view.frame.origin.y += 80.0
                        self.isKeyboardHidden = true
                    } else {
                        self.view.frame.origin.y += 100.0
                        self.isKeyboardHidden = true
                    }
                }
            }
        }
    }
}
extension ViewController{
    func saveSocialMediaType(SocialMediaType:String){
        
        let defaults = UserDefaults.standard
        defaults.set(SocialMediaType, forKey: defaultsKeys.socialMediaType)
        defaults.synchronize()
        
    }
}
//MARK:- UITextfield Delegate
extension ViewController: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField){
        activeField = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField){
        activeField = nil
    }
    //MARK: - Configure Facebook permissions
    func getFBUserData(){
        if((FBSDKAccessToken.current()) != nil){
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, email, name, first_name, last_name, picture.type(large)"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    //everything works print the user data
                    print(result ?? " No  Data")
                    //everything works print the user data
                    print(result ?? " No  Data")
                    let Result = result as? NSDictionary
                    
                    
                    let socialDict = NSMutableDictionary()
                    //socialDict.setValue(FBSDKAccessToken.current().tokenString, forKey: "refresh_token")
                    socialDict.setValue("facebook", forKey: "provider_type")
                    socialDict.setValue(Result?.value(forKey: "id"), forKey: "provider_id")
                    socialDict.setValue(Result?.value(forKey: "name"), forKey: "name")
                    socialDict.setValue(Result?.value(forKey: "email"), forKey: "email")
                    socialDict.setValue("", forKey: "mobile_number")
                    socialDict.setValue("", forKey: "country_code")
//                    socialDict.setValue(UIDevice.current.identifierForVendor!.uuidString, forKey: "deviceID")
//                    socialDict.setValue("", forKey: "mobile")
//                    socialDict.setValue("", forKey: "password")
//                    socialDict.setValue("player", forKey: "role")
//                    let alert = UIAlertController(title: "", message: "Please enter your mobile number", preferredStyle: UIAlertController.Style.alert)
//                    let action = UIAlertAction(title: "OK", style: .default) { (alertAction) in
//
//                        let textField = alert.textFields![0] as UITextField
//                        textField.keyboardType = .numberPad
//                        if textField.text!.length == 10 {
//                            socialDict.setValue(textField.text, forKey: "mobile_number")
//                            socialDict.setValue("91", forKey: "country_code")
                            self.saveSocialMediaType(SocialMediaType: "2")
                            self.callLoginApi(withData: socialDict, isSocial: true)
//                        } else {
//                            CRNotifications.showNotification(type: CRNotifications.error, title: "Alert!", message: "Mobile number must have 10 digits", dismissDelay: 3)
//                        }
//                    }
//                    alert.addTextField { (textField) in
//
//                        textField.placeholder = "Mobile number"
//                        textField.tag = 222
//
//                    }
//                    alert.addAction(action)
//
//                    self.present(alert, animated:true, completion: nil)
                    
                  
                }
            })
        }
    }
}
//MARK:- Google Login Delegates

extension ViewController : GIDSignInUIDelegate,GIDSignInDelegate{
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if (error == nil)
        {
   
            let socialDict = NSMutableDictionary()
//            socialDict.setValue(GIDSignIn.sharedInstance().currentUser.authentication.accessToken, forKey: "refresh_token")
            socialDict.setValue("google", forKey: "provider_type")
            socialDict.setValue(user.userID, forKey: "provider_id")
            socialDict.setValue( user.profile.name, forKey: "name")
            socialDict.setValue(user.profile.email, forKey: "email")
            socialDict.setValue("", forKey: "mobile_number")
            socialDict.setValue("", forKey: "country_code")
//            socialDict.setValue(UIDevice.current.identifierForVendor!.uuidString, forKey: "deviceID")
//            socialDict.setValue("", forKey: "mobile")
//            socialDict.setValue("", forKey: "password")
//            socialDict.setValue("player", forKey: "role")
//            let alert = UIAlertController(title: "", message: "Please enter your mobile number", preferredStyle: UIAlertController.Style.alert)
//            let action = UIAlertAction(title: "OK", style: .default) { (alertAction) in
//
//                let textField = alert.textFields![0] as UITextField
//                textField.keyboardType = .numberPad
//                if textField.text!.length == 10 {
//                    socialDict.setValue(textField.text, forKey: "mobile_number")
//                    socialDict.setValue("91", forKey: "country_code")
                    self.saveSocialMediaType(SocialMediaType: "0")
                    self.callLoginApi(withData: socialDict, isSocial: true)
//                } else {
//                    CRNotifications.showNotification(type: CRNotifications.error, title: "Alert!", message: "Mobile number must have 10 digits", dismissDelay: 3)
//                }
//            }
//            alert.addTextField { (textField) in
//
//                textField.placeholder = "Mobile number"
//                textField.tag = 222
//
//            }
//            alert.addAction(action)
//
//            self.present(alert, animated:true, completion: nil)
            
        }
        else
        {
           self.HideLoader()
            print("\(error.localizedDescription)")
        }
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        // Perform any operations when the user disconnects from app here.
    }
    
    
    
}
//Tina change
class KeychainAccess {
    
    func addKeychainData(itemKey: String, itemValue: String) throws {
        guard let valueData = itemValue.data(using: .utf8) else {
            print("Keychain: Unable to store data, invalid input - key: \(itemKey), value: \(itemValue)")
            return
        }
        
        //delete old value if stored first
        do {
            try deleteKeychainData(itemKey: itemKey)
        } catch {
            print("Keychain: nothing to delete...")
        }
        
        let queryAdd: [String: AnyObject] = [
            kSecClass as String: kSecClassGenericPassword,
            kSecAttrAccount as String: itemKey as AnyObject,
            kSecValueData as String: valueData as AnyObject,
            kSecAttrAccessible as String: kSecAttrAccessibleWhenUnlocked
        ]
        let resultCode: OSStatus = SecItemAdd(queryAdd as CFDictionary, nil)
        
        if resultCode != 0 {
            print("Keychain: value not added - Error: \(resultCode)")
        } else {
            print("Keychain: value added successfully")
        }
    }
    
    func deleteKeychainData(itemKey: String) throws {
        let queryDelete: [String: AnyObject] = [
            kSecClass as String: kSecClassGenericPassword,
            kSecAttrAccount as String: itemKey as AnyObject
        ]
        
        let resultCodeDelete = SecItemDelete(queryDelete as CFDictionary)
        
        if resultCodeDelete != 0 {
            print("Keychain: unable to delete from keychain: \(resultCodeDelete)")
        } else {
            print("Keychain: successfully deleted item")
        }
    }
    
    func queryKeychainData (itemKey: String) throws -> String? {
        let queryLoad: [String: AnyObject] = [
            kSecClass as String: kSecClassGenericPassword,
            kSecAttrAccount as String: itemKey as AnyObject,
            kSecReturnData as String: kCFBooleanTrue,
            kSecMatchLimit as String: kSecMatchLimitOne
        ]
        var result: AnyObject?
        let resultCodeLoad = withUnsafeMutablePointer(to: &result) {
            SecItemCopyMatching(queryLoad as CFDictionary, UnsafeMutablePointer($0))
        }
        
        if resultCodeLoad != 0 {
            print("Keychain: unable to load data - \(resultCodeLoad)")
            return nil
        }
        
        guard let resultVal = result as? NSData, let keyValue = NSString(data: resultVal as Data, encoding: String.Encoding.utf8.rawValue) as String? else {
            print("Keychain: error parsing keychain result - \(resultCodeLoad)")
            return nil
        }
        return keyValue
    }
}
