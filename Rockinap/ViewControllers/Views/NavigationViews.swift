//
//  NavigationViews.swift
//  Rockinap
//
//  Created by orionspica on 06/10/18.
//  Copyright © 2018 funndynamix. All rights reserved.
//

import UIKit

class NavigationViews: UIView {

    @IBOutlet weak var btnMenu: UIButton!
    
    @IBOutlet weak var imgLogo: UIImageView!
    
    @IBAction func btnMenuClicked(_ sender: UIButton) {
        DrawerRootViewController.shared().openDrawer()
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
