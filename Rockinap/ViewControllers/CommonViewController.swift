//
//  CommonViewController.swift
//  Rockinap
//
//  Created by orionspica on 02/10/18.
//  Copyright © 2018 funndynamix. All rights reserved.
//

import UIKit
import MBProgressHUD
class CommonViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    func backViewNeeded() {
        let viewHeader = UIView()
        
        if self.navigationController != nil {
            viewHeader.frame  = CGRect.init(x: self.navigationController?.navigationBar.frame.origin.x ?? 0, y: (self.navigationController?.navigationBar.frame.origin.y)! + (self.navigationController?.navigationBar.frame.height)! , width: self.navigationController?.navigationBar.frame.width ?? self.view.frame.width, height: 30)
            let btnBAck = UIButton.init(frame: CGRect.init(x: 5, y: 0, width: 30, height: 30))
            btnBAck.setTitleColor(UIColor.lightGray, for: .normal)
            //btnBAck.setTitle("< Back", for: .normal)
            btnBAck.setBackgroundImage(UIImage.init(named: "back_nav")?.maskWithColor(color: UIColor.darkGray), for: .normal)
            btnBAck.addTarget(self, action: #selector(btnBackClicked), for: .touchUpInside)
            viewHeader.addSubview(btnBAck)
            self.view.addSubview(viewHeader)
        }
        
    }
    @objc fileprivate func btnBackClicked()  {
        self.navigationController?.popViewController(animated: true)
    }
    func isNavigationBarRequire(_ isNeed:Bool = true)
    {
        if(isNeed)
        {
            self.UpdateNavigationBar()
//            self.customizeLeftNavigationBar()
        }
        else
        {
            //DrawerRootViewController.shared().updateNavigationBarStatus(true)
        }
    }
    
    func UpdateNavigationBar()
    {
        addGradientColors_Nav()
        //DrawerRootViewController.shared().updateNavigationBarStatus()
    }
    
    func customizeLeftNavigationBar(_ isback:Bool = false)
    {
        var leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_icon"), style: .plain, target: self, action: #selector(clickleftBarButtonItem(_:)))
        if(isback)
        {
            leftBarButtonItem = UIBarButtonItem(image:#imageLiteral(resourceName: "back_nav"), style: .plain, target: self, action: #selector(clickleftBarButtonItem(_:)))
        }
        self.navigationItem.leftBarButtonItem  = leftBarButtonItem
    }
    @objc func clickleftBarButtonItem(_ isMenu:Bool = true)
    {
        //DrawerRootViewController.shared().openDrawer()
    }
    /*func ShowLoader()
    {
        DispatchQueue.main.async
            {
                MBProgressHUD.showAdded(to: self.view, animated: true)
        }
    }
    
    func HideLoader()
    {
        DispatchQueue.main.async
            {
                MBProgressHUD.hide(for: self.view, animated: true)
        }
    }*/
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView()
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
