//
//  OutPremiseLandingVC.swift
//  Rockinap
//
//  Created by orionspica on 04/10/18.
//  Copyright © 2018 funndynamix. All rights reserved.
//

import UIKit
import Kingfisher
class OutPremiseLandingVC: CommonViewController {
    
    @IBOutlet weak var viewReserveTable: UIView!
    @IBOutlet weak var viewInstantOffer: UIView!
    @IBOutlet weak var viewWindow: UIView!
    @IBOutlet weak var btnReserveTable: UIButton!
    @IBOutlet weak var btnInstantOffer: UIButton!
    @IBOutlet weak var btnWindow: UIButton!
    @IBOutlet weak var imgViewPoster: UIImageView!
    @IBOutlet weak var lblDescription: UILabel!
    
    @IBOutlet weak var constDescHeight: NSLayoutConstraint!
    @IBOutlet weak var btnExpand: UIButton!
    private var isTapped = false
    var outObj:Branches?
    var isOfferDisabled = false
    @IBOutlet weak var imgViewOutlet: UIImageView!
    @IBOutlet weak var lblOutletDesc: UILabel!
    @IBOutlet weak var constPopUpTop: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
        constPopUpTop.constant = 1000
        super.backViewNeeded()
        let url = URL(string: outObj?.image_path?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? "")
        self.imgViewPoster.kf.setImage(with: url, placeholder:UIImage.init(named: "loading") )
        lblDescription.text = outObj?.description
        self.imgViewOutlet.kf.setImage(with: url, placeholder:UIImage.init(named: "loading") )
        lblOutletDesc.text = outObj?.description
        //viewReserveTable.backgroundColor = UIColor(red: 171, green: 178, blue: 186, alpha: 1.0)
        viewInstantOffer.viewBottomShadow()
        viewWindow.viewBottomShadow()
        viewReserveTable.viewBottomShadow()
        if selectedCategory == OutleCategory.ShopinOut || selectedCategory == OutleCategory.ChilinOut {
            viewWindow.isHidden = true
        }
        // Do any additional setup after loading the view.
    }
    private func callSettingsAPI() {
        self.ShowLoader()
        APIFunctions.getSettings(dictParams: ["outlet_id":outObj?.id ?? ""]) { (status, obj) in
            self.HideLoader()
            if let objOutles = obj as? SettingsModel {
                if objOutles.data!.count > 0 {
                    if let objSetting = objOutles.data?.filter({ (setting) -> Bool in
                        setting.setting_key == "instant_offer"
                    })[0] {
                        if objSetting.value == 0 {
                            self.isOfferDisabled = true
                        }
                    }
                } else {
                    self.isOfferDisabled = true
                }
                
            }
            print("")
        }
    }
    @IBAction func btnExpandClicked(_ sender: UIButton) {
//        isTapped = isTapped ? false : true
//        btnExpand.setImage(isTapped ? UIImage.init(named: "arrowUp") : UIImage.init(named: "arrowDown"), for: .normal)
//        constDescHeight.constant = isTapped ? 60 : 20
        self.constPopUpTop.constant = 150
        UIView.animate(withDuration: 0.4) {
            self.view.layoutIfNeeded()
        }
        
    }
    @IBAction func btnDoneClicked(_ sender: UIButton) {
        self.constPopUpTop.constant = 1000
        UIView.animate(withDuration: 0.4) {
            self.view.layoutIfNeeded()
        }
    }
    @IBAction func btnOptionsClicked(_ sender: UIButton) {
        
        switch sender.tag {
        case 1:
            //reserve a tbale
            viewWindow.backgroundColor = UIColor.lightGray
            viewReserveTable.backgroundColor = UIColor.init().hexStringToUIColor(hex: ColorCodes.color_Out_Premise_Landing_Blue)
            viewInstantOffer.backgroundColor = UIColor.lightGray
            
            let reserveVC = self.storyboard?.instantiateViewController(withIdentifier: "CatalogListVC") as! CatalogListVC
            reserveVC.catalogOfOutletObj = outObj
            //reserveVC.catalogOfOutletObj?.category = outObj?.category
            self.navigationController?.pushViewController(reserveVC, animated: true)
            
        case 2:
            //avail offer
            if !isOfferDisabled {
                viewReserveTable.backgroundColor = UIColor.lightGray
                viewWindow.backgroundColor = UIColor.lightGray
                viewInstantOffer.backgroundColor = UIColor.init().hexStringToUIColor(hex: ColorCodes.color_Out_Premise_Landing_Blue)
                self.ShowLoader()
                var userID = ""
                if let userInformation = UserDefaults.standard.dictionary(forKey: "userInformation") {
                    userID = userInformation["userID"] as! String
                }
                APIFunctions.callInstantOfferAPI(dictParams: ["uoid":outObj?.id ?? "", "uuid": userID, "message":"test"]) { (status, resObj) in
                    self.HideLoader()
                    
                    guard let dictData = resObj as? Data else
                    {
                        return
                    }
                    
                    do {
                        let stringDate = String.init(data: resObj as! Data, encoding: String.Encoding.utf8)
                        print(stringDate!)
                        let jsonObjet = try JSONSerialization.jsonObject(with: dictData, options: .mutableContainers) as? [String:AnyObject]
                        if jsonObjet!["error"] as? Bool ?? false == true{
                            self.alert(message: jsonObjet!["message"] as? String ?? "", title: "Alert")
                        } else {
                            
                            self.alert(message: jsonObjet!["message"] as? String ?? "", title: "Success")
                            //self.dismiss(animated: true, completion: nil)
                        }
                    }
                    catch{
                        
                    }
                }
            } else {
                self.alert(message: "Sorry, instant offer is currently not available for this outlet.", title: "Alert")
            }
        case 3:
            viewWindow.backgroundColor = UIColor.init().hexStringToUIColor(hex: ColorCodes.color_Out_Premise_Landing_Blue)
            viewReserveTable.backgroundColor = UIColor.lightGray
            viewInstantOffer.backgroundColor = UIColor.lightGray
            
            let reserveVC = self.storyboard?.instantiateViewController(withIdentifier: "ReserveTablePopUpVC") as! ReserveTablePopUpVC
            reserveVC.modalPresentationStyle = .overCurrentContext
            reserveVC.modalTransitionStyle = .coverVertical
            reserveVC.bookingObj = outObj
            reserveVC.selectedOutleID = outObj?.id ?? "0"
            self.navigationController?.present(reserveVC, animated: true, completion: nil)
            //window
            
            
        default:
            print("default")
        }
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
