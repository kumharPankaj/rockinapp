//
//  ReserveTablePopUpVC.swift
//  Rockinap
//
//  Created by orionspica on 06/10/18.
//  Copyright © 2018 funndynamix. All rights reserved.
//

import UIKit

class ReserveTablePopUpVC: UIViewController {
    @IBOutlet weak var txtFieldDate: UITextField!
    @IBOutlet weak var txtFieldTime: UITextField!
    @IBOutlet weak var txtFieldGuestNo: UITextField!
    @IBOutlet weak var btnSeletDate: UIButton!
    @IBOutlet weak var btnSeletTime: UIButton!
    @IBOutlet weak var btnReserveTable: UIButton!
    var bookingObj :Branches?
    var selectedOutleID = "0"
    var datePicker = UIDatePicker()
    let toolbar = UIToolbar();
    override func viewDidLoad() {
        super.viewDidLoad()

        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.viewTapped(sender:)))
        self.view.addGestureRecognizer(tapGesture)
        setUpUI()
        // Do any additional setup after loading the view.
    }
    
    func setUpUI()  {
        self.addLineToView(view: txtFieldDate, position:.LINE_POSITION_BOTTOM, color: UIColor.black, width: 1.0)
        self.addLineToView(view: txtFieldGuestNo, position:.LINE_POSITION_BOTTOM, color: UIColor.black, width: 1.0)
        self.addLineToView(view: txtFieldTime, position:.LINE_POSITION_BOTTOM, color: UIColor.black, width: 1.0)
    }
    
    @IBAction func btnSelectDateTimeClicked(_ sender: UIButton) {
        self.view.endEditing(true)
        if sender.tag == 1{
            txtFieldDate.becomeFirstResponder()
        } else {
            txtFieldTime.becomeFirstResponder()
            
        }
        
    }
    
    @IBAction func btnClosePopupClicked(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func btnReserveTableClicked(_ sender: UIButton) {
        
        var params = [String: Any]()
        params["num_people"] = txtFieldGuestNo.text
        params["outlet_id"] = bookingObj?.id
        params["date"] = txtFieldDate.text
        params["time"] = Functions.formatTime(datePicker.date)
        params["message"] = "Reserve table for me on \(txtFieldDate.text!) at \(txtFieldTime.text!) PM to \(txtFieldGuestNo.text!) guests."
        
        self.ShowLoader()
        APIFunctions.bookATable(dictParams: params) { (status, resObj) in
            self.HideLoader()
            guard let dictData = resObj as? Data else {
                return
            }
            do {
                let jsonObjet = try JSONSerialization.jsonObject(with: dictData, options: .mutableContainers) as? [String:AnyObject]
                if jsonObjet!["error"] as? Bool ?? false == true{
                    self.alert(message: jsonObjet!["message"] as? String ?? "", title: "Alert")
                } else {
                    
                    self.alert(message: jsonObjet!["message"] as? String ?? "", title: "Success")
                        self.dismiss(animated: true, completion: nil)
                    
                }
                
                
            }
            catch {
                
            }
        }
        
    }
    // MARK: - Tap Action
    @objc func viewTapped(sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    //MARK:- Date picker setup
    func addDatePicker(_ datePickerMode:UIDatePicker.Mode = .date){
        //Formate Date
        datePicker.datePickerMode = datePickerMode
        //datePicker.addTarget(self, action: #selector(datePickerValueChanged), for: .valueChanged)
        datePicker.minimumDate = Date()
        //ToolBar
        toolbar.sizeToFit()
        //done button & cancel button
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(self.donedatePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.done, target: self, action: #selector(self.cancelDatePicker))
        toolbar.setItems([cancelButton,spaceButton,doneButton], animated: false)
        
        
    }
    @objc func donedatePicker(){
        setDate()
       
    }
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    @objc func datePickerValueChanged(sender: UIDatePicker) {
        setDate()
        print(sender.date)
    }
    //MARK: - Date Setting
    func setDate() -> Void {
        if(datePicker.datePickerMode == .time)
        {
            txtFieldTime.text = "\(Functions.formatTimeForDisplay(datePicker.date))"
        } else if(datePicker.datePickerMode == .date) {
            txtFieldDate.text = "\(Functions.formatDate(datePicker.date))"
        } else {
            
        }
        self.view.endEditing(true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension ReserveTablePopUpVC: UITextFieldDelegate{

    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == txtFieldDate {
            
            addDatePicker(.date)
            textField.inputAccessoryView = toolbar
            textField.inputView = datePicker
            
        } else if textField == txtFieldTime {
            addDatePicker(.time)
            textField.inputAccessoryView = toolbar
            textField.inputView = datePicker
        } else {
            textField.inputAccessoryView = toolbar
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txtFieldGuestNo{
            return true
        }
        return false
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if txtFieldGuestNo.text?.length ?? 0 > 0 && txtFieldGuestNo.text?.length ?? 0 > 0 && txtFieldGuestNo.text?.length ?? 0 > 0{
            btnReserveTable.isUserInteractionEnabled = true
            btnReserveTable.backgroundColor = UIColor.init(hexString: "#FCB315")
        }
    }
}
