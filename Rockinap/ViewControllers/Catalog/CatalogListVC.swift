//
//  CatalogListVC.swift
//  Rockinap
//
//  Created by ORION on 24/09/18.
//  Copyright © 2018 cjpl. All rights reserved.
//

import UIKit

class CatalogListVC: CommonViewController {

    var arrCatalogs = [Products]()
    @IBOutlet weak var cvCatalog: UICollectionView!
    var catalogOfOutletObj :Branches?
    override func viewDidLoad() {
        super.viewDidLoad()
        super.backViewNeeded()
        getCatalogList()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidLayoutSubviews() {
        self.cvCatalog.reloadData()
    }

    //MARK:- API Call
    
    func getCatalogList() {
        APIFunctions.getCatalogList(dictParams: ["outlet_id":catalogOfOutletObj?.id ?? "1", "sortType":"asc", "limit":""]) { (status, resObj) in
            
            if let objOutles = resObj as? ProductModel{
                self.arrCatalogs = objOutles.productList!
                self.cvCatalog.reloadData()
                
            } else {
                self.alert(message: resObj as? String ?? "Somthing went wrong", title: "Alert")
            }
           
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension CatalogListVC: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
      return arrCatalogs.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellCatalog", for: indexPath) as! CatalogListCell
        let obj = arrCatalogs[indexPath.item]
        cell.lblCatalogName.text = obj.name
        cell.lblCatalogDesc.text = obj.description
        let url = URL(string: obj.imgae_url?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? "")
        cell.imgContent.kf.setImage(with: url, placeholder:UIImage.init(named: "loading") )
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let obj = arrCatalogs[indexPath.item]
        let reserveVC = self.storyboard?.instantiateViewController(withIdentifier: "CatalogDetailsVC") as! CatalogDetailsVC
        reserveVC.selectedCatalogObj = obj
        self.navigationController?.pushViewController(reserveVC, animated: true)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width/2 - 10, height: (collectionView.frame.size.width/2 - 10) * 1.5)
    }
}
