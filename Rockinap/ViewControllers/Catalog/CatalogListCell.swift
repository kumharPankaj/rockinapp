//
//  CatalogListCell.swift
//  Rockinap
//
//  Created by ORION on 24/09/18.
//  Copyright © 2018 cjpl. All rights reserved.
//

import UIKit

class CatalogListCell: UICollectionViewCell {

    @IBOutlet weak var imgContent: UIImageView!
    
    @IBOutlet weak var lblCatalogName: UILabel!
    @IBOutlet weak var lblCatalogDesc: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
