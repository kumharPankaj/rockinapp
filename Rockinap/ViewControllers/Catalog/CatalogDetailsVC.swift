//
//  CatalogDetailsVC.swift
//  Rockinap
//
//  Created by Pankaj Kumhar on 1/2/19.
//  Copyright © 2019 funndynamix. All rights reserved.
//

import UIKit

class CatalogDetailsVC: CommonViewController {
    @IBOutlet weak var collectionView: BJAutoScrollingCollectionView! //Step 1
    @IBOutlet weak var collectionViewFlowLayout: UICollectionViewFlowLayout!
    
    @IBOutlet weak var lblRating: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDesc: UITextView!
    var selectedCatalogObj:Products?
    var imagesArray = [String]()//[#imageLiteral(resourceName: "BlueDrop"), #imageLiteral(resourceName: "GreenDrops"), #imageLiteral(resourceName: "HouseFly"), #imageLiteral(resourceName: "DropsMacro"), #imageLiteral(resourceName: "ZoomedDrop")]
    override func viewDidLoad() {
        super.viewDidLoad()
        if selectedCatalogObj?.image_slider?.count ?? 0 > 0 {
            if selectedCatalogObj?.image_slider?[0].length ?? 0 > 0 {
                imagesArray = (selectedCatalogObj?.image_slider)!
            } else {
                imagesArray.append(selectedCatalogObj?.imgae_url ?? "")
            }
        } else {
            imagesArray.append(selectedCatalogObj?.imgae_url ?? "")
        }
        
        super.backViewNeeded()
        self.initCollectionView()
        // Do any additional setup after loading the view.
    }
    
    func initCollectionView() {
        lblRating.text = "\(selectedCatalogObj?.avg_rating ?? 0.0)"
        lblDesc.text = (selectedCatalogObj?.description ?? "")// + "\n\(selectedCatalogObj?.)"
        lblTitle.text = selectedCatalogObj?.name
        
        self.collectionView.scrollInterval = 2 //Step 2
        self.collectionViewFlowLayout.scrollDirection = .horizontal
        self.collectionViewFlowLayout.minimumInteritemSpacing = 0
        self.collectionViewFlowLayout.minimumLineSpacing = 0
        self.collectionView.startScrolling() //Step 3
    }
    
   /* @IBAction func startScrollingButtonTapped(_ sender: Any) {
        self.collectionView.startScrolling()
    }
    
    @IBAction func stopScrollingButtonTapped(_ sender: Any) {
        self.collectionView.stopScrolling() //Step 4
    }
    
    @IBAction func previousButtonTapped(_ sender: Any) {
        self.collectionView.scrollToPreviousOrNextCell(direction: .left) //Step 5
    }
    
    @IBAction func nextButtonTapped(_ sender: Any) {
        self.collectionView.scrollToPreviousOrNextCell(direction: .right) //Step 6
    }*/
    
    /*@IBAction func quitButtonTapped(_ sender: Any) {
        //Don't do this in a real project 🤪
        DispatchQueue.global().async {
            let unwrapMe: Character? = nil
            _ = unwrapMe!
        }
    }*/
    
}

extension CatalogDetailsVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cellIdentifier: String = "CustomImageCollectionCell"
        let cell = self.collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! CustomImageCollectionViewCell
        let url = URL(string: self.imagesArray[indexPath.row].addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
        cell.imageView.kf.setImage(with: url, placeholder:UIImage.init(named: "loading") )
        //cell.imageView.image = self.imagesArray[indexPath.row]
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.imagesArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: self.collectionView.frame.size.width, height: self.collectionView.frame.size.height)
    }
}

