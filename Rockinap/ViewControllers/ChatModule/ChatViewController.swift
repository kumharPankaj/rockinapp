//
//  ChatViewController.swift
//  Rockinap
//
//  Created by Pankaj Kumhar on 10/20/18.
//  Copyright © 2018 funndynamix. All rights reserved.
//

import UIKit
import  JSQMessagesViewController
import FirebaseAuth
//import SQLite
class ChatViewController: JSQMessagesViewController {
    fileprivate var messages = [JSQMessage]()
    lazy var outgoingBubble: JSQMessagesBubbleImage = {
        return JSQMessagesBubbleImageFactory()!.outgoingMessagesBubbleImage(with: UIColor.jsq_messageBubbleBlue())
    }()
    
    lazy var incomingBubble: JSQMessagesBubbleImage = {
        return JSQMessagesBubbleImageFactory()!.incomingMessagesBubbleImage(with: UIColor.jsq_messageBubbleGreen())
    }()
    
    var objChatUser = OnlineUser()
    var isUserFromBuddies = false
    override func viewDidLoad() {
        super.viewDidLoad()
        var userID = ""
        if let userInformation = UserDefaults.standard.dictionary(forKey: "userInformation") {
            userID = userInformation["userID"] as! String
            senderId = userID
            senderDisplayName = objChatUser.name
        }
        
        let viewHeader = UIView()
        
        if self.navigationController != nil {
            viewHeader.frame  = CGRect.init(x: self.navigationController?.navigationBar.frame.origin.x ?? 0, y: (self.navigationController?.navigationBar.frame.origin.y)! + (self.navigationController?.navigationBar.frame.height)! , width: self.navigationController?.navigationBar.frame.width ?? self.view.frame.width, height: 30)
            let btnBAck = UIButton.init(frame: CGRect.init(x: 5, y: 0, width: 30, height: 30))
            btnBAck.setTitleColor(UIColor.lightGray, for: .normal)
            //btnBAck.setTitle("< Back", for: .normal)
            btnBAck.setBackgroundImage(UIImage.init(named: "back_nav")?.maskWithColor(color: UIColor.darkGray), for: .normal)
            btnBAck.addTarget(self, action: #selector(btnBackClicked), for: .touchUpInside)
            viewHeader.addSubview(btnBAck)
            self.view.addSubview(viewHeader)
        }
        /*if  let id = defaults.string(forKey: "jsq_id"),
         let name = defaults.string(forKey: "jsq_name")
         {
         
         }
         else
         {
         senderId = String(arc4random_uniform(999999))
         senderDisplayName = ""
         
         defaults.set(senderId, forKey: "jsq_id")
         defaults.synchronize()
         
         showDisplayNameDialog()
         }*/
        
        title = "Chat: \(senderDisplayName!)"
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(showDisplayNameDialog))
        tapGesture.numberOfTapsRequired = 1
        
        //navigationController?.navigationBar.addGestureRecognizer(tapGesture)
        //Auth.auth().signInAnonymously(completion: nil)
        inputToolbar.contentView.leftBarButtonItem = nil
        collectionView.collectionViewLayout.incomingAvatarViewSize = CGSize.zero
        collectionView.collectionViewLayout.outgoingAvatarViewSize = CGSize.zero
        //getChatHistory()
        getChatHistory()
        /* let query = Constants.refs.databaseChats.queryLimited(toLast: 10)
         
         _ = query.observe(.childAdded, with: { [weak self] snapshot in
         
         if  let data        = snapshot.value as? [String: String],
         let id          = data["sender_id"],
         let name        = data["name"],
         let text        = data["text"],
         !text.isEmpty
         {
         if let message = JSQMessage(senderId: id, displayName: name, text: text)
         {
         self?.messages.append(message)
         
         self?.finishReceivingMessage()
         }
         }
         })*/
        
        // Do any additional setup after loading the view.
    }
    @objc fileprivate func btnBackClicked()  {
        if let userInformation = UserDefaults.standard.dictionary(forKey: "userInformation") {
           let userID = userInformation["userID"] as! String
            SocketIOManager.sharedInstance.backPressed(messageBody: ["sender_id":userID, "receiver_uuid":objChatUser.uuid])
        }
        
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if isUserFromBuddies{
            self.inputToolbar.isHidden = true
            /*let array = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
            let docDir = array[0]
            let docPath = docDir + "/Rockinap.sqlite"
            
            let db = try! Connection(docPath)
            for row in try! db.prepare("SELECT * FROM ChatMessage where (senderId = ? AND receiverId = ?) or (receiverId = ? AND senderId = ?)",objChatUser.uuid, senderId,objChatUser.uuid, senderId) {
                let message = JSQMessage.init(senderId: row[1] as? String ?? "", displayName: row[3] as? String ?? "", text: row[4] as? String ?? "", receiverId: row[2] as? String ?? "")
                self.messages.append(message!)
            }
            //self.messages.reverse()
            self.finishReceivingMessage()*/
            //getChatHistory()
        } else {
//            let array = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
//            let docDir = array[0]
//            let docPath = docDir + "/Rockinap.sqlite"
//
//            let db = try! Connection(docPath)
//            for row in try! db.prepare("SELECT * FROM ChatMessage where (senderId = ? AND receiverId = ?) or (receiverId = ? AND senderId = ?)",objChatUser.uuid, senderId,objChatUser.uuid, senderId) {
//                let msg = JSQMessage.init(senderId: row[1] as? String ?? "", displayName: row[3] as? String ?? "", text: row[4] as? String ?? "", receiverId: row[2] as? String ?? "")
//                //let message = JSQMessage.init(senderId: row[1] as? String ?? "", displayName: row[3] as? String ?? "", text: row[4] as? String ?? "", receiverId: row[2] as? String ?? "")
//
//                self.messages.append(msg!)
//            }
//            self.finishReceivingMessage()
            
            //sleep(1)
            SocketIOManager.sharedInstance.getUnreadChatMessage { (messageInfo) -> Void in
                //DispatchQueue.main.async {
                let message = JSQMessage.init(senderId: messageInfo["uuid"] as? String ?? "", displayName: self.objChatUser.name, text: messageInfo["message"] as? String ?? "")
                self.messages.append(message!)
                self.finishReceivingMessage()
                var dictAck = messageInfo
                dictAck["is_seen"] = 1 as AnyObject
                dictAck["is_deliver"] = 1 as AnyObject
                dictAck["is_window_open"] = 1 as AnyObject
                SocketIOManager.sharedInstance.receiveMsgAck(messageBody: dictAck)
                
//                let array = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
//                let docDir = array[0]
//                let docPath = docDir + "/Rockinap.sqlite"
//                let db = try! Connection(docPath)
//                let stmt = try! db.prepare("INSERT INTO ChatMessage (senderId, receiverId, displayName, message, isImage) VALUES (?,? ,?,?, ?)"
//                    , self.objChatUser.uuid /*messageInfo["uuid"] as? String ?? ""*/, self.senderId,
//                      self.objChatUser.name,messageInfo["message"] as? String ?? "",0)
//
//                try! db.transaction {
//                    try stmt.run()
//                }
                
            }
        }
        
        
        
        
    }
    func getChatHistory() {
        //self.ShowLoader()
        if isUserFromBuddies {
            if let userInformation = UserDefaults.standard.dictionary(forKey: "userInformation") {
                let userID = userInformation["userID"] as! String
                self.ShowLoader()
                APIFunctions.getHistoryMsgs(dictParams: ["sender":userID, "receiver":objChatUser.uuid]) { (status, obj) in
                   self.HideLoader()
                    if status {
                        guard let dictChat = obj as? [String: AnyObject] else {
                            return
                        }
                        let arrMsgs = dictChat["data"] as? [[String: AnyObject]]
                        if arrMsgs?.count ?? 0 > 0{
                            for messageInfo in arrMsgs! {
                                if  messageInfo["receiver"] as? String ?? "" == self.senderId {
                                    let message = JSQMessage.init(senderId: messageInfo["sender"] as? String ?? "", displayName: self.objChatUser.name, text: messageInfo["message"] as? String ?? "")
                                    self.messages.append(message!)
                                } else {
                                    if let userInformation = UserDefaults.standard.dictionary(forKey: "userInformation") {
                                        let userID = userInformation["userID"] as! String
                                        let message = JSQMessage.init(senderId: userID, displayName: self.objChatUser.name, text: messageInfo["message"] as? String ?? "")
                                        self.messages.append(message!)
                                    }
                                }
                                
                                if messageInfo["is_seen"] as? Int == 0 {
                                    var dictAck = messageInfo
                                    dictAck["is_seen"] = 1 as AnyObject
                                    dictAck["is_deliver"] = 1 as AnyObject
                                    dictAck["is_window_open"] = 1 as AnyObject
                                    SocketIOManager.sharedInstance.receiveMsgAck(messageBody: dictAck)
                                }
                            }
                            DispatchQueue.main.async {
                                self.finishReceivingMessage()
                            }
                        }
                    }
                }
            }
        } else  {
            SocketIOManager.sharedInstance.getChatHistory { (obj) in
                if obj.count > 0 {
                    for messageInfo in obj {
                        if  messageInfo["receiver"] as? String ?? "" == self.senderId {
                            let message = JSQMessage.init(senderId: messageInfo["sender"] as? String ?? "", displayName: self.objChatUser.name, text: messageInfo["message"] as? String ?? "")
                            self.messages.append(message!)
                        } else {
                            if let userInformation = UserDefaults.standard.dictionary(forKey: "userInformation") {
                                let userID = userInformation["userID"] as! String
                                let message = JSQMessage.init(senderId: userID, displayName: self.objChatUser.name, text: messageInfo["message"] as? String ?? "")
                                self.messages.append(message!)
                            }
                        }
                        
                        if messageInfo["is_seen"] as? Int == 0 {
                            var dictAck = messageInfo
                            dictAck["is_seen"] = 1 as AnyObject
                            dictAck["is_deliver"] = 1 as AnyObject
                            dictAck["is_window_open"] = 1 as AnyObject
                            SocketIOManager.sharedInstance.receiveMsgAck(messageBody: dictAck)
                        }
                    }
                    DispatchQueue.main.async {
                        self.finishReceivingMessage()
                    }
                }
            }
        }
    }
    @objc func showDisplayNameDialog()
    {
        let defaults = UserDefaults.standard
        
        let alert = UIAlertController(title: "Your Display Name", message: "Before you can chat, please choose a display name. Others will see this name when you send chat messages. You can change your display name again by tapping the navigation bar.", preferredStyle: .alert)
        
        alert.addTextField { textField in
            
            if let name = defaults.string(forKey: "jsq_name")
            {
                textField.text = name
            }
            else
            {
                let names = ["Ford", "Arthur", "Zaphod", "Trillian", "Slartibartfast", "Humma Kavula", "Deep Thought"]
                textField.text = names[Int(arc4random_uniform(UInt32(names.count)))]
            }
        }
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak self, weak alert] _ in
            
            if let textField = alert?.textFields?[0], !textField.text!.isEmpty {
                
                self?.senderDisplayName = textField.text
                
                self?.title = "Chat: \(self!.senderDisplayName!)"
                
                defaults.set(textField.text, forKey: "jsq_name")
                defaults.synchronize()
            }
        }))
        
        present(alert, animated: true, completion: nil)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageDataForItemAt indexPath: IndexPath!) -> JSQMessageData!
    {
        return messages[indexPath.item]
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return messages.count
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageBubbleImageDataForItemAt indexPath: IndexPath!) -> JSQMessageBubbleImageDataSource!
    {
        
        return messages[indexPath.item].senderId == senderId ? outgoingBubble : incomingBubble
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, avatarImageDataForItemAt indexPath: IndexPath!) -> JSQMessageAvatarImageDataSource!
    {
        return nil
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, attributedTextForMessageBubbleTopLabelAt indexPath: IndexPath!) -> NSAttributedString!
    {
        return messages[indexPath.item].senderId == senderId ? nil : NSAttributedString(string: messages[indexPath.item].senderDisplayName)
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForMessageBubbleTopLabelAt indexPath: IndexPath!) -> CGFloat
    {
        return messages[indexPath.item].senderId == senderId ? 0 : 15
    }
    
    override func didPressSend(_ button: UIButton!, withMessageText text: String!, senderId: String!, senderDisplayName: String!, date: Date!)
    {
        /*let ref = Constants.refs.databaseChats.childByAutoId()
         
         let message = ["sender_id": senderId, "name": senderDisplayName, "text": text]
         
         ref.setValue(message)*/
        var userID = ""
        var token = ""
        var name = ""
        if let userInformation = UserDefaults.standard.dictionary(forKey: "userInformation") {
            userID = userInformation["userID"] as! String
            token = userInformation["token"] as! String
            name = userInformation["name"] as! String
            let currentDate = Date()
            let since1970 = currentDate.timeIntervalSince1970
            SocketIOManager.sharedInstance.sendMessage(messageBody: ["user":name, "receiver":objChatUser.uuid,
                                                                     "uuid":userID, "message":text, "image":"",
                                                                     "socket_id":objChatUser.socket_id,
                                                                     "is_window_open":0,
                                                                     "uoid":objChatUser.outlet_id,
                                                                     "timestamp":Int(since1970 * 1000)])
//            "\(Int(NSDate().timeIntervalSince1970))"
            let array = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
            let docDir = array[0]
            let docPath = docDir + "/Rockinap.sqlite"
            /*let db = try! Connection(docPath)
            let stmt = try! db.prepare(
                "INSERT INTO ChatMessage (senderId, receiverId, displayName, message, isImage) VALUES (?, ? , ?, ?, ?)"
                , userID, objChatUser.uuid,name,text,0)
            
            try! db.transaction {
                try stmt.run()
            }*/
            
            let message = JSQMessage.init(senderId: userID, displayName: name, text: text)
            self.messages.append(message!)
            finishSendingMessage()
            
        }
        
    }
}
