//
//  OutletListVC.swift
//  Rockinap
//
//  Created by orionspica on 02/10/18.
//  Copyright © 2018 funndynamix. All rights reserved.
//

import UIKit
import Kingfisher
class OutletListVC: CommonViewController {

    @IBOutlet weak var tblOutletList: UITableView!
    var arrOutlet = [Outlets]()
    var arrFilteredOutlet = [Outlets]()
   
    fileprivate let searchController = UISearchController(searchResultsController: nil)
    @IBOutlet fileprivate var searchFooter: SearchFooter!
    
    override func viewDidLoad() {
        super.viewDidLoad()

       
        // Setup the Search Controller
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search"
        navigationItem.searchController = searchController
        definesPresentationContext = true
        
        
        super.backViewNeeded()
        self.title = selectedCategory
        getOutletList()
        // Do any additional setup after loading the view.
    }
    
   
    override func viewDidLayoutSubviews()
    {
        super.viewDidLayoutSubviews()
        let viewHeader = UIView.init(frame: CGRect.init(x: 55, y: 5, width: self.view.frame.width - 55, height: 30))
        viewHeader.backgroundColor = UIColor.red
        self.navigationItem.titleView = viewHeader
    }
    override func viewWillAppear(_ animated: Bool) {
//        if splitViewController!.isCollapsed {
//            if let selectionIndexPath = tblOutletList.indexPathForSelectedRow {
//                tblOutletList.deselectRow(at: selectionIndexPath, animated: animated)
//            }
//        }
        super.viewWillAppear(animated)
    }
    //MARK:- API call
    func getOutletList() {
         self.ShowLoader()
        APIFunctions.getOutletList(dictParams: [API_PARAM_KEYS.CATEGORY.rawValue:selectedCategory]) { (status, resObj) in
             self.HideLoader()
            if let objOutles = resObj as? OutletModel{
                self.arrOutlet = objOutles.outletList!
                self.viewDidLayoutSubviews()
                self.tblOutletList.setNeedsLayout()
                self.tblOutletList.layoutIfNeeded()
                self.tblOutletList.reloadData()
            } else {
                let alertController = UIAlertController(title:"Alert!" , message: "Please login again to enjoy Rockinap.", preferredStyle:.alert)
                let firstAction = UIAlertAction(title: "Logout", style: .default) { (alert: UIAlertAction!) -> Void in
                    if let token = UserDefaults.standard.value(forKey: defaultsKeys.fcmToken) {
                        var user = ""
                        if let userInformation = UserDefaults.standard.dictionary(forKey: "userInformation") {
                            user = userInformation["userID"] as! String
                            
                        }
                        let id:String = ""
                        self.ShowLoader()
                        APIFunctions.logoutAPICall(dictParams: ["uuid":user, "device_id":id.getUUID() ?? ""]) { (status, resObj) in
                            //            objBeaconDetected?.name ?? "1"
                            
                            self.HideLoader()
                            self.resetDefaults()
                            DrawerRootViewController.shared().logout()
                            //self.navigationController?.popViewController(animated: true)
                        }
                        
                    } else {
                        self.resetDefaults()
                        DrawerRootViewController.shared().logout()
                    }
                    
                    
                    alertController.dismiss(animated: true, completion: nil)
                }
               
                alertController.addAction(firstAction)
                self.present(alertController, animated: true, completion: nil)
            }
        }
        
    }
    // MARK: - Private instance methods
    
    fileprivate func filterContentForSearchText(_ searchText: String) {
        arrFilteredOutlet = arrOutlet.filter({( candy : Outlets) -> Bool in
            
            return candy.name?.lowercased().contains(searchText.lowercased()) ?? false
        })
        tblOutletList.reloadData()
    }
    
    fileprivate func searchBarIsEmpty() -> Bool {
        return searchController.searchBar.text?.isEmpty ?? true
    }
    
    fileprivate func isFiltering() -> Bool {
        let searchBarScopeIsFiltering = searchController.searchBar.selectedScopeButtonIndex != 0
        return searchController.isActive && (!searchBarIsEmpty() || searchBarScopeIsFiltering)
    }
    //MARK:- Table view methods
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if isFiltering() {
            searchFooter.setIsFilteringToShow(filteredItemCount: arrFilteredOutlet.count, of: arrOutlet.count)
            return arrFilteredOutlet.count
        }
        
        searchFooter.setNotFiltering()
        return arrOutlet.count 
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "cellOutlet")

        let arrViews = Bundle.main.loadNibNamed("OutletTVCell", owner: self, options: nil)
        let cellOutlet = arrViews?[1] as? OutletCell
        
        cellOutlet?.setUpOutletCell(objOutlet: isFiltering() ? arrFilteredOutlet[indexPath.row] : arrOutlet[indexPath.row])
        cell?.setNeedsLayout()
        cell?.layoutIfNeeded()
        cellOutlet?.subviews[0].subviews[0].dropShadow(color: .lightGray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 3, scale: true, cellWidth: Int(self.tblOutletList.frame.width - 10))
        cell = cellOutlet!
        return cell ?? UITableViewCell()
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 110
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
            let initialViewController = self.storyboard!.instantiateViewController(withIdentifier: "OutletBranchListVC") as! OutletBranchListVC
            initialViewController.selectedOutlet = isFiltering() ? arrFilteredOutlet[indexPath.row] : arrOutlet[indexPath.row]
            initialViewController.selectedOutlet?.category = selectedCategory
            self.navigationController?.pushViewController(initialViewController, animated: true)
            
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension OutletListVC: UISearchBarDelegate {
    // MARK: - UISearchBar Delegate
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        filterContentForSearchText(searchBar.text!)
    }
}

extension OutletListVC: UISearchResultsUpdating {
    // MARK: - UISearchResultsUpdating Delegate
    func updateSearchResults(for searchController: UISearchController) {
        let searchBar = searchController.searchBar
        //let scope = searchBar.scopeButtonTitles![searchBar.selectedScopeButtonIndex]
        filterContentForSearchText(searchController.searchBar.text!)
    }
}
