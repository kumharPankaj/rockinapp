//
//  OutletTVCell.swift
//  Rockinap
//
//  Created by orionspica on 03/10/18.
//  Copyright © 2018 funndynamix. All rights reserved.
//

import UIKit

class OutletTVCell: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}

class OutletCell: UITableViewCell {
    
    @IBOutlet weak var imgViewOutle: UIImageView!
    @IBOutlet weak var lblOutletName: UILabel!
    @IBOutlet weak var lblOutletRating: UILabel!
    @IBOutlet weak var lblOutletDesc: UILabel!
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    override func layoutIfNeeded() {
        super.layoutIfNeeded()
    }
    func setUpOutletCell(objOutlet: Outlets) {
        self.lblOutletName.text = objOutlet.name
        self.lblOutletDesc.text = objOutlet.description
        //self.lblOutletRating.attributedText = addRatingStar(uptoCount: Double(objOutlet.outlet_count ?? Int(0.0)))
        let url = URL(string: objOutlet.image_path?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? "")
        self.imgViewOutle.kf.setImage(with: url, placeholder:UIImage.init(named: "loading") )
    }
    
    func setUpBranchCell(objBranch: Branches) {
        self.lblOutletName.text = objBranch.name
        self.lblOutletDesc.text = objBranch.description
        //self.lblOutletRating.attributedText = addRatingStar(uptoCount: Double(objBranch. ?? Int(0.0)))
        let url = URL(string: objBranch.image_path?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? "")
        self.imgViewOutle.kf.setImage(with: url, placeholder:UIImage.init(named: "loading") )
    }
}
