//
//  InOutPopUpVC.swift
//  Rockinap
//
//  Created by orionspica on 03/10/18.
//  Copyright © 2018 funndynamix. All rights reserved.
//

import UIKit

class InOutPopUpVC: UIViewController {
    
    @IBOutlet weak var viewMain: UIView!
    public var onDismissChangedHandler:WebCallbackHandler? = nil
    
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let gesture = UITapGestureRecognizer.init(target: self, action: #selector(self.dismissListVc))
        viewMain.addGestureRecognizer(gesture)
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnCloseClicked(_ sender: UIButton) {
        switch sender.tag {
        case 1:
             dismissListVc()
        case 2:
            dismissListVc()
            if(onDismissChangedHandler != nil) {onDismissChangedHandler!(true,"InPremise" as AnyObject)}
        case 3:
             dismissListVc()
            if(onDismissChangedHandler != nil) {onDismissChangedHandler!(true,"OutPremise" as AnyObject)}
            
        default:
            print("default")
        }
        
    }

    @objc func dismissListVc()
    {
        self.dismiss(animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
