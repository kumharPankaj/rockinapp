//
//  OutletBranchListVC.swift
//  Rockinap
//
//  Created by orionspica on 05/10/18.
//  Copyright © 2018 funndynamix. All rights reserved.
//

import UIKit
import CoreBluetooth
class OutletBranchListVC: CommonViewController, CBCentralManagerDelegate, CLLocationManagerDelegate {

    private var manager:CBCentralManager!
    private var isBluetoothOn = false
    @IBOutlet weak var tblBranches: UITableView!
     var arrBranches = [Branches]()
    var arrFilteredBranches = [Branches]()
    var selectedOutlet:Outlets?
    fileprivate let searchController = UISearchController(searchResultsController: nil)
    @IBOutlet fileprivate var searchFooter: SearchFooter!
    var currentLocation: CLLocation!
    private var locationManager = CLLocationManager()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        manager          = CBCentralManager()
        manager.delegate = self
        // Setup the Search Controller
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search"
        navigationItem.searchController = searchController
        definesPresentationContext = true
        locationManager.delegate = self
        //locationManager.requestAlwaysAuthorization()
        
        if( CLLocationManager.authorizationStatus() == .authorizedWhenInUse ||
            CLLocationManager.authorizationStatus() ==  .authorizedAlways){
            
            currentLocation = locationManager.location
            
        }
        super.backViewNeeded()
        self.title = selectedOutlet?.name
        /*var locManager = CLLocationManager()
        locManager.requestWhenInUseAuthorization()
        
        
        if( CLLocationManager.authorizationStatus() == .authorizedWhenInUse ||
            CLLocationManager.authorizationStatus() ==  .authorizedAlways){
            
            currentLocation = locManager.location
            
        }*/
        getOutletBranchList()
        // Do any additional setup after loading the view.
    }
    
    func getOutletBranchList() {
        self.ShowLoader()
        //[API_PARAM_KEYS.ORGANISATION_ID.rawValue:selectedOutlet?.organisation_id ?? "1" , "lat":currentLocation.coordinate.latitude,"long":currentLocation.coordinate.longitude, "sortType":"asc", "limit":10]
        //if currentLocation != nil{
        //[API_PARAM_KEYS.ORGANISATION_ID.rawValue:selectedOutlet?.organisation_id ?? "1" , "sortType":"asc", "limit":10, "lat":currentLocation?.coordinate.latitude ?? 0.0, "lon":currentLocation?.coordinate.longitude ?? 0.0]
//        [API_PARAM_KEYS.ORGANISATION_ID.rawValue:selectedOutlet?.organisation_id ?? "1" , "sortType":"asc", "limit":10, "lat":19.226250, "lon":72.831400]
        APIFunctions.getBranchesList(dictParams: [API_PARAM_KEYS.ORGANISATION_ID.rawValue:selectedOutlet?.organisation_id ?? "1" , "sortType":"asc", "limit":10, "lat":19.226250, "lon":72.831400]) { (status, resObj) in
                self.HideLoader()
                if let objOutles = resObj as? BrancheModel{
                    self.arrBranches = objOutles.branchesList!
                    self.viewDidLayoutSubviews()
                    self.tblBranches.reloadData()
                } else {
                    let dicResp = resObj as? ErrorModel
                    self.alert(message: dicResp?.message ?? "", title: "Alert")
                }
            }
       /* } else {
            let alertController = UIAlertController(title:"", message: "Please enable your location!", preferredStyle:.alert)
            let firstAction = UIKit.UIAlertAction(title: "Ok", style: .default) { (alert: UIAlertAction!) -> Void in
                self.navigationController?.popViewController(animated: true)
                alertController.dismiss(animated: true, completion: nil)
            }
            alertController.addAction(firstAction)
            self.present(alertController, animated: true, completion: nil)
        }*/
        
    }
    
    // MARK: - Private instance methods
    
    fileprivate func filterContentForSearchText(_ searchText: String) {
        arrFilteredBranches = arrBranches.filter({( candy : Branches) -> Bool in
            
            return candy.name?.lowercased().contains(searchText.lowercased()) ?? false
        })
        tblBranches.reloadData()
    }
    
    fileprivate func searchBarIsEmpty() -> Bool {
        return searchController.searchBar.text?.isEmpty ?? true
    }
    
    fileprivate func isFiltering() -> Bool {
        let searchBarScopeIsFiltering = searchController.searchBar.selectedScopeButtonIndex != 0
        return searchController.isActive && (!searchBarIsEmpty() || searchBarScopeIsFiltering)
    }
    //MARK:- Table view methods
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if isFiltering() {
            searchFooter.setIsFilteringToShow(filteredItemCount: arrFilteredBranches.count, of: arrBranches.count)
            return arrFilteredBranches.count
        }
        
        searchFooter.setNotFiltering()
        return arrBranches.count 
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "cellOutlet")
        
        let arrViews = Bundle.main.loadNibNamed("OutletTVCell", owner: self, options: nil)
        let cellOutlet = arrViews?[1] as? OutletCell
        cellOutlet?.setUpBranchCell(objBranch: isFiltering() ? arrFilteredBranches[indexPath.row] : arrBranches[indexPath.row])
        cellOutlet?.subviews[0].subviews[0].dropShadow(color: .lightGray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 3, scale: true, cellWidth: Int(self.tblBranches.frame.width - 10))
        cell = cellOutlet!
        return cell ?? UITableViewCell()
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 110
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let inOutVC = self.storyboard?.instantiateViewController(withIdentifier: "InOutPopUpVC") as! InOutPopUpVC
        inOutVC.modalPresentationStyle = .overCurrentContext
        inOutVC.modalTransitionStyle = .coverVertical
        
        self.navigationController?.present(inOutVC, animated: true, completion: nil)
        
        inOutVC.onDismissChangedHandler = { (status,data) in
            let strPremise = data as? String
            if strPremise == "InPremise"{
                
                
                if self.isBluetoothOn {
                    let initialViewController = self.storyboard!.instantiateViewController(withIdentifier: "InPremiseLandingVC") as! InPremiseLandingVC
                    initialViewController.objSelectedBranchID = self.isFiltering() ? self.arrFilteredBranches[indexPath.row].id : self.arrBranches[indexPath.row].id
                initialViewController.imgUrl = (self.isFiltering() ? self.arrFilteredBranches[indexPath.row].image_path : self.arrBranches[indexPath.row].image_path) ?? ""
                initialViewController.strLoc = (self.isFiltering() ? self.arrFilteredBranches[indexPath.row].location : self.arrBranches[indexPath.row].location) ?? ""
                    //initialViewController.objSelectedBranch?.category = self.selectedOutlet?.category
                    self.navigationController?.pushViewController(initialViewController, animated: true)
                } else {
                    self.alert(message: "You need to switch on your bluetooth first.", title: "Alert!")
                }
            
            } else {
                let initialViewController = self.storyboard!.instantiateViewController(withIdentifier: "OutPremiseLandingVC") as! OutPremiseLandingVC
                initialViewController.outObj = self.isFiltering() ? self.arrFilteredBranches[indexPath.row] : self.arrBranches[indexPath.row]
                //initialViewController.outObj?.category = self.selectedOutlet?.category
                self.navigationController?.pushViewController(initialViewController, animated: true)
            }
            
        }
    }
    
    //Bluetooth manager delegate
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        switch central.state {
        case .poweredOn:
            isBluetoothOn = true
            break
        case .poweredOff:
            isBluetoothOn = false
            print("Bluetooth is Off.")
            break
        case .resetting:
            break
        case .unauthorized:
            break
        case .unsupported:
            break
        case .unknown:
            break
        default:
            break
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension OutletBranchListVC: UISearchBarDelegate {
    // MARK: - UISearchBar Delegate
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        filterContentForSearchText(searchBar.text!)
    }
}

extension OutletBranchListVC: UISearchResultsUpdating {
    // MARK: - UISearchResultsUpdating Delegate
    func updateSearchResults(for searchController: UISearchController) {
        let searchBar = searchController.searchBar
        //let scope = searchBar.scopeButtonTitles![searchBar.selectedScopeButtonIndex]
        filterContentForSearchText(searchController.searchBar.text!)
    }
}
