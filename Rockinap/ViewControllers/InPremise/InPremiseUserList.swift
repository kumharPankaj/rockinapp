//
//  InPremiseUserList.swift
//  Rockinap
//
//  Created by Pankaj Kumhar on 12/6/18.
//  Copyright © 2018 funndynamix. All rights reserved.
//

import UIKit
//import SQLite

class InPremiseUserList: CommonViewController {

    fileprivate var messages = [JSQMessage]()
    var objSelectedForChatID:String?
    var arrOnlineUsers = [OnlineUser]()
    var isFromBuddies = false
    @IBOutlet weak var tblOnlineUserList: UITableView!
    @IBOutlet weak var lblNoDataFound: UILabel!
    var mySocketId = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        super.backViewNeeded()
        self.title = ""
        
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        arrOnlineUsers.removeAll()
        getCurrentUserList()
        /*SocketIOManager.sharedInstance.getUnreadChatMessage { (messageInfo) -> Void in
            //DispatchQueue.main.async {
            let tempChatArray = self.arrOnlineUsers.filter { (objMessage) -> Bool in
                return objMessage.uuid ==  messageInfo["uuid"] as? String ?? ""
            }
            if tempChatArray.count > 0{
                var uuid = ""
                if let userInformation = UserDefaults.standard.dictionary(forKey: "userInformation") {
                    uuid = userInformation["userID"] as! String
                    
                }
                var dictAck = messageInfo
                dictAck["is_seen"] = 1 as AnyObject
                dictAck["is_deliver"] = 1 as AnyObject
                dictAck["is_window_open"] = 1 as AnyObject
                SocketIOManager.sharedInstance.receiveMsgAck(messageBody: dictAck)
                let message = JSQMessage.init(senderId: messageInfo["uuid"] as? String ?? "", displayName: tempChatArray[0].name, text: messageInfo["message"] as? String ?? "", receiverId: uuid)
                self.messages.append(message!)
                
                let array = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
                let docDir = array[0]
                let docPath = docDir + "/Rockinap.sqlite"
                let db = try! Connection(docPath)
                let stmt = try! db.prepare("INSERT INTO ChatMessage (senderId, receiverId, displayName, message, isImage) VALUES (?,? ,?,?, ?)", messageInfo["uuid"] as? String ?? "", uuid, tempChatArray[0].name, messageInfo["message"] as? String ?? "",0)
                
                try! db.transaction {
                    try stmt.run()
                }
                
                self.tblOnlineUserList.reloadData()
            }
            
            //self.finishReceivingMessage()
        }*/
    }
    func getCurrentUserList()  {
        var userName = ""
        var token = ""
        var uuid = ""
        if let userInformation = UserDefaults.standard.dictionary(forKey: "userInformation") {
            userName = userInformation["name"] as! String
            token = userInformation["token"] as! String
            uuid = userInformation["userID"] as! String
            
            
        }
        if isFromBuddies{
            self.ShowLoader()
            var dict = [String: Any]()
            dict["uuid"] = uuid
            dict["sort_type"] = "asc"
            dict["limit"] = ""
            if self.objSelectedForChatID?.length ?? 0 > 0 {
                dict["outlet_id"] = self.objSelectedForChatID
            }
            APIFunctions.chatBuddyAPICall(dictParams: dict) { (status, resObj) in
                self.HideLoader()
                guard let dictChat = resObj as? [String: AnyObject] else {
                    return
                }
                let arrMsgs = dictChat["data"] as? [[String: AnyObject]]
                if arrMsgs?.count ?? 0 > 0{
                    for msg in arrMsgs! {
                        let tempUser = OnlineUser()
                        tempUser.name = msg["user_name"] as! String
                        tempUser.uuid = msg["buddy"] as! String
                        tempUser.outletName = msg["outlet_name"] as? String
                        self.arrOnlineUsers.append(tempUser)
                        
                    }
                }
            
                self.arrOnlineUsers = self.arrOnlineUsers.filter({ (objUser) -> Bool in
                    return objUser.uuid != uuid
                })
                
                if self.arrOnlineUsers.count > 0{
                    
                    self.lblNoDataFound.isHidden = true
                    self.tblOnlineUserList.isHidden = false
                    self.tblOnlineUserList.reloadData()
                } else {
                    self.lblNoDataFound.text = "No user found!"
                    self.lblNoDataFound.isHidden = false
                    self.tblOnlineUserList.isHidden = true
                }
            }
            
            
            /*let array = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
            let docDir = array[0]
            let docPath = docDir + "/Rockinap.sqlite"

            let db = try! Connection(docPath)
            
            try! db.execute("CREATE TABLE IF NOT EXISTS User (id integer PRIMARY KEY,name text,outletId text,socketId text,uuid text,dnd_status integer);")
            
            try! db.execute("CREATE TABLE IF NOT EXISTS ChatMessage (id integer PRIMARY KEY,senderId text,displayName text,message text,isImage integer, isSeen integer);")
            self.arrOnlineUsers.removeAll()
            for row in try! db.prepare("SELECT * FROM User") {
                let tempUser = OnlineUser()
                tempUser.name = row[1] as! String
                tempUser.outlet_id = row[2] as! String
                tempUser.uuid = row[4] as! String
                tempUser.socket_id = row[3] as! String
                self.arrOnlineUsers.append(tempUser)
            }*/
            
            
        } else {
       
        SocketIOManager.sharedInstance.connectToServerWithUserData(userData: ["name":userName, "access-token":token, "uoid": objSelectedForChatID ?? "1", "dnd_status":false], completionHandler: { (userList) -> Void in
            //dispatch_async(dispatch_get_main_queue(), { () -> Void in
                if userList != nil {
                    self.arrOnlineUsers.removeAll()
                    
                        if (userList?.count)! > 0{
                            let array = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
                            let docDir = array[0]
                            let docPath = docDir + "/Rockinap.sqlite"
                            
                            
//                            let resourcePath = Bundle.main.resourceURL!.absoluteString
//                            let dbPath = resourcePath + "Rockinap.sqlite"
                            
                            /*var strPath = ""
                            if let path = UserDefaults.standard.value(forKey: defaultsKeys.localDbPath){
                                strPath = path as! String
                            } else {
                                
                                strPath = docPath
                                UserDefaults.standard.setValue(strPath, forKey: defaultsKeys.localDbPath)
                            }*/
                            /*let db = try! Connection(docPath)
                            
                            try! db.execute("CREATE TABLE IF NOT EXISTS User (id integer PRIMARY KEY,name text,outletId text,socketId text,uuid text,dnd_status integer);")
                            
                            try! db.execute("CREATE TABLE IF NOT EXISTS ChatMessage (id integer PRIMARY KEY,senderId text, receiverId text, displayName text,message text,isImage integer);")
                            var tempUserArray = [OnlineUser]()
                            for row in try! db.prepare("SELECT * FROM User") {
                                let tempUser = OnlineUser()
                                tempUser.name = row[1] as! String
                                tempUser.outlet_id = row[2] as! String
                                tempUser.uuid = row[4] as! String
                                tempUser.socket_id = row[3] as! String
                                tempUserArray.append(tempUser)
                            }*/
                            
                            for user in userList!{
                                let tempUser = OnlineUser.init(fromDictionary: user as NSDictionary) //try jsonDecoder.decode(OnlineUser.self, from: dataExample)
                                
                                
                                /*if tempUserArray.count == 0{
                                    let stmt = try! db.prepare("INSERT INTO User (name, outletId, socketId, uuid, dnd_status) VALUES (?,? ,?,?,?)", tempUser.name, tempUser.outlet_id, tempUser.socket_id, tempUser.uuid, tempUser.dnd_status)
                                    
                                    try! db.transaction {
                                        try stmt.run()
                                    }
                                } else if !(tempUserArray.contains(where: { $0.uuid == tempUser.uuid })){
                                    let stmt = try! db.prepare("INSERT INTO User (name, outletId, socketId, uuid, dnd_status) VALUES (?,? ,?,?,?)", tempUser.name, tempUser.outlet_id, tempUser.socket_id, tempUser.uuid, tempUser.dnd_status)
                                    
                                    try! db.transaction {
                                        try stmt.run()
                                    }
                                }*/
                                if tempUser.outlet_id == self.objSelectedForChatID{
                                self.arrOnlineUsers.append(tempUser)
                                }
                            }
                            self.arrOnlineUsers = self.arrOnlineUsers.filter({ (objUser) -> Bool in
                                return objUser.uuid != uuid
                            })
                            let obj = self.arrOnlineUsers.filter({ (objUser) -> Bool in
                                return objUser.uuid == uuid
                            })
                            if obj.count > 0 {
                                self.mySocketId = obj[0].socket_id
                            }
                            if self.arrOnlineUsers.count > 0{
                                self.lblNoDataFound.isHidden = true
                                self.tblOnlineUserList.isHidden = false
                                self.tblOnlineUserList.reloadData()
                            } else {
                                self.lblNoDataFound.text = "No user found!"
                                self.lblNoDataFound.isHidden = false
                                self.tblOnlineUserList.isHidden = true
                            }
                        }
                        
                    
//                    self.tblUserList.reloadData()
//                    self.tblUserList.hidden = false
                }
           // })
        })
    }
    }

    //MARK:- Table view methods
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrOnlineUsers.count//arrOutlet.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellUser")
        (cell?.viewWithTag(3) as! UILabel).text = arrOnlineUsers[indexPath.row].name
        (cell?.viewWithTag(2) as! UILabel).text = arrOnlineUsers[indexPath.row].name.first?.description.uppercased()
        let lblCount = cell?.viewWithTag(1) as! UILabel
//        let tempChatArray = self.messages.filter { (objMessage) -> Bool in
//            return objMessage.senderId == self.arrOnlineUsers[indexPath.row].uuid
//        }
//        if tempChatArray.count > 0{
//            lblCount.isHidden = false
//            lblCount.text = "\(tempChatArray.count)"
//        } else {
//            lblCount.isHidden = true
//        }
        if isFromBuddies {
            (cell?.viewWithTag(4) as! UILabel).isHidden = false
            (cell?.viewWithTag(4) as! UILabel).text = arrOnlineUsers[indexPath.row].outletName
        } else {
            (cell?.viewWithTag(4) as! UILabel).isHidden = true
            
        }
        if arrOnlineUsers[indexPath.row].msgCount ?? 0 > 0{
            lblCount.isHidden = false
            lblCount.text = "\(arrOnlineUsers[indexPath.row].msgCount ?? 0)"
        } else {
            lblCount.isHidden = true
        }

        return  cell ?? UITableViewCell()
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return  isFromBuddies ? 80 : 60
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let chatVC = self.storyboard!.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
        chatVC.objChatUser = arrOnlineUsers[indexPath.row]
        chatVC.isUserFromBuddies = isFromBuddies
        if let userInformation = UserDefaults.standard.dictionary(forKey: "userInformation") {
            let userID = userInformation["userID"] as! String
            SocketIOManager.sharedInstance.emitChatHistory(userData: ["sender_id":userID, "receiver_uuid":arrOnlineUsers[indexPath.row].uuid])
            self.navigationController?.pushViewController(chatVC, animated: true)
        }
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
