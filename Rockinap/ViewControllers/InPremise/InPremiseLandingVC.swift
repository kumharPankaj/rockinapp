//
//  InPremiseLandingVC.swift
//  Rockinap
//
//  Created by orionspica on 07/10/18.
//  Copyright © 2018 funndynamix. All rights reserved.
//

import UIKit
import Kingfisher
import MBProgressHUD
import SocketIO

class InPremiseLandingVC: CommonViewController {
    @IBOutlet weak var viewOffers: UIView!
    @IBOutlet weak var viewGames: UIView!
    @IBOutlet weak var viewNetwork: UIView!
    @IBOutlet weak var viewLoyalty: UIView!
    private var layer1:CAShapeLayer?
    private var layer2:CAShapeLayer?
    private var layer3:CAShapeLayer?
    private var layer4:CAShapeLayer?
    @IBOutlet weak var imgViewOutlet: UIImageView!
    
    @IBOutlet weak var viewBottomConst: NSLayoutConstraint!
    var objSelectedBranchID:String?
    @IBOutlet weak var imgSelectedGlobeOutlet: UIImageView!
    private var items = [Item]()
    private let locationManager = CLLocationManager()
    @IBOutlet weak var baseView: UIView!
    var rippleView: SMRippleView?
    var isBeconFound = false
    var detectedBeacon: Item?
    @IBOutlet weak var lblOutletLoc: UILabel!
    private var seconds = 0 //This variable will hold a starting value of seconds. It could be any amount above 0.
    private var timer = Timer()
    private var isTimerRunning = true
    var imgUrl = ""
    var isBackPress = true
    var strLoc = ""
    override func viewDidLoad() {
        super.viewDidLoad()

        var userID = ""
        if let userInformation = UserDefaults.standard.dictionary(forKey: "userInformation") {
            userID = userInformation["mobile_number"] as! String
        }
        AppDelegate.shareddelegate().liveBranchId = objSelectedBranchID ?? ""
        AppDelegate.shareddelegate().currentUserId = userID
//        NotificationCenter.default.addObserver(self, selector: #selector(willResignActive), name: UIApplication.didBecomeActiveNotification, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(willEnterBackground), name: UIApplication.willResignActiveNotification, object: nil)
        super.backViewNeeded()
        let url = URL(string: imgUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? "" )
        self.imgViewOutlet.kf.setImage(with: url, placeholder:nil )
        //self.view.isUserInteractionEnabled = false
        SocketIOManager.sharedInstance.establishConnection()
        
        locationManager.delegate = self
        //locationManager.requestAlwaysAuthorization()
      
        loadItems()
        // Do any additional setup after loading the view.
    }
    
    @objc func willResignActive() {
        // code to execute
        if !isTimerRunning {
            self.addCurrentUserCall()
        }
    }
    @objc func willEnterBackground() {
        // code to execute
        print("background")
        var userID = ""
        if let userInformation = UserDefaults.standard.dictionary(forKey: "userInformation") {
            userID = userInformation["mobile_number"] as! String
        }
        SocketIOManager.sharedInstance.closeConnection()
        APIFunctions.leaveOutlet(dictParams: ["outlet_id":objSelectedBranchID ?? "", "mobile_number":userID]) { (status, obj) in
            
        }
    }
    override func viewDidLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        viewBottomConst.constant = (self.navigationController?.navigationBar.frame.origin.y ?? 0.0 ) + (self.navigationController?.navigationBar.frame.height ?? 0.0)
        
        imgSelectedGlobeOutlet.layer.cornerRadius = imgSelectedGlobeOutlet.frame.width / 2
        imgSelectedGlobeOutlet.clipsToBounds = true
        //let url = URL(string: objSelectedBranch!.image_path ?? "")
        //self.imgSelectedGlobeOutlet.kf.setImage(with: url, placeholder:UIImage.init(named: "loading") )
    }
    override func viewWillAppear(_ animated: Bool) {
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        var userID = ""
        if let userInformation = UserDefaults.standard.dictionary(forKey: "userInformation") {
            userID = userInformation["mobile_number"] as! String
        }
        if isBackPress {
            AppDelegate.shareddelegate().isUserInpremise = false
            SocketIOManager.sharedInstance.closeConnection()
            APIFunctions.leaveOutlet(dictParams: ["outlet_id":objSelectedBranchID ?? "", "mobile_number":userID]) { (status, obj) in
                
            }
        }
        rippleView?.stopAnimation()
        self.baseView.isHidden = true
    }
    override func viewWillLayoutSubviews() {
        //if !isLinesDrawn {
        if isTimerRunning {
            layer1?.removeFromSuperlayer()
            layer2?.removeFromSuperlayer()
            layer3?.removeFromSuperlayer()
            layer4?.removeFromSuperlayer()
            layer1 = self.drawLineForDashoard(xPosStart: Int(viewOffers.frame.origin.x + viewOffers.frame.width), yPosStart: Int(viewOffers.frame.origin.y + (viewOffers.frame.height/2) - 10), xPosEnd: Int(viewOffers.frame.origin.x + viewOffers.frame.width + 50), yPosEnd: Int(viewOffers.frame.origin.y + (viewOffers.frame.height/2) - 10))
            layer2 = (self.drawLineForDashoard(xPosStart: Int(viewOffers.frame.origin.x + viewOffers.frame.width + 50), yPosStart: Int(viewOffers.frame.origin.y + (viewOffers.frame.height/2) - 10), xPosEnd: Int(imgSelectedGlobeOutlet.frame.origin.x + 50), yPosEnd: Int(viewOffers.frame.origin.y + (viewOffers.frame.height/2) + 20)))
            
            layer3 = self.drawLineForDashoard(xPosStart: Int(viewNetwork.frame.origin.x + viewNetwork.frame.width), yPosStart: Int(viewNetwork.frame.origin.y + (viewNetwork.frame.height/2) + 10), xPosEnd: Int(viewNetwork.frame.origin.x + viewNetwork.frame.width + 50), yPosEnd: Int(viewNetwork.frame.origin.y + (viewNetwork.frame.height/2) + 10))
            layer4 = (self.drawLineForDashoard(xPosStart: Int(viewNetwork.frame.origin.x + viewNetwork.frame.width + 50), yPosStart: Int(viewNetwork.frame.origin.y + (viewNetwork.frame.height/2) + 10), xPosEnd: Int(imgSelectedGlobeOutlet.frame.origin.x + 50), yPosEnd: Int(viewNetwork.frame.origin.y + (viewNetwork.frame.height/2) - 20)))
            
            view.layer.addSublayer(layer1!)
            view.layer.addSublayer(layer2!)
            view.layer.addSublayer(layer3!)
            view.layer.addSublayer(layer4!)
            view.bringSubviewToFront(imgSelectedGlobeOutlet)
            imgViewOutlet.layer.cornerRadius = imgViewOutlet.frame.width / 2
            view.bringSubviewToFront(imgViewOutlet)
            view.bringSubviewToFront(lblOutletLoc)
            lblOutletLoc.text = strLoc
            let fillColor: UIColor? = UIColor.darkGray//UIColor(red: 33/255, green: 150/255, blue: 243/255, alpha: 1)
            rippleView = SMRippleView(frame: baseView.bounds, rippleColor: UIColor.darkGray, rippleThickness: 0.2, rippleTimer: 0.6, fillColor: fillColor, animationDuration: 4, parentFrame: self.view.frame)
            self.baseView.addSubview(rippleView!)
            self.baseView.isHidden = false
            view.bringSubviewToFront( self.baseView)
        }
        
        //}
    }
    
    @IBAction func btnInPremiseOptionClicked(_ sender: UIButton) {
        if isTimerRunning {
            self.alert(message: "Please detect an inpremise beacon first.", title: "Alert")
        } else {
            switch sender.tag {
            case 1:
                isBackPress = false
                let gamesVC = self.storyboard!.instantiateViewController(withIdentifier: "GameListVc") as! GameListVc
                gamesVC.objBranchSelectedID = objSelectedBranchID ?? ""
                gamesVC.objDetectedBeacon = detectedBeacon
                gamesVC.arrBeacons = self.items
                self.navigationController?.pushViewController(gamesVC, animated: true)
            case 2:
                isBackPress = false
                let offersVC = self.storyboard!.instantiateViewController(withIdentifier: "OffersListVC") as! OffersListVC
                offersVC.objSelectedForOffersID = objSelectedBranchID ?? ""
                self.navigationController?.pushViewController(offersVC, animated: true)
                selectedCategory = OutleCategory.ChilinOut
            case 3:
                
                let alertController = UIAlertController(title:"Disclaimer",message:"By clicking the Network you have chosen to text and receive text from the other Rockinapers within this premise.\nYou can choose not to receive text by not activating the Network or rejecting this disclaimer.\n We advice not to exchange your contact of any oher personal details with each other.\nStrictly there is no tolerance for objectionable content or abusive users.\nIn case a need arises you may report to the available Manager of the Premise." , preferredStyle:.alert)
                let firstAction = UIAlertAction(title: "Agree", style: .default) { (alert: UIAlertAction!) -> Void in
                    alertController.dismiss(animated: true, completion: nil)
                    self.isBackPress = false
                    let usersVC = self.storyboard!.instantiateViewController(withIdentifier: "InPremiseUserList") as! InPremiseUserList
                    usersVC.objSelectedForChatID = self.objSelectedBranchID ?? ""
                    self.navigationController?.pushViewController(usersVC, animated: true)
                }
                let cancelAction = UIAlertAction(title: "Disagree", style: .default) { (alert: UIAlertAction!) -> Void in
                    alertController.dismiss(animated: true, completion: nil)
                }
                alertController.addAction(firstAction)
                alertController.addAction(cancelAction)
                self.present(alertController, animated: true, completion: nil)
                
                
                
                //            let gamesVC = self.storyboard!.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
            //            self.navigationController?.pushViewController(gamesVC, animated: true)
            case 4:
                isBackPress = false
                let initialViewController = self.storyboard!.instantiateViewController(withIdentifier: "InPremiseUserList") as! InPremiseUserList
                initialViewController.isFromBuddies = true
                initialViewController.objSelectedForChatID = objSelectedBranchID ?? ""
                self.navigationController?.pushViewController(initialViewController, animated: true)
                /*let treasureHuntVC = self.storyboard!.instantiateViewController(withIdentifier: "SearchBeaconVC") as! SearchBeaconVC
                 treasureHuntVC.selectedThBranch = self.objSelectedBranch
                 self.navigationController?.pushViewController(treasureHuntVC, animated: true)*/
            default:
                print("default")
            }
        }
        
    }
    
    func loadItems() {
        APIFunctions.beaconListAPI_Call(dictParams: ["outlet_id":"\(objSelectedBranchID ?? "")"]) { (status, obj) in
            
            if let list = obj as? [Item]{
                self.items = list
//                AppDelegate.shareddelegate().isUserInpremise = true
//                self.detectedBeacon = self.items[5]
//                self.addCurrentUserCall()
                for obj in list{
                    self.startMonitoringItem(obj)
                }
            }
        }
    }
    private func startMonitoringItem(_ item: Item) {
        let beaconRegion = item.asBeaconRegion()
        locationManager.startMonitoring(for: beaconRegion)
        locationManager.startRangingBeacons(in: beaconRegion)
    }
    private func stopMonitoringItem(_ item: Item) {
        let beaconRegion = item.asBeaconRegion()
        locationManager.stopMonitoring(for: beaconRegion)
        locationManager.stopRangingBeacons(in: beaconRegion)
    }
    
    
    func runTimer() {
        //        DispatchQueue.global().async {
        self.timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(self.updateTimer)), userInfo: nil, repeats: true)
        //        }
    }
    @objc func updateTimer() {
        if seconds > 120{
            if !isTimerRunning{
                self.timer.invalidate()
                self.alert(message: "No ", title: "")
            } else {
                self.timer.invalidate()
            }
        } else {
            
        }
        
    }
    @IBAction func btnHomeClicked(_ sender: UIButton) {
        for vc in (self.navigationController?.viewControllers ?? []) {
            if vc is HomeTabVC {
                isBackPress = false
                AppDelegate.shareddelegate().isUserInpremise = false
                var userID = ""
                if let userInformation = UserDefaults.standard.dictionary(forKey: "userInformation") {
                    userID = userInformation["mobile_number"] as! String
                }
                SocketIOManager.sharedInstance.closeConnection()
                APIFunctions.leaveOutlet(dictParams: ["outlet_id":objSelectedBranchID ?? "", "mobile_number":userID]) { (status, obj) in
                    _ = self.navigationController?.popToViewController(vc, animated: true)
                    
                }
                break
            }
        }
        
    }
    @IBAction func btnNotificationClicked(_ sender: UIButton) {
        isBackPress = false
        let initialViewController = self.storyboard!.instantiateViewController(withIdentifier: "NotifcationVC") as! NotifcationVC
        initialViewController.isFromOutlet = true
        initialViewController.notiOutletID = objSelectedBranchID!
        self.navigationController?.pushViewController(initialViewController, animated: true)
    }
    private func addCurrentUserCall()  {
        if let userInformation = UserDefaults.standard.dictionary(forKey: "userInformation") {
            let userID = userInformation["mobile_number"] as! String
            let uuid = userInformation["userID"] as! String
            self.ShowLoader()
            APIFunctions.addInCurrentUser(dictParams: ["uoid":"\(objSelectedBranchID ?? "")","uuid": uuid, "mobile_number":userID, "beacon_name":detectedBeacon?.name ?? ""]) { (status, obj) in
                self.HideLoader()
                
                self.ShowLoader()
                APIFunctions.getOffersList(dictParams: ["outlet_id":self.objSelectedBranchID ?? "", "beacon_name": self.detectedBeacon?.name ?? "","mobile_number":userID, "limit": 1]) { (status, resObj) in
                    self.HideLoader()
                    
                    if let objOutles = resObj as? OffersModel{
                        if objOutles.offersList?.count ?? 0 > 0 {
                            //if objOutles.offersList?[0].isAutoTriggerd == 1 {
                                if let preDate: Date = UserDefaults.standard.value(forKey: "timeAutoTrigger") as? Date {
                                    let currentDate = Date()
                                    let timeInterval = currentDate.timeIntervalSince(preDate)
                                    if ((timeInterval / 60) / 60) > 24 {
                                        self.ShowLoader()
                                        APIFunctions.updateOfferHistory(dictParams: ["outlet_id":self.objSelectedBranchID ?? "", "mobile_number":userID , "offer_type": "normal", "offer_code":objOutles.offersList?[0].offer_code ?? "1234", "is_auto_triggered":"1"]) { (status, resObj) in
                                            self.HideLoader()
                                            UserDefaults.standard.set(Date(), forKey: "timeAutoTrigger")
                                            UserDefaults.standard.synchronize()
                                            let alertController = UIAlertController(title:"Here it will be 'Your Offer Code!'" , message:"\(objOutles.offersList?[0].offer_code ?? "1234")" , preferredStyle:.alert)
                                            let firstAction = UIAlertAction(title: "Ok", style: .default) { (alert: UIAlertAction!) -> Void in
                                                alertController.dismiss(animated: true, completion: nil)
                                                
                                            }
                                            alertController.addAction(firstAction)
                                            self.present(alertController, animated: true, completion: nil)
                                        }
                                    }
                                    
                                } else {
                                    self.ShowLoader()
                                    APIFunctions.updateOfferHistory(dictParams: ["outlet_id":self.objSelectedBranchID ?? "", "mobile_number":userID , "offer_type": "normal", "offer_code":objOutles.offersList?[0].offer_code ?? "1234", "is_auto_triggered":"1"]) { (status, resObj) in
                                         self.HideLoader()
                                        UserDefaults.standard.set(Date(), forKey: "timeAutoTrigger")
                                        UserDefaults.standard.synchronize()
                                        let alertController = UIAlertController(title:"Here it will be 'Your Offer Code!'" , message:"\(objOutles.offersList?[0].offer_code ?? "1234")" , preferredStyle:.alert)
                                        let firstAction = UIAlertAction(title: "Ok", style: .default) { (alert: UIAlertAction!) -> Void in
                                            alertController.dismiss(animated: true, completion: nil)
                                            
                                        }
                                        alertController.addAction(firstAction)
                                        self.present(alertController, animated: true, completion: nil)
                                       
                                    }
                                }
                          //  }
                        }
                    }
                }
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension InPremiseLandingVC: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, monitoringDidFailFor region: CLRegion?, withError error: Error) {
        print("Failed monitoring region: \(error.localizedDescription)")
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Location manager failed: \(error.localizedDescription)")
    }
    
    func locationManager(_ manager: CLLocationManager, didRangeBeacons beacons: [CLBeacon], in region: CLBeaconRegion) {
        
        // Find the same beacons in the table.
        var indexPaths = [IndexPath]()
        print("beacon--->",beacons.count)
        for beacon in beacons {
            for row in 0..<items.count {
                if items[row] == beacon {
                    print(beacon.proximity)
                    if items[row].nameForProximity(beacon.proximity) == "Far" || items[row].nameForProximity(beacon.proximity) == "Near" || items[row].nameForProximity(beacon.proximity) == "Immediate"{
                        for obj in self.items{
                            self.stopMonitoringItem(obj)
                        }
                        detectedBeacon = items[row]
                        AppDelegate.shareddelegate().detectedBeaconName = detectedBeacon?.name ?? ""
                        rippleView?.stopAnimation()
                        self.baseView.isHidden = true
                        isTimerRunning = false
                        AppDelegate.shareddelegate().isUserInpremise = true
                        self.addCurrentUserCall()
                        self.view.isUserInteractionEnabled = true
                        
                        break
                    } else {
                        items[row].beacon = beacon
                        print(items[row].locationString())
                        indexPaths += [IndexPath(row: row, section: 0)]
                    }
                    
                }
            }
        }
        
        // Update beacon locations of visible rows.
        /*if let visibleRows = tableView.indexPathsForVisibleRows {
            let rowsToUpdate = visibleRows.filter { indexPaths.contains($0) }
            for row in rowsToUpdate {
                let cell = tableView.cellForRow(at: row) as! ItemCell
                cell.refreshLocation()
            }
        }*/
    }
}
