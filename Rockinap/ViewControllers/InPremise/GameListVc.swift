//
//  GameListVc.swift
//  Rockinap
//
//  Created by orionspica on 07/10/18.
//  Copyright © 2018 funndynamix. All rights reserved.
//

import UIKit

class GameListVc: CommonViewController {
    
    var objBranchSelectedID = ""
    var arrGames = [["img": "tresure-hunt-icon-1","name":"Treasure Hunt"], ["img": "quiz-logo-1", "name":"Quiz Game"]]
    var objDetectedBeacon: Item?
    var arrBeacons = [Item]()
    @IBOutlet weak var tblGames: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        tblGames.isHidden = true
        super.backViewNeeded()
        self.callSettingsAPI()
        // Do any additional setup after loading the view.
    }
    
    private func callSettingsAPI() {
        self.ShowLoader()
        APIFunctions.getSettings(dictParams: ["outlet_id":objBranchSelectedID]) { (status, obj) in
            self.HideLoader()
            if let objOutles = obj as? SettingsModel {
                if objOutles.data?.count ?? 0 > 0 {
                    if let objSetting = objOutles.data?.filter({ (setting) -> Bool in
                        setting.setting_key == "treasurehunt" || setting.setting_key == "Treasure Hunt"
                    })[0] {
                        if objSetting.value == 0 {
                            self.arrGames = [["img": "quiz-logo-1", "name":"Quiz Game"]]
                        }
                    }
                } else {
                    self.arrGames = [["img": "quiz-logo-1", "name":"Quiz Game"]]
                }
                self.tblGames.reloadData()
                self.tblGames.isHidden = false
            } else {
                self.tblGames.reloadData()
                self.tblGames.isHidden = false
            }
            print("")
        }
    }
    
    //MARK:- Table view methods
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrGames.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellGames")
        let imgView = cell?.viewWithTag(1) as! UIImageView
        let lblGameName = cell?.viewWithTag(2) as! UILabel
        
        let obj = arrGames[indexPath.row]
        
        imgView.image = UIImage.init(named: obj["img"]!)
        lblGameName.text = obj["name"]!
        return cell ?? UITableViewCell()
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellGames")
        return (cell?.frame.height)!
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if arrGames.count == 2 {
            if indexPath.row == 0{
                if let preDate: Date = UserDefaults.standard.value(forKey: "time") as? Date {
                    let currentDate = Date()
                    let timeInterval = currentDate.timeIntervalSince(preDate)
                    if ((timeInterval / 60) / 60) > 24 {
                        let initialViewController = self.storyboard!.instantiateViewController(withIdentifier: "SearchBeaconVC") as! SearchBeaconVC
                        initialViewController.selectedThBranchID = objBranchSelectedID
                        initialViewController.presentBeaconsInOutlet = arrBeacons
                        self.navigationController?.pushViewController(initialViewController, animated: true)
                    } else {
                        self.alert(message: "Next TH will be available after 24 hours.", title: "Alert")
                    }
                } else {
                    let initialViewController = self.storyboard!.instantiateViewController(withIdentifier: "SearchBeaconVC") as! SearchBeaconVC
                    initialViewController.selectedThBranchID = objBranchSelectedID
                    initialViewController.presentBeaconsInOutlet = arrBeacons
                    self.navigationController?.pushViewController(initialViewController, animated: true)
                }
                
            } else if indexPath.row == 1{
                let initialViewController = self.storyboard!.instantiateViewController(withIdentifier: "QuizQuestionsVC") as! QuizQuestionsVC
                initialViewController.objSelectedQuizBranchID = objBranchSelectedID
                initialViewController.objBeaconDetected = self.objDetectedBeacon
                self.navigationController?.pushViewController(initialViewController, animated: true)
            }
            
        } else {
            let initialViewController = self.storyboard!.instantiateViewController(withIdentifier: "QuizQuestionsVC") as! QuizQuestionsVC
            initialViewController.objSelectedQuizBranchID = objBranchSelectedID
            initialViewController.objBeaconDetected = self.objDetectedBeacon
            self.navigationController?.pushViewController(initialViewController, animated: true)
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
