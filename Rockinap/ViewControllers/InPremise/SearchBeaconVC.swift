//
//  SearchBeaconVC.swift
//  Rockinap
//
//  Created by Pankaj Kumhar on 11/7/18.
//  Copyright © 2018 funndynamix. All rights reserved.
//

import UIKit
import CoreLocation
class SearchBeaconVC: CommonViewController {

    private var items = [Item]()
    private let locationManager = CLLocationManager()
    var selectedThBranchID = ""
    var arrTHGames = [THGame]()
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var txtDescription: UITextView!
    @IBOutlet weak var lblClue: UILabel!
    @IBOutlet weak var lblBeaconNumber: UILabel!
    @IBOutlet weak var lblRemainingTime: UILabel!
    @IBOutlet weak var progressBarTH: UIProgressView!
    private var progressValue = 0.0
    private var seconds = 300 //This variable will hold a starting value of seconds. It could be any amount above 0.
    private var timer = Timer()
    private var isTimerRunning = false //This will be used to make sure only one timer is created at a time.
    var presentBeaconsInOutlet = [Item]()
    var beaconToFind: Item?
    private var gameId = 0
    private var isStatusFalse = false
    override func viewDidLoad() {
        super.viewDidLoad()
        super.backViewNeeded()
        locationManager.delegate = self
        locationManager.requestAlwaysAuthorization()

        var userID = ""
        if let userInformation = UserDefaults.standard.dictionary(forKey: "userInformation") {
            userID = userInformation["userID"] as! String
        }
        self.ShowLoader()
        APIFunctions.thStatus_Call(dictParams: ["uuid":userID, "outlet_id":selectedThBranchID, "game_name": "general","win_status":0]) { (status, resObj) in
            self.HideLoader()
            if let objStatus = resObj as? [String: Any] {
                if status {
                    let objData = objStatus["data"] as? [String: Any]
                    self.gameId = objData?["game_id"] as? Int ?? 0
                    self.getBeaconData()
                    
                } else {
                    self.getBeaconData()
                    self.isStatusFalse = true
                    self.alert(message: objStatus["message"] as! String, title: "Alert")
                    self.navigationController?.popViewController(animated: true)
                    
                }
                
            }
        }
        // Do any additional setup after loading the view.
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        var userID = ""
        if let userInformation = UserDefaults.standard.dictionary(forKey: "userInformation") {
            userID = userInformation["userID"] as! String
        }
        if !isStatusFalse {
            APIFunctions.thStatus_Call(dictParams: ["game_id":self.gameId, "uuid":userID, "outlet_id":selectedThBranchID,"status":"unfinish", "game_name": "general","win_status":0]) { (status, resObj) in
                if let objStatus = resObj as? [String: Any] {
                    
                }
            }
        }
        
    }
    func getBeaconData() {
        self.ShowLoader()
        APIFunctions.getTHGameWithBeaconDetails(dictParams: ["outlet_id":"\(selectedThBranchID)", "limit":"1"]) { (status, obj) in
            
            self.HideLoader()
            if let objOutles = obj as? THModel{
                self.arrTHGames = (objOutles.data?[0].gameList)!
                self.viewDidLayoutSubviews()
                if self.arrTHGames.count > 0{
                    self.setUpUI()
                    let objBeacon = self.presentBeaconsInOutlet.filter({ (obj) -> Bool in
                        return obj.name == self.arrTHGames[0].beacon_name
                    })
                    //let minor = UInt8((beacon.minorValue ?? "0000"), radix: 16) ?? 0
                    //let major = UInt8((beacon.majorValue ?? "0000"), radix: 16) ?? 0
                    //let beacon = Item.init(name: self.arrTHGames[0].clue ?? "Beacon", icon: 1, uuid: UUID(uuidString: objBeacon.uuid ?? "abc1abc2-abc3-abc4-abc5-abc6abc7abc8")! , majorValue:objBeacon.majorValue , minorValue:objBeacon.minorValue )
                    if objBeacon.count > 0 {
                        self.gameId = self.arrTHGames[0].game_id ?? 0
                    self.beaconToFind = objBeacon[0]
                    self.items.append(objBeacon[0])
                    self.startMonitoringItem(objBeacon[0])
                    self.runTimer()
                    }
                }
                
            } else {
                self.alert(message: obj as? String ?? "Somthing went wrong", title: "Alert")
            }
            /*if let list = obj as? [Item]{
                self.items = list
                for obj in list{
                    self.startMonitoringItem(obj)
                }
            }*/
        }
        
    }
    func setUpUI() {
        lblTitle.text = arrTHGames[0].question
        txtDescription.text = arrTHGames[0].description
        lblClue.text = arrTHGames[0].clue
        lblBeaconNumber.text = arrTHGames[0].beacon_name
    }
    
    func runTimer() {
        //        DispatchQueue.global().async {
        self.timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(self.updateTimer)), userInfo: nil, repeats: true)
        //        }
    }
    @objc func updateTimer() {
        if seconds <= 300 && seconds >= 0{
            seconds -= 1
            progressValue += 1
            lblRemainingTime.text = timeString(time: TimeInterval(seconds)) //This will update the label.
            //updateStatus(status: "finish")
            UIView.animate(withDuration: 3, animations: { () -> Void in
                self.progressBarTH.setProgress(Float(((self.progressValue/90) * 100)/100), animated: true)
            })
        } else {
            timer.invalidate()
            stopMonitoringItem(beaconToFind!)
            //updateStatus(status: "finish")
            let alertController = UIAlertController(title:"OOPS!\n 😋" , message: arrTHGames[0].loosing_msg ?? "", preferredStyle:.alert)
            let firstAction = UIAlertAction(title: "Ok", style: .default) { (alert: UIAlertAction!) -> Void in
                alertController.dismiss(animated: true, completion: nil)
                self.navigationController?.popViewController(animated: true)
            }
            alertController.addAction(firstAction)
            self.present(alertController, animated: true, completion: nil)
        }
        
    }
    private func timeString(time:TimeInterval) -> String {
        let minutes = Int(time) / 60 % 60
        let seconds = Int(time) % 60
        return String(format:"%02i:%02i", minutes, seconds)
    }
    //Mark:- Beacon searching
    private func startMonitoringItem(_ item: Item) {
        let beaconRegion = item.asBeaconRegion()
        locationManager.startMonitoring(for: beaconRegion)
        locationManager.startRangingBeacons(in: beaconRegion)
    }
    private func stopMonitoringItem(_ item: Item) {
        let beaconRegion = item.asBeaconRegion()
        locationManager.stopMonitoring(for: beaconRegion)
        locationManager.stopRangingBeacons(in: beaconRegion)
    }
    
    private func updateStatus(status: String) {
        var userID = ""
        if let userInformation = UserDefaults.standard.dictionary(forKey: "userInformation") {
            userID = userInformation["userID"] as! String
        }
        self.ShowLoader()
        APIFunctions.thStatus_Call(dictParams: ["uuid":userID, "outlet_id":selectedThBranchID,"game_id":self.gameId,"status":status, "game_name": "general","win_status":0]) { (status, resObj) in
            self.HideLoader()
            if let objStatus = resObj as? [String: Any] {
                let objData = objStatus["data"] as? [String: Any]
                self.isStatusFalse = true
                print("")
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension SearchBeaconVC: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, monitoringDidFailFor region: CLRegion?, withError error: Error) {
        print("Failed monitoring region: \(error.localizedDescription)")
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Location manager failed: \(error.localizedDescription)")
    }
    func locationManager(_ manager: CLLocationManager, didRangeBeacons beacons: [CLBeacon], in region: CLBeaconRegion) {
        
        // Find the same beacons in the table.
        
        //var indexPaths = [IndexPath]()
        for beacon in beacons {
            for row in 0..<items.count {
                if items[row] == beacon {
                    if items[row].nameForProximity(beacon.proximity) == "Immediate" || items[row].nameForProximity(beacon.proximity) == "Near"{
                        for obj in self.items {
                            UserDefaults.standard.set(Date(), forKey: "time")
                            UserDefaults.standard.synchronize()
                            self.stopMonitoringItem(obj)
                            
                            var userID = ""
                            if let userInformation = UserDefaults.standard.dictionary(forKey: "userInformation") {
                                userID = userInformation["userID"] as! String
                            }
                            self.ShowLoader()
                            APIFunctions.thStatus_Call(dictParams: ["uuid":userID, "outlet_id":selectedThBranchID,"game_id":self.gameId,"status":"finish", "game_name": "general","win_status":1]) { (status, resObj) in
                                self.HideLoader()
                                if let objStatus = resObj as? [String: Any] {
                                    self.isStatusFalse = true
                                }
                                let bingoVC = self.storyboard!.instantiateViewController(withIdentifier: "BingoPopUpVC") as! BingoPopUpVC
                                bingoVC.modalPresentationStyle = .overCurrentContext
                                bingoVC.modalTransitionStyle = .coverVertical
                                
                                bingoVC.msgLoosing = self.arrTHGames[0].winning_msg ?? ""
                                bingoVC.outletID = self.selectedThBranchID
                                bingoVC.backToGameVC = {
                                    print($0)
                                    bingoVC.dismiss(animated: true, completion: nil)
                                    for controller in self.navigationController!.viewControllers as Array {
                                        if controller.isKind(of: GameListVc.self) {
                                            self.navigationController!.popToViewController(controller, animated: true)
                                            break
                                        }
                                    }
                                    
                                }
                                self.navigationController?.present(bingoVC, animated: true, completion: nil)
                            }
                            
                            
                            
                            
                        }
                        break
                    } else {
//                        items[row].beacon = beacon
//                        print(items[row].locationString())
//                        indexPaths += [IndexPath(row: row, section: 0)]
                    }
                    
                }
            }
        }
        
        // Update beacon locations of visible rows.
        /*if let visibleRows = tableView.indexPathsForVisibleRows {
            let rowsToUpdate = visibleRows.filter { indexPaths.contains($0) }
            for row in rowsToUpdate {
                let cell = tableView.cellForRow(at: row) as! ItemCell
                cell.refreshLocation()
            }
        }*/
    }
}
