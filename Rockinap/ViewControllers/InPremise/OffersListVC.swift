//
//  OffersListVC.swift
//  Rockinap
//
//  Created by Pankaj Kumhar on 1/6/19.
//  Copyright © 2019 funndynamix. All rights reserved.
//

import UIKit
import CoreBluetooth
class OffersListVC: CommonViewController, CBCentralManagerDelegate, CLLocationManagerDelegate  {

    private var manager = CLLocationManager()
    private var bluetoothManager:CBCentralManager!
    private var isBluetoothOn = false
    var arrOffers = [Offers]()
    @IBOutlet weak var tblOffers: UITableView!
    var objSelectedForOffersID:String?
    var offersGlobal = false
    private var currentLocation: CLLocation?
    override func viewDidLoad() {
        super.viewDidLoad()
        super.backViewNeeded()
        
        self.title = ""
        manager.delegate = self
        manager.requestAlwaysAuthorization()
        
        if( CLLocationManager.authorizationStatus() == .authorizedWhenInUse ||
            CLLocationManager.authorizationStatus() ==  .authorizedAlways){
            
            currentLocation = manager.location
            
        }
        getOffersData()
        // Do any additional setup after loading the view.
    }
    

    func getOffersData(){
        //["outlet_id":objSelectedForOffers?.id ?? "", "limit":10, "lat":currentLocation.coordinate.latitude,"lon":currentLocation.coordinate.longitude]
        //        {
//        "offer_type":"normal",
//        "lat":"23.06",
//        "lon":"72.56"
//    }
        var dictParams = [String: Any]()
        if objSelectedForOffersID == nil {
            dictParams["offer_type"] = "normal"
            dictParams["lat"] = currentLocation?.coordinate.latitude ?? 0.0//19.2288//
            dictParams["lon"] = currentLocation?.coordinate.longitude ?? 0.0//72.8323//
        } else {
            dictParams["outlet_id"] = objSelectedForOffersID ?? ""
        }
        self.ShowLoader()
        APIFunctions.getOffersList(dictParams: dictParams) { (status, resObj) in
            self.HideLoader()
            if let objOutles = resObj as? OffersModel{
                self.arrOffers = objOutles.offersList!
                if self.arrOffers.count > 0 {
                    self.viewDidLayoutSubviews()
                    self.tblOffers.setNeedsLayout()
                    self.tblOffers.layoutIfNeeded()
                    self.tblOffers.reloadData()
                } else {
                    self.alert(message: resObj as? String ?? "No offers found", title: "Alert")
                }
            } else {
                let alertController = UIAlertController(title:"Alert!" , message: "Please login again to enjoy Rockinap.", preferredStyle:.alert)
                let firstAction = UIAlertAction(title: "Logout", style: .default) { (alert: UIAlertAction!) -> Void in
                    if let token = UserDefaults.standard.value(forKey: defaultsKeys.fcmToken) {
                        var user = ""
                        if let userInformation = UserDefaults.standard.dictionary(forKey: "userInformation") {
                            user = userInformation["userID"] as! String
                            
                        }
                        let id:String = ""
                        self.ShowLoader()
                        APIFunctions.logoutAPICall(dictParams: ["uuid":user, "device_id":id.getUUID() ?? ""]) { (status, resObj) in
                            //            objBeaconDetected?.name ?? "1"
                            
                            self.HideLoader()
                            self.resetDefaults()
                            DrawerRootViewController.shared().logout()
                            //self.navigationController?.popViewController(animated: true)
                        }
                        
                    } else {
                        self.resetDefaults()
                        DrawerRootViewController.shared().logout()
                    }
                    
                    
                    alertController.dismiss(animated: true, completion: nil)
                }
                
                alertController.addAction(firstAction)
                self.present(alertController, animated: true, completion: nil)
            }
        }
    }
    //Bluetooth manager delegate
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        switch central.state {
        case .poweredOn:
            isBluetoothOn = true
            break
        case .poweredOff:
            isBluetoothOn = false
            print("Bluetooth is Off.")
            break
        case .resetting:
            break
        case .unauthorized:
            break
        case .unsupported:
            break
        case .unknown:
            break
        default:
            break
        }
    }
    //MARK:- Table view methods
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrOffers.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellOffers") as! OfferTVCell
        cell.setUpOffersCell(objOutlet: arrOffers[indexPath.row])
        cell.dropCownClicked = { cell in
            self.arrOffers[indexPath.row].isTapped = self.arrOffers[indexPath.row].isTapped ? false : true
            cell.btnDropDown.setImage(self.arrOffers[indexPath.row].isTapped ? UIImage.init(named: "arrowUp") : UIImage.init(named: "arrowDown"), for: .normal)
            self.tblOffers.beginUpdates()
            self.tblOffers.endUpdates()
        }
        cell.redeemButtonClicked = { cell in
            //self.alert(message: "Please show your offer code:\(self.arrOffers[indexPath.row].offer_code ?? "") to admin", title: "Offer Code")
            let alertController = UIAlertController(title:"Offer Code" , message:"Please show your offer code:\(self.arrOffers[indexPath.row].offer_code ?? "") to admin" , preferredStyle:.alert)
            let firstAction = UIAlertAction(title: "Ok", style: .default) { (alert: UIAlertAction!) -> Void in
                var userID = ""
                if let userInformation = UserDefaults.standard.dictionary(forKey: "userInformation") {
                    userID = userInformation["mobile_number"] as! String
                }
                self.ShowLoader()
                APIFunctions.updateOfferHistory(dictParams: ["outlet_id":self.arrOffers[indexPath.row].outlet_id ?? "", "mobile_number":userID , "offer_type": self.arrOffers[indexPath.row].offer_type ?? "normal", "offer_code":self.arrOffers[indexPath.row].offer_code ?? "", "is_auto_triggered":self.arrOffers[indexPath.row].isAutoTriggerd ?? 0]) { (status, resObj) in
                    //            objBeaconDetected?.name ?? "1"
                    self.HideLoader()
                }
                alertController.dismiss(animated: true, completion: nil)
                
            }
            alertController.addAction(firstAction)
            self.present(alertController, animated: true, completion: nil)
        }
        return cell
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if offersGlobal {
            
            
            let inOutVC = self.storyboard?.instantiateViewController(withIdentifier: "InOutPopUpVC") as! InOutPopUpVC
            inOutVC.modalPresentationStyle = .overCurrentContext
            inOutVC.modalTransitionStyle = .coverVertical
            
            self.navigationController?.present(inOutVC, animated: true, completion: nil)
            
            inOutVC.onDismissChangedHandler = { (status,data) in
                let strPremise = data as? String
                if strPremise == "InPremise" {
                    
                    
                    if self.isBluetoothOn {
                        let initialViewController = self.storyboard!.instantiateViewController(withIdentifier: "InPremiseLandingVC") as! InPremiseLandingVC
                        initialViewController.objSelectedBranchID = self.arrOffers[indexPath.row].outlet_id
                        initialViewController.imgUrl = self.arrOffers[indexPath.row].image_path ?? ""
                        //initialViewController.objSelectedBranch?.category = self.selectedOutlet?.category
                        self.navigationController?.pushViewController(initialViewController, animated: true)
                    } else {
                        self.bluetoothManager          = CBCentralManager()
                        self.bluetoothManager.delegate = self
                        //self.alert(message: "You need to switch on your bluetooth first.", title: "Alert!")
                    }
                    
                } else {
                    let initialViewController = self.storyboard!.instantiateViewController(withIdentifier: "OutPremiseLandingVC") as! OutPremiseLandingVC
                    
                    let obj = ["id":self.arrOffers[indexPath.row].outlet_id, "name":self.arrOffers[indexPath.row].name, "description":self.arrOffers[indexPath.row].offer_description, "newcustomer":self.arrOffers[indexPath.row].newcustomer,"image_path": self.arrOffers[indexPath.row].image_path ?? ""]
                    //let dataExample: Data = NSKeyedArchiver.archivedData(withRootObject: obj)
                    do {
                        let jsonData = try JSONSerialization.data(withJSONObject: obj, options: .prettyPrinted)
                        // here "jsonData" is the dictionary encoded in JSON data
                        let jsonDecoder = JSONDecoder()
                        let objTesd = try jsonDecoder.decode(Branches.self, from: jsonData)
                        initialViewController.outObj = objTesd
                    }
                    catch {
                        
                    }
                    //initialViewController.outObj?.category = self.selectedOutlet?.category
                    self.navigationController?.pushViewController(initialViewController, animated: true)
                }
                
            }
        }
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return arrOffers[indexPath.row].isTapped ? UITableView.automaticDimension : 120
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
