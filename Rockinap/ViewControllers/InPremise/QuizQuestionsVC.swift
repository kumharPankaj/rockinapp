//
//  QuizQuestionsVC.swift
//  Rockinap
//
//  Created by orionspica on 06/10/18.
//  Copyright © 2018 funndynamix. All rights reserved.
//

import UIKit

class QuizQuestionsVC: CommonViewController {

    @IBOutlet weak var pregressTime: UIProgressView!
    @IBOutlet weak var lblTimer: UILabel!
    @IBOutlet weak var tblViewQuestions: UITableView!
    
    private var seconds = 90 //This variable will hold a starting value of seconds. It could be any amount above 0.
    private var timer = Timer()
    private var isTimerRunning = false //This will be used to make sure only one timer is created at a time.
    private var progressValue = 0.0
    var selectedAnswer = ""
    var correctAnswer = ""
    var currentQuesObj : QuesObj?
    var currentQuesIndex = 0
    var totalMarks = 0
    var objQuesResponse:QuizQuessModel?
    var arrAnswers = [String]()
    var gameID = ""
    var objSelectedQuizBranchID = ""
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var lblQuesCount: UILabel!
    var objBeaconDetected: Item?
    var isStatusFalse = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        super.backViewNeeded()
        //tblViewQuestions.tableFooterView = UIView()
        //getQuizData()
        var userID = ""
        if let userInformation = UserDefaults.standard.dictionary(forKey: "userInformation") {
            userID = userInformation["userID"] as! String
        }
        self.ShowLoader()
        APIFunctions.quizStatus_Call(dictParams: ["uuid":userID, "outlet_id":objSelectedQuizBranchID, "game_name": "general","win_status":0]) { (status, resObj) in
            self.HideLoader()
            if let objStatus = resObj as? [String: Any] {
                if status {
                    let objData = objStatus["data"] as? [String: Any]
                    self.gameID = objData?["game_id"] as! String
                    self.getQuizData()
                    
                } else {
                    self.isStatusFalse = true
                    self.alert(message: objStatus["message"] as! String, title: "Alert")
                    self.navigationController?.popViewController(animated: true)
                    
                }
                
            }
        }
        // Do any additional setup after loading the view.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        var userID = ""
        if let userInformation = UserDefaults.standard.dictionary(forKey: "userInformation") {
            userID = userInformation["userID"] as! String
        }
        if !isStatusFalse {
            APIFunctions.quizStatus_Call(dictParams: ["game_id":self.gameID, "uuid":userID, "outlet_id":objSelectedQuizBranchID,"status":"unfinish", "game_name": "general","win_status":0]) { (status, resObj) in
                if let objStatus = resObj as? [String: Any] {
                    
                }
            }
        }
        
    }
    func getQuizData() {
//        var userID = ""
//        if let userInformation = UserDefaults.standard.dictionary(forKey: "userInformation") {
//            userID = userInformation["userID"] as! String
//        }
        self.ShowLoader()
        APIFunctions.quizQuesList_Call(dictParams: ["outlet_id":objSelectedQuizBranchID, "beacon_name":objBeaconDetected?.name ?? "Beacon 01" , "limit": "10"]) { (status, resObj) in
//            objBeaconDetected?.name ?? "1"
            self.HideLoader()
            if let objOutles = resObj as? QuizQuessModel {
                
                //self.gameID = objOutles.data![0].game_id!
                //self.arrQuestions.removeAll()
                self.objQuesResponse = objOutles
                self.tblViewQuestions.delegate = self
                self.tblViewQuestions.dataSource = self
                self.runTimer()
                self.changeQues()
            } else {
                let alertController = UIAlertController(title:"Alert" , message: "Quiz is not available at the moment.", preferredStyle:.alert)
                let firstAction = UIAlertAction(title: "Ok", style: .default) { (alert: UIAlertAction!) -> Void in
                    alertController.dismiss(animated: true, completion: nil)
                    self.navigationController?.popViewController(animated: true)
                }
                alertController.addAction(firstAction)
                self.present(alertController, animated: true, completion: nil)
            }
        }
    }
    private func updateOfferHistoryCall(code: String, type: String) {
        var userID = ""
        if let userInformation = UserDefaults.standard.dictionary(forKey: "userInformation") {
            userID = userInformation["mobile_number"] as! String
        }
        self.ShowLoader()
        APIFunctions.updateOfferHistory(dictParams: ["outlet_id":objSelectedQuizBranchID, "mobile_number":userID , "offer_type": type, "offer_code":code, "is_auto_triggered":"0"]) { (status, resObj) in
            //            objBeaconDetected?.name ?? "1"
            self.HideLoader()
            self.navigationController?.popViewController(animated: true)
        }
    }
    private func updateStatus(status: String) {
        var userID = ""
        if let userInformation = UserDefaults.standard.dictionary(forKey: "userInformation") {
            userID = userInformation["userID"] as! String
        }
        self.ShowLoader()
        APIFunctions.quizStatus_Call(dictParams: ["uuid":userID, "outlet_id":objSelectedQuizBranchID,"game_id":self.gameID,"status":status, "game_name": "general","win_status":0]) { (status, resObj) in
            self.HideLoader()
            if let objStatus = resObj as? [String: Any] {
                let objData = objStatus["data"] as? [String: Any]
                self.isStatusFalse = true
                print("")
            }
        }
    }
    func runTimer() {
//        DispatchQueue.global().async {
            self.timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(self.updateTimer)), userInfo: nil, repeats: true)
//        }
    }
    
    @objc func updateTimer() {
        if seconds > 0{
            if progressValue == 90{
                timer.invalidate()
                updateStatus(status: "finish")
                if totalMarks > 30{
                    self.alert(message: "Woohooo.. You have won the quiz", title: "Alerts")
                } else {
                    self.alert(message: "Ohhhh.. You have lost the quiz. Better luck next time", title: "")
                }
            } else {
                seconds -= 1     //This will decrement(count down)the seconds.
                progressValue += 1
                lblTimer.text = timeString(time: TimeInterval(seconds)) //This will update the label.
                UIView.animate(withDuration: 3, animations: { () -> Void in
                    self.pregressTime.setProgress(Float(((self.progressValue/90) * 100)/100), animated: true)
                })
            }
        
        } else {
            
        }
        
    }
    
   private func timeString(time:TimeInterval) -> String {
        let minutes = Int(time) / 60 % 60
        let seconds = Int(time) % 60
        return String(format:"%02i:%02i", minutes, seconds)
    }
    func changeQues() {
        if currentQuesIndex < (objQuesResponse?.data![0].quizList?.count)!{
            if  currentQuesIndex == 0 || selectedAnswer.length != 0{
                if selectedAnswer == currentQuesObj?.answer{
                    totalMarks = totalMarks + 5
                }
                lblQuesCount.text = "\(currentQuesIndex + 1)/\(objQuesResponse?.data![0].quizList?.count ?? 0)"
                selectedAnswer = ""
                currentQuesObj = self.objQuesResponse?.data?[0].quizList?[currentQuesIndex]
                currentQuesIndex += 1
                arrAnswers = [currentQuesObj?.option_a, currentQuesObj?.option_b, currentQuesObj?.option_c, currentQuesObj?.option_d] as! [String]
                tblViewQuestions.reloadData()
            }
        } else {
            if  currentQuesIndex == 0 || selectedAnswer.length != 0{
                if selectedAnswer == currentQuesObj?.answer{
                    totalMarks = totalMarks + 5
                }
                //lblQuesCount.text = "\(currentQuesIndex + 1)/\(arrQuestions.count)"
            }
            timer.invalidate()
            var userID = ""
            if let userInformation = UserDefaults.standard.dictionary(forKey: "userInformation") {
                userID = userInformation["userID"] as! String
            }
            APIFunctions.quizStatus_Call(dictParams: ["uuid":userID, "outlet_id":objSelectedQuizBranchID,"game_id":self.gameID,"status":"finish", "game_name": "general","win_status":1]) { (status, resObj) in
                if let objStatus = resObj as? [String: Any] {
                    let objData = objStatus["data"] as? [String: Any]
                    self.isStatusFalse = true
                    print("")
                }
            }
            
            if totalMarks > 30{
                let alertController = UIAlertController(title:"😎\nCongrats!" , message: "THAT'S A WINNER\nYOU SCORED  \(totalMarks) POINTS", preferredStyle:.alert)
                let firstAction = UIAlertAction(title: "Ok", style: .default) { (alert: UIAlertAction!) -> Void in
                    alertController.dismiss(animated: true, completion: nil)
                    self.ShowLoader()
                    APIFunctions.getOffersList(dictParams: ["outlet_id":self.objSelectedQuizBranchID, "offer_type": "quiz", "limit": 1]) { (status, resObj) in
                        self.HideLoader()
                        if let objOutles = resObj as? OffersModel{
                            if objOutles.offersList?.count ?? 0 > 0 {
                                let alertController = UIAlertController(title:"Your award offer Code for the Quiz" , message: objOutles.offersList?[0].offer_code, preferredStyle:.alert)
                                let firstAction = UIAlertAction(title: "Ok", style: .default) { (alert: UIAlertAction!) -> Void in
                                    self.updateOfferHistoryCall(code:(objOutles.offersList?[0].offer_code)!, type: (objOutles.offersList?[0].offer_type)! )
                                    alertController.dismiss(animated: true, completion: nil)
                                    
                                }
                                alertController.addAction(firstAction)
                                self.present(alertController, animated: true, completion: nil)
                            } else {
                                self.navigationController?.popViewController(animated: true)
                            }
                        } else {
                            self.alert(message: resObj as? String ?? "Somthing went wrong", title: "Alert")
                        }
                    }
                }
                alertController.addAction(firstAction)
                self.present(alertController, animated: true, completion: nil)
                
            } else {
                let alertController = UIAlertController(title:"🤭\nOops!" , message: "SORRY! BETTER LUCK NEXT TIME..", preferredStyle:.alert)
                let firstAction = UIAlertAction(title: "Ok", style: .default) { (alert: UIAlertAction!) -> Void in
                    alertController.dismiss(animated: true, completion: nil)
                    self.navigationController?.popViewController(animated: true)
                }
                alertController.addAction(firstAction)
                self.present(alertController, animated: true, completion: nil)
               
            }
            
        }
        
    }
    
    @IBAction func btnNextClicked(_ sender: UIButton) {
         changeQues()
    }
    
    //MARK:- Table view methods
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrAnswers.count + 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellHeader")
            (cell?.subviews[0].subviews[0] as! UILabel).text = currentQuesObj?.question
            return cell ?? UITableViewCell()
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellOptions")
            (cell?.subviews[0].subviews[0] as! UILabel).text = arrAnswers[indexPath.row - 1]
            cell?.accessoryType = .none
            return cell ?? UITableViewCell()
        }
        
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
         return UITableView.automaticDimension
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row != 0 {
            tblViewQuestions.cellForRow(at: indexPath)?.accessoryType = .checkmark
            selectedAnswer = arrAnswers[indexPath.row - 1]
        }
    }
    override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        tblViewQuestions.cellForRow(at: indexPath)?.accessoryType = .none
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
