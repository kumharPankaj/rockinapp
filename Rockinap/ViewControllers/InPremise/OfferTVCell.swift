//
//  OfferTVCell.swift
//  Rockinap
//
//  Created by Pankaj Kumhar on 13/07/19.
//  Copyright © 2019 funndynamix. All rights reserved.
//

import UIKit

class OfferTVCell: UITableViewCell {

    @IBOutlet weak var imgViewOffer: UIImageView!
    @IBOutlet weak var lblOfferTitle: UILabel!
    @IBOutlet weak var lblOfferDesc: UITextView!
    @IBOutlet weak var lblExpiryDate: UILabel!
    @IBOutlet weak var btnRedeem: UIButton!
    @IBOutlet weak var btnDropDown: UIButton!
    var dropCownClicked: ((OfferTVCell) -> Void)?
    var redeemButtonClicked: ((OfferTVCell) -> Void)?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setUpOffersCell(objOutlet: Offers) {
        
        self.lblOfferTitle.text = objOutlet.offer_name
        
        self.btnDropDown.setImage(objOutlet.isTapped ? UIImage.init(named: "arrowUp") : UIImage.init(named: "arrowDown"), for: .normal)
        var strDesc = "Outlet Name: " + (objOutlet.name ?? "") + "\n"
        strDesc = strDesc + objOutlet.offer_description! + "\n" + "Offer Message: " + objOutlet.offer_message! + "\n" + "Offer Code: " + objOutlet.offer_code!
        strDesc = strDesc + "\n" + "Offer Percentage: " + objOutlet.discount_percent! + "% for you"
        strDesc = strDesc + "\n" + (objOutlet.webLink ?? "N/A")
        strDesc = strDesc + "\n" + "Received on: " + (objOutlet.offer_sent_on ?? "")
        self.lblOfferDesc.text = strDesc
        self.lblExpiryDate.text = "Expires on: " + objOutlet.expires_on!
        if objOutlet.redeem_status == 1 {
            self.btnRedeem.setTitle("Redeemed", for: .normal)
            self.btnRedeem.setTitleColor(.red, for: .normal)
            self.btnRedeem.isUserInteractionEnabled = false
        } else {
            self.btnRedeem.setTitle("Redeem", for: .normal)
            self.btnRedeem.setTitleColor(.green, for: .normal)
            self.btnRedeem.isUserInteractionEnabled = true
        }
        let url = URL(string: objOutlet.image_url?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? "")
        self.imgViewOffer.kf.setImage(with: url, placeholder:UIImage.init(named: "loading") )
    }
    @IBAction func btnRedeemClicked(_ sender: UIButton) {
        redeemButtonClicked?(self)
    }
    @IBAction func btnDropDownClicked(_ sender: UIButton) {
        dropCownClicked?(self)
    }
}
