//
//  BingoPopUpVC.swift
//  Rockinap
//
//  Created by Pankaj Kumhar on 2/3/19.
//  Copyright © 2019 funndynamix. All rights reserved.
//

import UIKit

class BingoPopUpVC: UIViewController {

    @IBOutlet weak var lblWinningMsg: UILabel!
    var msgLoosing = ""
    var backToGameVC:((String)->())?
    var outletID = ""
    var huntCompleted: ((String) -> Void)?
    override func viewDidLoad() {
        super.viewDidLoad()

        lblWinningMsg.text = msgLoosing
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnOkClicked(_ sender: UIButton) {
        
        self.ShowLoader()
        APIFunctions.getOffersList(dictParams: ["outlet_id":outletID, "offer_type": "treasurehunt", "limit": 1]) { (status, resObj) in
            self.HideLoader()
            if let objOutles = resObj as? OffersModel{
               
                if objOutles.offersList?.count ?? 0 > 0 {
                    
                     let alertController = UIAlertController(title:"This is Your Offer Code" , message:"\(objOutles.offersList?[0].offer_code ?? "1234") for the Treasure Hunt Award" , preferredStyle:.alert)
                    let firstAction = UIAlertAction(title: "Ok", style: .default) { (alert: UIAlertAction!) -> Void in
                        self.updateOfferHistoryCall(code:(objOutles.offersList?[0].offer_code)!, type: (objOutles.offersList?[0].offer_type)! )
                        alertController.dismiss(animated: true, completion: nil)
                        
                    }
                    alertController.addAction(firstAction)
                    self.present(alertController, animated: true, completion: nil)
                } else {
                    self.navigationController?.popViewController(animated: true)
                }
            } else {
                self.alert(message: resObj as? String ?? "Somthing went wrong", title: "Alert")
            }
        }
        
        
        
        /*let alertController = UIAlertController(title:"" , message: "Congratulations! You get an Offer" , preferredStyle:.alert)
        let firstAction = UIAlertAction(title: "Ok", style: .default) { (alert: UIAlertAction!) -> Void in
            alertController.dismiss(animated: true, completion: nil)

            
                if let callback = self.backToGameVC
                {
                    callback("Back")
                }
        }
        alertController.addAction(firstAction)
        self.present(alertController, animated: true, completion: nil)*/
        
        //let bingoVC = self.storyboard!.instantiateViewController(withIdentifier: "GameListVc") as! GameListVc
        //.popToViewController(bingoVC, animated: true)//.popViewController(animated: false)
    }
    private func updateOfferHistoryCall(code: String, type: String) {
        var userID = ""
        if let userInformation = UserDefaults.standard.dictionary(forKey: "userInformation") {
            userID = userInformation["mobile_number"] as! String
        }
        self.ShowLoader()
        APIFunctions.updateOfferHistory(dictParams: ["outlet_id":outletID, "mobile_number":userID , "offer_type": type, "offer_code":code, "is_auto_triggered":"0"]) { (status, resObj) in
            //            objBeaconDetected?.name ?? "1"
            
            self.HideLoader()
            if let callback = self.backToGameVC
            {
                callback("Back")
            }
           
            //self.navigationController?.popViewController(animated: true)
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
