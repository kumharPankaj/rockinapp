//
//  RegisterVCViewController.swift
//  Rockinap
//
//  Created by Pankaj Kumhar on 2/2/19.
//  Copyright © 2019 funndynamix. All rights reserved.
//

import UIKit


class RegisterVCViewController: UIViewController {

    @IBOutlet weak var txtName: TextField!
    @IBOutlet weak var txtEmail: TextField!
    @IBOutlet weak var txtPassword: TextField!
    @IBOutlet weak var txtMobileNumber: TextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.navigationBar.isHidden = true
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification , object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification , object: nil)
    }
    
    //MARK:- Keyboard Listeners
    @objc func keyboardWillShow(notification: NSNotification = NSNotification()) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            var distanceBetweenTextfielAndKeyboard = (self.view.frame.height - textFieldRealYPosition - keyboardSize.height) - 100
            if !Functions.IsiPhoneDevice(){
                distanceBetweenTextfielAndKeyboard = -(distanceBetweenTextfielAndKeyboard/2) - 100
            }
            if distanceBetweenTextfielAndKeyboard < 0 {
                UIView.animate(withDuration: 0.4) {
                    self.view.transform = CGAffineTransform(translationX: 0.0, y: distanceBetweenTextfielAndKeyboard)
                }
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification = NSNotification()) {
        UIView.animate(withDuration: 0.4) {
            self.view.transform = .identity
        }
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    @IBAction func btnBackClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSignUpCliced(_ sender: UIButton) {
        
        self.view.endEditing(true)
        if (txtName.text?.length)! > 0 && (txtEmail.text?.length)! > 0 && (txtPassword.text?.length)! > 0 && (txtMobileNumber.text?.length)! > 0{
            
            if Functions.emailValidation(txtEmail.text!) && !((txtMobileNumber.text?.length)! < 10 || (txtMobileNumber.text?.length)! > 15){
                
                let dict = NSMutableDictionary()
                dict["deviceID"] = UIDevice.current.identifierForVendor!.uuidString
                dict["email"] = txtEmail.text
                dict["mobile_number"] = txtMobileNumber.text
                dict["country_code"] = "91"
                dict["name"] = txtName.text
                dict["password"] = txtPassword.text
                dict["authType"] = "manual"
                dict["refresh_token"] = ""
                dict["role"] = "player"
                self.ShowLoader()
                APIFunctions.registerUser(dictParams: dict as! [String : Any]) { (status, resObj) in
                    self.HideLoader()
                   
                    guard let dictData = resObj as? Data else
                    {
                        return
                    }
                    
                    do {
                        let jsonObjet = try JSONSerialization.jsonObject(with: dictData, options: .mutableContainers) as? [String:AnyObject]
                        
                        if(jsonObjet != nil)
                        {
                            if jsonObjet!["error"] as? Bool ?? true == false
                            {
                                let data = jsonObjet?["data"] as! [String: Any]
                                
                                let reserveVC = self.storyboard?.instantiateViewController(withIdentifier: "OTPVerificationVC") as! OTPVerificationVC
                                //reserveVC.isFromForgotPassword = true
                                reserveVC.strOtp = data["otp"] as? Int ?? 123456
                                reserveVC.strMobileNumber = self.txtMobileNumber.text!
                                self.navigationController?.pushViewController(reserveVC, animated: true)
                                
                                //self.verifyOTP(dictParam: ["otp_code":data["otp"] as Any,"mobile_number":self.txtMobileNumber.text!, "country_code":"91"])
                                
                            }
                            else
                            {
                                CRNotifications.showNotification(type: CRNotifications.error, title: "Alert!", message: jsonObjet?["message"] as? String ?? "Login failed!", dismissDelay: 3)
                            }
                        } else
                        {
                            CRNotifications.showNotification(type: CRNotifications.error, title: "Alert!", message: jsonObjet?["message"] as? String ?? "Login failed!", dismissDelay: 3)
                        }
                        
                    }
                    catch{
                        print(error.localizedDescription)
                    }
                }
            }
            else{
                if !Functions.emailValidation(txtEmail.text!){
                    CRNotifications.showNotification(type: CRNotifications.error, title: "Alert!", message: "Invalid E-mail ID.", dismissDelay: 3)
                }else{
                    CRNotifications.showNotification(type: CRNotifications.error, title: "Alert!", message: "Invalid mobile number.", dismissDelay: 3)
                }
            }
        }
        else{
            CRNotifications.showNotification(type: CRNotifications.error, title: "Alert!", message: "All fields are Mandatory.", dismissDelay: 3)
        }
    }
    private func verifyOTP(dictParam: [String: Any]) -> Void {
        self.ShowLoader()
        APIFunctions.verifyOTP(dictParams: dictParam){ (status, resObj) in
            self.HideLoader()
            guard let dictData = resObj as? Data else
            {
                return
            }
            
            do {
                let jsonObjet = try JSONSerialization.jsonObject(with: dictData, options: .mutableContainers) as? [String:AnyObject]
                if(jsonObjet != nil)
                {
                    if jsonObjet!["error"] as? Bool ?? false == false {
                        let data = jsonObjet?["data"] as! [String: Any]
                        let userData = data["data"] as! [String: Any]
                        let userInfo = ["token": data["token"] as! String, "name": userData["name"] as! String,  "role": userData["role"] as! String,"email":  userData["role"] as! String,] as [String : Any]
                        UserDefaults.standard.set(userInfo, forKey: "userInformation")
                        UserDefaults.standard.set(true, forKey: defaultsKeys.keepSigned)
                        UserDefaults.standard.synchronize()
                        DispatchQueue.main.async {
                            if status == true {
                                if let token = UserDefaults.standard.value(forKey: defaultsKeys.fcmToken) {
                                    self.callRefreshToken(token: token as! String)
                                }
                                let appDelegate = UIApplication.shared.delegate! as! AppDelegate
                                DrawerRootViewController.shared().updateNavigationBarStatus(true)
                                
                                let VC = DrawerRootViewController.shared().setupDrawerController()
                                appDelegate.window!.rootViewController = VC
                                
                            }
                        }
                    }
                }
            }
            catch{
                print(error.localizedDescription)
            }
        }
    }
    
    private func callRefreshToken(token: String) {
        let id:String = ""
        print("uuid--->",id.getUUID() ?? "")
        if let userInformation = UserDefaults.standard.dictionary(forKey: "userInformation") {
            APIFunctions.refresNotificationToken(dictParams: ["refresh_token":token,
                                                              "uuid":userInformation["userID"] as! String,
                                                              "device_type":"ios",
                                                              "device_id":id.getUUID() ?? "",
                                                              "mobile_number":userInformation["mobile_number"] as! String]) { (status, resObj) in
                                                                
                                                                
            }
        }
        
    }
    func validateForm() -> Bool {
        
        return true
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension RegisterVCViewController : UITextFieldDelegate{
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textFieldRealYPosition = textField.frame.origin.y + textField.frame.height
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if textField  == txtMobileNumber  {
            let inverseSet = NSCharacterSet(charactersIn:"0123456789").inverted
            let components = string.components(separatedBy: inverseSet)
            let filtered = components.joined(separator: "")
            return string == filtered
        }
        else if textField  == txtEmail {
            let inverseSet = NSCharacterSet(charactersIn:ACCEPTABLE_CHAR_EMAIL).inverted
            let components = string.components(separatedBy: inverseSet)
            let filtered = components.joined(separator: "")
            return string == filtered
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case txtName:
            DispatchQueue.main.async {
                self.txtName.resignFirstResponder()
                self.txtEmail.becomeFirstResponder()
            }
            
        case txtEmail:
            DispatchQueue.main.async {
                self.txtEmail.resignFirstResponder()
                self.txtPassword.becomeFirstResponder()
            }
            
        case txtPassword:
            DispatchQueue.main.async {
                self.txtPassword.resignFirstResponder()
                self.txtMobileNumber.becomeFirstResponder()
                self.view.endEditing(true)
            }
        case txtMobileNumber:
            DispatchQueue.main.async {
                self.view.endEditing(true)
            }
            
        default:
            return true
        }
        return true
    }
}
