//
//  OTPVerificationVC.swift
//  Rockinap
//
//  Created by Pankaj Kumhar on 3/24/19.
//  Copyright © 2019 funndynamix. All rights reserved.
//

import UIKit
import Firebase

class OTPVerificationVC: UIViewController {

    @IBOutlet weak var txtCode: UITextField!
    @IBOutlet weak var txtNumber: UITextField!
    @IBOutlet weak var btnSendOTP: UIButton!
    @IBOutlet weak var txtOTP: UITextField!
    @IBOutlet weak var btnVerifyOTP: UIButton!
    @IBOutlet weak var btnResendOTP: UIButton!
    var isFromForgotPassword = false
    var strMobileNumber = ""
    var strOtp = 0
    override func viewDidLoad() {
        super.viewDidLoad()

        txtNumber.text = strMobileNumber
        txtOTP.text = "\(strOtp)"
        setUpUI()
        if isFromForgotPassword {
            self.ShowLoader()
            APIFunctions.sendOTP(dictParams: ["mobile_number":txtNumber.text!, "country_code":"91"], isFromForgotPwd: isFromForgotPassword) { (status, resObj) in
                self.HideLoader()
                guard let dictData = resObj as? Data else
                {
                    return
                }
                
                do {
                    let jsonObjet = try JSONSerialization.jsonObject(with: dictData, options: .mutableContainers) as? [String:AnyObject]
                    
                    if(jsonObjet != nil)
                    {
                        if jsonObjet!["error"] as? Bool ?? false == false {
                            let data = jsonObjet?["data"] as! [String: Any]
                            self.txtOTP.text = "\((data["otp"] as? Int ?? 000000))"
                            self.setUpUI()
                            if !self.isFromForgotPassword {
                                self.verifyOTP(dictParam: ["otp_code":data["otp"] as Any,"mobile_number":self.txtNumber.text!, "country_code":"91"])
                            }
                        } else {
                            CRNotifications.showNotification(type: CRNotifications.error, title: "Alert!", message: jsonObjet?["message"] as? String ?? "OTP sending failed!", dismissDelay: 3)
                        }
                        
                    } else
                    {
                        CRNotifications.showNotification(type: CRNotifications.error, title: "Alert!", message: jsonObjet?["message"] as? String ?? "Login failed!", dismissDelay: 3)
                    }
                    
                }
                catch{
                    print(error.localizedDescription)
                }
            }
        }
        // Do any additional setup after loading the view.
    }
    
    func setUpUI() -> Void {
//        txtCode.text?.length ?? 0 == 2 &&
        if  txtNumber.text?.length ?? 0 >= 9{
            btnSendOTP.isUserInteractionEnabled = true
            btnSendOTP.backgroundColor = UIColor.init(hexString: "007AFF")
            txtOTP.isUserInteractionEnabled = true
            txtOTP.backgroundColor = UIColor.white
            if txtOTP.text?.length == 6 {
                btnSendOTP.isUserInteractionEnabled = false
                btnSendOTP.backgroundColor = .lightGray
                btnVerifyOTP.isUserInteractionEnabled = true
                btnVerifyOTP.backgroundColor = UIColor.init(hexString: "007AFF")
            } else {
                btnVerifyOTP.isUserInteractionEnabled = false
                btnVerifyOTP.backgroundColor = UIColor.lightGray
            }
            btnResendOTP.isUserInteractionEnabled = true
            //btnResendOTP.backgroundColor = UIColor.lightGray
        } else {
            btnSendOTP.isUserInteractionEnabled = false
            btnSendOTP.backgroundColor = .lightGray //UIColor.init(hexString: "007AFF")
            txtOTP.isUserInteractionEnabled = false
            txtOTP.backgroundColor = UIColor.lightGray
            btnResendOTP.isUserInteractionEnabled = false
            //btnResendOTP.backgroundColor = UIColor.lightGray
            btnVerifyOTP.isUserInteractionEnabled = false
            btnVerifyOTP.backgroundColor = UIColor.lightGray
        }
    }

    func verifyOTP(dictParam: [String: Any]) -> Void {
        self.ShowLoader()
        APIFunctions.verifyOTP(dictParams: dictParam){ (status, resObj) in
            self.HideLoader()
            guard let dictData = resObj as? Data else
            {
                return
            }
            
            do {
                let jsonObjet = try JSONSerialization.jsonObject(with: dictData, options: .mutableContainers) as? [String:AnyObject]
                if(jsonObjet != nil)
                {
                    if jsonObjet!["error"] as? Bool ?? false == false {
                        let data = jsonObjet?["data"] as! [String: Any]
                        let userInfo = ["token": data["token"] as! String]
                        UserDefaults.standard.set(userInfo, forKey: "userInformation")
                        UserDefaults.standard.synchronize()
                        if self.isFromForgotPassword {
                            let alert = UIAlertController(title: "Forgot Password?", message: "", preferredStyle: UIAlertController.Style.alert)
                            let action = UIAlertAction(title: "Send", style: .default) { (alertAction) in
                                
                                let textField = alert.textFields![0] as UITextField
                                let textFieldConfirm = alert.textFields![1] as UITextField
                                
                                if textField.text?.length ?? 0 == 6 {
                                    if textField.text == textFieldConfirm.text {
                                        self.ShowLoader()
                                        APIFunctions.resetPassword(dictParams: ["new_password":textField.text!, "confirm_password":textFieldConfirm.text!], completion: { (status, reObj) in
                                            self.HideLoader()
                                            guard let dictData = resObj as? Data else
                                            {
                                                return
                                            }
                                            
                                            do {
                                                let jsonObjet = try JSONSerialization.jsonObject(with: dictData, options: .mutableContainers) as? [String:AnyObject]
                                                if status {
                                                    CRNotifications.showNotification(type: CRNotifications.success, title: "Success!", message: "Password updated successfully.", dismissDelay: 3)
                                                    self.navigationController?.popViewController(animated: true)
                                                } else {
                                                    CRNotifications.showNotification(type: CRNotifications.error, title: "Alert!", message: "Failed to update password. Please try again", dismissDelay: 3)
                                                }
                                            } catch {
                                                print(error.localizedDescription)
                                            }
                                        })
                                        
                                    } else {
                                        CRNotifications.showNotification(type: CRNotifications.error, title: "Alert!", message: "Confirm New Password is not matching with New Password", dismissDelay: 3)
                                    }
                                    
                                } else {
                                    CRNotifications.showNotification(type: CRNotifications.error, title: "Alert!", message: "OTP must be 6 characters long", dismissDelay: 3)
                                }
                                
                            }
                            alert.addTextField { (textField) in
                                textField.placeholder = "New Password"
                                textField.isSecureTextEntry = true
                                
                            }
                            alert.addTextField { (textField) in
                                textField.isSecureTextEntry = true
                                textField.placeholder = "Confirm New Password"
                            }
                            alert.addAction(action)
                            
                            self.present(alert, animated:true, completion: nil)
                        } else {
                            let data = jsonObjet?["data"] as! [String: Any]
                            let userData = data["data"] as! [String: Any]
                            //let userInfo = ["token": data["token"] as! String, "name": userData["name"] as! String,  "role": userData["role"] as! String,"email":  userData["role"] as! String] as [String : Any]
                            let userInfo = ["token": data["token"] as! String, "name": userData["name"] as! String, "userID": userData["uuid"] as! String, "role": userData["role"] as? String ?? "player","email": userData["email"] as! String ,"mobile_number": userData["mobile_number"] as! String] as [String : Any]
                            UserDefaults.standard.set(userInfo, forKey: "userInformation")
                            UserDefaults.standard.set(true, forKey: defaultsKeys.keepSigned)
                            UserDefaults.standard.synchronize()
                            DispatchQueue.main.async {
                                if status == true {
                                    UserDefaults.standard.set(Messaging.messaging().fcmToken, forKey: defaultsKeys.fcmToken)
                                    if let token = Messaging.messaging().fcmToken {
                                        self.callRefreshToken(token: token )
                                    }
                                    let appDelegate = UIApplication.shared.delegate! as! AppDelegate
                                    DrawerRootViewController.shared().updateNavigationBarStatus(true)
                                    
                                    let VC = DrawerRootViewController.shared().setupDrawerController()
                                    appDelegate.window!.rootViewController = VC
                                    
                                }
                            }
                        }
                       
                    }
                }
            }
            catch{
                print(error.localizedDescription)
            }
        }
    }
    private func callRefreshToken(token: String) {
         let id:String = ""
        if let userInformation = UserDefaults.standard.dictionary(forKey: "userInformation") {
            APIFunctions.refresNotificationToken(dictParams: ["refresh_token":token,
                                                              "uuid":userInformation["userID"] as! String,
                                                              "device_type":"ios",
                                                              "device_id":id.getUUID() ?? "",
                                                              "mobile_number":userInformation["mobile_number"] as! String]) { (status, resObj) in
                                                                
                                                                
            }
        }
        
    }
   
    //MARK:- Button Actions
    @IBAction func btnSendOTPClicked(_ sender: UIButton) {
        var countryCode = txtCode.text
        //if (txtCode.text?.contains(word: "+"))!{
            countryCode = countryCode?.replace(target: "+", withString: "")
        //}
        self.ShowLoader()
        APIFunctions.sendOTP(dictParams: ["mobile_number":txtNumber.text!, "country_code":"91"], isFromForgotPwd: isFromForgotPassword) { (status, resObj) in
            self.HideLoader()
            guard let dictData = resObj as? Data else
            {
                return
            }
            
            do {
                let jsonObjet = try JSONSerialization.jsonObject(with: dictData, options: .mutableContainers) as? [String:AnyObject]
                
                if(jsonObjet != nil)
                {
                    if jsonObjet!["error"] as? Bool ?? false == false {
                        let data = jsonObjet?["data"] as! [String: Any]
                        self.txtOTP.text = "\((data["otp"] as? Int ?? 000000))"
                        self.setUpUI()
                        if !self.isFromForgotPassword {
                        self.verifyOTP(dictParam: ["otp_code":data["otp"] as Any,"mobile_number":self.txtNumber.text!, "country_code":"91"])
                        }
                    } else {
                       CRNotifications.showNotification(type: CRNotifications.error, title: "Alert!", message: jsonObjet?["message"] as? String ?? "OTP sending failed!", dismissDelay: 3)
                    }
                    
                } else
                {
                    CRNotifications.showNotification(type: CRNotifications.error, title: "Alert!", message: jsonObjet?["message"] as? String ?? "Login failed!", dismissDelay: 3)
                }
                
            }
            catch{
                print(error.localizedDescription)
            }
        }
    }
   
    @IBAction func btnVerifyOTPClicked(_ sender: UIButton) {
//        self.ShowLoader()
//        APIFunctions.verifyOTP(dictParams: ["mobile_number":txtNumber.text!, "country_code":"91", "otp_code":txtOTP.text!]) { (status, resObj) in
//            self.HideLoader()
//
//        }
        
        self.verifyOTP(dictParam: ["otp_code":txtOTP.text!, "country_code":"91", "mobile_number":self.txtNumber.text!])
    }
    @IBAction func btnResendOTPClicked(_ sender: UIButton) {
        self.ShowLoader()
        APIFunctions.sendOTP(dictParams: ["mobile_number":txtNumber.text!, "country_code":"91"]) { (status, resObj) in
            self.HideLoader()
            
        }
    }
    @IBAction func btnBackClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
// MARK: - Textfield Delegate
extension OTPVerificationVC : UITextFieldDelegate{
    public func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        return true
        
    }
    public func textFieldDidEndEditing(_ textField: UITextField) {
        
        switch textField {
        case txtCode:
            //textField.resignFirstResponder()
            txtNumber.becomeFirstResponder()
            break
       
        default:
            textField.resignFirstResponder()
            setUpUI()
            break
        }
        
        /*let cell = currentTextField.superview?.superview as! UITableViewCell
         cell.backgroundColor = UIColor.clear
         
         if((cell.contentView.subviews[0] as Any ) is UILabel){
         (cell.contentView.subviews[0] as! UILabel ).textColor = UIColor.init(hexString: "#3A566E")
         }*/
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == txtCode {
            if (textField.text?.length)! <= 2 {
                let aSet = NSCharacterSet(charactersIn:"0123456789+").inverted
                let compSepByCharInSet = string.components(separatedBy: aSet)
                let numberFiltered = compSepByCharInSet.joined(separator: "")
                setUpUI()
                return string == numberFiltered
            }
            
        } else {
            if textField == txtNumber {
                if (textField.text?.length)! < 9 {
                    let aSet = NSCharacterSet(charactersIn:"0123456789").inverted
                    let compSepByCharInSet = string.components(separatedBy: aSet)
                    let numberFiltered = compSepByCharInSet.joined(separator: "")
                    return string == numberFiltered
                } else if (textField.text?.length)! == 9 {
                    let aSet = NSCharacterSet(charactersIn:"0123456789").inverted
                    let compSepByCharInSet = string.components(separatedBy: aSet)
                    let numberFiltered = compSepByCharInSet.joined(separator: "")
                    setUpUI()
                    return string == numberFiltered
                    
                } else {
                    return true
                }
            } else if textField == txtOTP {
                if (textField.text?.length)! < 4 {
                    let aSet = NSCharacterSet(charactersIn:"0123456789").inverted
                    let compSepByCharInSet = string.components(separatedBy: aSet)
                    let numberFiltered = compSepByCharInSet.joined(separator: "")
                    setUpUI()
                    return string == numberFiltered
                } else {
                    return false
                }
            }
            
        }
        return false
    }
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true;
        
    }
}
