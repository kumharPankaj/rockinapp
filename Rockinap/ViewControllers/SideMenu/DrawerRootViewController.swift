//
//  DrawerRootViewController.swift
//  StyleFix
//
//  Created by tilak.raj on 22/11/17.
//  Copyright © 2017 sravan. All rights reserved.
//

import UIKit
import MMDrawerController


class DrawerRootViewController: NSObject
{
   
    let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
    var fcm_token:String? = nil
    let appdelegate = UIApplication.shared.delegate as! AppDelegate

   public var mDrawerController:MMDrawerController?
    var consultantViewController : UIViewController? = nil
    
    private static var sharedManager: DrawerRootViewController =
    {
        let tempSharedManager = DrawerRootViewController.init()
        
        return tempSharedManager
    }()
    
    class func shared() -> DrawerRootViewController
    {
        return sharedManager
    }
    
    public func setupDrawerController(_ isPushToChat:Bool = false,_ withVC:UIViewController = UIViewController()) -> UIViewController
    {
       let mainVC = self.loadViewControllerFromStoryBoard("TabController")
        let rightVC = self.loadViewControllerFromStoryBoard("MenuViewController")
        consultantViewController = mainVC //as! HomePageVC
        let navigationController = UINavigationController.init(rootViewController: mainVC)
        AppDelegate.shareddelegate().tempNavigationViewController = navigationController
    
        navigationController.isNavigationBarHidden = false
        navigationController.navigationBar.isTranslucent = true
        let drawerController = MMDrawerController.init(center: navigationController, rightDrawerViewController: rightVC)
        drawerController?.extendedLayoutIncludesOpaqueBars = true
        drawerController?.closeDrawerGestureModeMask = .all
        drawerController?.maximumLeftDrawerWidth = UIScreen.main.bounds.size.width-64
        drawerController?.showsShadow = false
        drawerController?.setDrawerVisualStateBlock({ (drawerController, drawerSide, percentVisible) in
            var sideDrawerViewController:UIViewController?
            if (drawerSide == MMDrawerSide.left)
            {
                sideDrawerViewController = (drawerController?.leftDrawerViewController)!;
            } else
                if (drawerSide == MMDrawerSide.right)
                {
                    sideDrawerViewController = (drawerController?.rightDrawerViewController)!;
            }
            sideDrawerViewController?.view.alpha = percentVisible
        })
        self.mDrawerController = drawerController
        return drawerController!
    }
    
    public func setupUserDrawerController() -> UIViewController
    {
        let userHomeViewController = self.loadViewControllerFromStoryBoard("TabController")// as! HomePageVC
        let rightVC = self.loadViewControllerFromStoryBoard("MenuViewController")
        let navigationController = UINavigationController.init(rootViewController: userHomeViewController)
        navigationController.isNavigationBarHidden = true
        navigationController.navigationBar.isTranslucent = true
        let drawerController = MMDrawerController.init(center: navigationController, rightDrawerViewController: rightVC)
        //drawerController?.openDrawerGestureModeMask = .all
        drawerController?.closeDrawerGestureModeMask = .all
        drawerController?.maximumLeftDrawerWidth = UIScreen.main.bounds.size.width-50
        drawerController?.showsShadow = true
        drawerController?.setDrawerVisualStateBlock({ (drawerController, drawerSide, percentVisible) in
            var sideDrawerViewController:UIViewController?
            if (drawerSide == MMDrawerSide.left)
            {
                sideDrawerViewController = (drawerController?.leftDrawerViewController)!;
            } else
                if (drawerSide == MMDrawerSide.right)
                {
                    sideDrawerViewController = (drawerController?.rightDrawerViewController)!;
            }
            sideDrawerViewController?.view.alpha = percentVisible
        })
        self.mDrawerController = drawerController
        return drawerController!
    }
    
    
    public func setupInitialController() -> UIViewController
    {
            let mainVC = self.loadViewControllerFromStoryBoard("ViewController")
            let navigationController = UINavigationController.init(rootViewController: mainVC)
            navigationController.navigationBar.isTranslucent = true
            navigationController.isNavigationBarHidden = true
            return mainVC
    }
    
   
    
    //MARK: USER Setup
    
    public func setupUserInitialController() -> UIViewController
    {
        let mainVC = self.loadViewControllerFromStoryBoard("ViewController")
        
        let navigationController = UINavigationController.init(rootViewController: mainVC)
        navigationController.navigationBar.isTranslucent = true
        navigationController.isNavigationBarHidden = true
        return navigationController
    }
}

extension DrawerRootViewController
{
    public func loadViewControllerFromStoryBoard(_ viewConrollerIdentifie:String!) -> UIViewController
    {
        let viewController = self.storyBoard.instantiateViewController(withIdentifier: viewConrollerIdentifie)
        return viewController
    }
    
    func addDrawerAsRootVC(_ isPushToChat:Bool = false,_ withVC:UIViewController = UIViewController())
    {
        appdelegate.window?.rootViewController = self.setupDrawerController(isPushToChat,withVC)
    }
    
    func addUserDrawerAsRootVC()
    {
        appdelegate.window?.rootViewController = self.setupDrawerController()
    }
    
    func logoutUser()
    {
        appdelegate.window?.rootViewController = self.setupUserInitialController()
    }
    
    func logout()
    {
        //let defaults:UserDefaults = UserDefaults.standard
        //defaults.set(false, forKey: Names.name_isSaved)
        //defaults.set(nil, forKey: Names.name_currentuser)
        //appdelegate.userOnline(false)
        appdelegate.window?.rootViewController = self.setupUserInitialController()
        //appdelegate.window?.rootViewController = self.setupInitialController()
    }
    
    func openDrawer()
    {
        self.mDrawerController?.open(.right, animated: true, completion: { (isopen) in
        })
    }
    
    func closeWithViewController(_ viewController:UIViewController, _ oncomplitionHandler:WebCallbackHandler? = nil)
    {
        self.mDrawerController?.setCenterView(viewController, withCloseAnimation: true, completion: { (status) in
            if(oncomplitionHandler != nil)
            {
                oncomplitionHandler!(status,viewController)
            }
        })
    }
    
    func loadViewController(isRootVc:Bool,viewControllers:[UIViewController]?,_ viewController:UIViewController = UIViewController())
    {
        if(!isRootVc)
        {
            viewController.navigationController?.pushViewController(viewControllers![0], animated: true)
        }
        else
        {
            AppDelegate.shareddelegate().tempNavigationViewController?.setViewControllers(viewControllers!, animated: true)
            AppDelegate.shareddelegate().window?.rootViewController = AppDelegate.shareddelegate().tempNavigationViewController
        }
    }
    
    func loadViewController(isRootVc:Bool,viewControllers:[UIViewController]?) -> UINavigationController
    {
        AppDelegate.shareddelegate().tempNavigationViewController?.viewControllers = viewControllers!
        
        return (AppDelegate.shareddelegate().tempNavigationViewController)!
    }
    
    func updateNavigationBarStatus(_ isHidden:Bool = false)
    {
        AppDelegate.shareddelegate().tempNavigationViewController?.navigationBar.isHidden = isHidden
    }
   
   
    func writeToDocumentsFile(fileName:String,value:String) -> Bool
    {
        let status = false
        
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString
        let path = documentsPath.appendingPathComponent(fileName)
        do
        {
            try value.write(toFile: path, atomically: true, encoding: String.Encoding.utf8)
           
            return true
        } catch {
            return status
        }
        
        //return status
    }
    
    func readFromDocumentsFile(fileName:String) -> String {
        
        
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString
        let path = documentsPath.appendingPathComponent(fileName)
        let checkValidation = FileManager.default
        
        var file:String = ""
        
        if checkValidation.fileExists(atPath: path)
        {
            do {
               file = try NSString.init(contentsOfFile: path, encoding:String.Encoding.utf8.rawValue ) as String
            }
            catch {
                file = ""
            }
            
        } else {
            file = ""
        }
        
        return file
    } 
}
