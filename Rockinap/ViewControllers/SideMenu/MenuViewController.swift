//
//  MenuViewController.swift
//  EaseMyStay
//
//  Created by tilak raj verma on 06/09/18.
//  Copyright © 2018 Tilak Raj Verma. All rights reserved.
//

import UIKit

class MenuViewController: CommonViewController
{
    @IBOutlet weak var lbluserEmail: UILabel!
    @IBOutlet weak var lbluserName: UILabel!
    @IBOutlet weak var imageUser: UIImageView!
    @IBOutlet weak var tblView: UITableView!
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.tblView.delegate = self
        self.tblView.dataSource = self
        // Do any additional setup after loading the view.
        imageUser.layer.cornerRadius =  imageUser.frame.size.height/2.0
        imageUser.layer.borderWidth = 2.0
        imageUser.layer.borderColor = UIColor.white.cgColor
        imageUser.layer.masksToBounds = true
        imageUser.image = imageUser.image?.getImageWithColor(color: UIColor.white)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let items = AppDelegate.shareddelegate().menuItems?[indexPath.row]
        if(items?.id == 101)
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellLine")
             return cell!
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellmenu")
            let lblTitel = cell?.contentView.viewWithTag(1001) as! UILabel
            lblTitel.text = items?.name
             return cell!
        }
       
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return (AppDelegate.shareddelegate().menuItems?.count) ?? 0
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let tempMenue = AppDelegate.shareddelegate().menuItems![indexPath.row]
        if(tempMenue.id == 101)
        {
            return
        }
        
        let viewController = AppDelegate.shareddelegate().tempNavigationViewController?.topViewController
        
        if(viewController is HomePageVC)
        {
            let tempVC = viewController as! HomePageVC
            tempVC.selectedMenu = tempMenue
            tempVC.updateUIfromMenuSelection()
        }
       DrawerRootViewController.shared().closeWithViewController(AppDelegate.shareddelegate().tempNavigationViewController!)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
