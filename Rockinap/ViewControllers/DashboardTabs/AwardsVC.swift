//
//  AwardsVC.swift
//  Rockinap
//
//  Created by Pankaj Kumhar on 13/07/19.
//  Copyright © 2019 funndynamix. All rights reserved.
//

import UIKit
import CoreBluetooth
class AwardsVC: CommonViewController, CBCentralManagerDelegate  {

    private var manager:CBCentralManager!
    private var isBluetoothOn = false
    var arrAwards = [Awards]()
    @IBOutlet weak var tblAwards: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        super.backViewNeeded()

        manager          = CBCentralManager()
        manager.delegate = self
        getAwards()
        // Do any additional setup after loading the view.
    }
    func getAwards() -> Void {
        self.ShowLoader()
        APIFunctions.getAwardsList() { (status, resObj) in
            self.HideLoader()
            if let objOutles = resObj as? AwardsModel{
                if objOutles.awardList?.count ?? 0 > 0 {
                self.arrAwards = objOutles.awardList!
                    self.viewDidLayoutSubviews()
                    self.tblAwards.setNeedsLayout()
                    self.tblAwards.layoutIfNeeded()
                    self.tblAwards.reloadData()
                } else {
                    self.alert(message: "Currently there is not any award for you. Please try after some time.", title: "Alert")
                }
                
            } else {
                let alertController = UIAlertController(title:"Alert!" , message: "Please login again to enjoy Rockinap.", preferredStyle:.alert)
                let firstAction = UIAlertAction(title: "Logout", style: .default) { (alert: UIAlertAction!) -> Void in
                    if let token = UserDefaults.standard.value(forKey: defaultsKeys.fcmToken) {
                        var user = ""
                        if let userInformation = UserDefaults.standard.dictionary(forKey: "userInformation") {
                            user = userInformation["userID"] as! String
                            
                        }
                        let id:String = ""
                        self.ShowLoader()
                        APIFunctions.logoutAPICall(dictParams: ["uuid":user, "device_id":id.getUUID() ?? ""]) { (status, resObj) in
                            //            objBeaconDetected?.name ?? "1"
                            
                            self.HideLoader()
                            self.resetDefaults()
                            DrawerRootViewController.shared().logout()
                            //self.navigationController?.popViewController(animated: true)
                        }
                        
                    } else {
                        self.resetDefaults()
                        DrawerRootViewController.shared().logout()
                    }
                    
                    
                    alertController.dismiss(animated: true, completion: nil)
                }
                
                alertController.addAction(firstAction)
                self.present(alertController, animated: true, completion: nil)
            }
            print("")
        }
    }
    //Bluetooth manager delegate
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        switch central.state {
        case .poweredOn:
            isBluetoothOn = true
            break
        case .poweredOff:
            isBluetoothOn = false
            print("Bluetooth is Off.")
            break
        case .resetting:
            break
        case .unauthorized:
            break
        case .unsupported:
            break
        case .unknown:
            break
        default:
            break
        }
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrAwards.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellAwards") as! AwardsTVCell
        cell.setUpOffersCell(objOutlet: arrAwards[indexPath.row])
        cell.dropCownAwardClicked = { cell in
            self.arrAwards[indexPath.row].isExpanded = self.arrAwards[indexPath.row].isExpanded ? false : true
            cell.btnDropDown.setImage(self.arrAwards[indexPath.row].isExpanded ? UIImage.init(named: "arrowUp") : UIImage.init(named: "arrowDown"), for: .normal)
            self.tblAwards.beginUpdates()
            self.tblAwards.endUpdates()
        }
        cell.redeemAwardClicked = { cell in
            //self.alert(message: "Please show your offer code:\(self.arrOffers[indexPath.row].offer_code ?? "") to admin", title: "Offer Code")
            let alertController = UIAlertController(title:"Offer Code" , message:"Please show your offer code:\(self.arrAwards[indexPath.row].offer_code ?? "") to admin" , preferredStyle:.alert)
            let firstAction = UIAlertAction(title: "Ok", style: .default) { (alert: UIAlertAction!) -> Void in
                var userID = ""
                if let userInformation = UserDefaults.standard.dictionary(forKey: "userInformation") {
                    userID = userInformation["mobile_number"] as! String
                }
                self.ShowLoader()
                APIFunctions.updateOfferHistory(dictParams: ["outlet_id":self.arrAwards[indexPath.row].outlet_id ?? "", "mobile_number":userID , "offer_type": self.arrAwards[indexPath.row].offer_type ?? "normal", "offer_code":self.arrAwards[indexPath.row].offer_code ?? "", "is_auto_triggered":self.arrAwards[indexPath.row].isAutoTriggerd ?? 0]) { (status, resObj) in
                    //            objBeaconDetected?.name ?? "1"
                    self.HideLoader()
                }
                alertController.dismiss(animated: true, completion: nil)
                
            }
            alertController.addAction(firstAction)
            self.present(alertController, animated: true, completion: nil)
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return arrAwards[indexPath.row].isExpanded ? UITableView.automaticDimension : 190
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            let inOutVC = self.storyboard?.instantiateViewController(withIdentifier: "InOutPopUpVC") as! InOutPopUpVC
            inOutVC.modalPresentationStyle = .overCurrentContext
            inOutVC.modalTransitionStyle = .coverVertical
            
            self.navigationController?.present(inOutVC, animated: true, completion: nil)
            
            inOutVC.onDismissChangedHandler = { (status,data) in
                let strPremise = data as? String
                if strPremise == "InPremise" {
                    
                    
                    if self.isBluetoothOn {
                        let initialViewController = self.storyboard!.instantiateViewController(withIdentifier: "InPremiseLandingVC") as! InPremiseLandingVC
                        initialViewController.objSelectedBranchID = self.arrAwards[indexPath.row].outlet_id
                        initialViewController.imgUrl = self.arrAwards[indexPath.row].image_path ?? ""
                        //initialViewController.objSelectedBranch?.category = self.selectedOutlet?.category
                        self.navigationController?.pushViewController(initialViewController, animated: true)
                    } else {
                        self.alert(message: "You need to switch on your bluetooth first.", title: "Alert!")
                    }
                    
                } else {
                    let initialViewController = self.storyboard!.instantiateViewController(withIdentifier: "OutPremiseLandingVC") as! OutPremiseLandingVC
                    
                    let obj = ["id":self.arrAwards[indexPath.row].outlet_id, "name":self.arrAwards[indexPath.row].name, "description":self.arrAwards[indexPath.row].offer_description, "newcustomer":self.arrAwards[indexPath.row].newcustomer,"image_path": self.arrAwards[indexPath.row].image_path ?? ""]
                    //let dataExample: Data = NSKeyedArchiver.archivedData(withRootObject: obj)
                    do {
                        let jsonData = try JSONSerialization.data(withJSONObject: obj, options: .prettyPrinted)
                        // here "jsonData" is the dictionary encoded in JSON data
                        let jsonDecoder = JSONDecoder()
                        let objTesd = try jsonDecoder.decode(Branches.self, from: jsonData)
                        initialViewController.outObj = objTesd
                    }
                    catch {
                        
                    }
                    //initialViewController.outObj?.category = self.selectedOutlet?.category
                    self.navigationController?.pushViewController(initialViewController, animated: true)
                }
                
            }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
