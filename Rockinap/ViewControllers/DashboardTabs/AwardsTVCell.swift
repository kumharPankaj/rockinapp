//
//  AwardsTVCell.swift
//  Rockinap
//
//  Created by Pankaj Kumhar on 13/07/19.
//  Copyright © 2019 funndynamix. All rights reserved.
//

import UIKit

class AwardsTVCell: UITableViewCell {

    @IBOutlet weak var imgViewAward: UIImageView!
    @IBOutlet weak var lblAwardTitle: UILabel!
    @IBOutlet weak var lblAwardDesc: UILabel!
    @IBOutlet weak var lblExpiryDate: UILabel!
    @IBOutlet weak var lblCreatedDate: UILabel!
    @IBOutlet weak var btnDropDown: UIButton!
    var dropCownAwardClicked: ((AwardsTVCell) -> Void)?
    var redeemAwardClicked: ((AwardsTVCell) -> Void)?
    @IBOutlet weak var btnRedeem: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setUpOffersCell(objOutlet: Awards) {
        
        self.lblAwardTitle.text = objOutlet.offer_name
        
        self.btnDropDown.setImage(objOutlet.isExpanded ? UIImage.init(named: "arrowUp") : UIImage.init(named: "arrowDown"), for: .normal)
        var strDesc = "Outlet Name: " + (objOutlet.name ?? "") + "\n"
        strDesc = strDesc + "Type: " + objOutlet.offer_type! + "\n" + objOutlet.offer_description! + "\n"
        strDesc = strDesc + "Award Message: " + objOutlet.offer_message!
        self.lblAwardDesc.text = strDesc
        self.lblExpiryDate.text = "Expires on: " + objOutlet.offer_redeem_expires_on!
        self.lblCreatedDate.text = "Received on: " + objOutlet.offer_sent_on!
        if objOutlet.redeem_status == 1 {
            self.btnRedeem.setTitle("Redeemed", for: .normal)
            self.btnRedeem.setTitleColor(.red, for: .normal)
            self.btnRedeem.isUserInteractionEnabled = false
        } else {
            self.btnRedeem.setTitle("Redeem", for: .normal)
            self.btnRedeem.setTitleColor(.green, for: .normal)
            self.btnRedeem.isUserInteractionEnabled = true
        }
        let url = URL(string: objOutlet.image_url?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? "")
        self.imgViewAward.kf.setImage(with: url, placeholder:UIImage.init(named: "loading") )
    }
    
    @IBAction func btnDropDownClicked(_ sender: UIButton) {
        dropCownAwardClicked?(self)
    }
    @IBAction func btnRedeemClicked(_ sender: UIButton) {
        redeemAwardClicked?(self)
    }
}
