//
//  NotifcationVC.swift
//  Rockinap
//
//  Created by Pankaj Kumhar on 3/21/19.
//  Copyright © 2019 funndynamix. All rights reserved.
//

import UIKit

class NotifcationVC: CommonViewController {

    @IBOutlet weak var tblViewNotification: UITableView!
    var arrNotification = [Notification]()
    
    var isFromOutlet = false
    var notiOutletID = ""
    override func viewDidLoad() {
        super.viewDidLoad()

        super.backViewNeeded()
        self.title = "Notificaion"
        
        getNotifications()
        
        // Do any additional setup after loading the view.
    }
    

    func getNotifications()  {
        
        self.ShowLoader()
        let id:String = ""
        if let userInformation = UserDefaults.standard.dictionary(forKey: "userInformation") {
            var dictParams = [String: Any]()
            if isFromOutlet {
                dictParams = ["outlet_id": notiOutletID, "uuid":userInformation["userID"] as! String, "sort_type":"desc","device_id":id.getUUID() ?? "", "limit":""]
                
            } else {
                dictParams = ["device_id":id.getUUID() ?? "", "sort_type":"desc", "limit":""]
            }
            APIFunctions.getAllNotification(dictParams: dictParams) { (status, resObj) in
                self.HideLoader()
                if let objOutles = resObj as? NotificationModel{
                    if objOutles.notificationList?.count ?? 0 > 0 {
                        self.arrNotification = objOutles.notificationList!
                        self.viewDidLayoutSubviews()
                        self.tblViewNotification.setNeedsLayout()
                        self.tblViewNotification.layoutIfNeeded()
                        self.tblViewNotification.reloadData()
                    } else {
                        self.alert(message: "You have not received any notification yet.", title: "Alert")
                    }
                } else {
                    let alertController = UIAlertController(title:"Alert!" , message: "Please login again to enjoy Rockinap.", preferredStyle:.alert)
                    let firstAction = UIAlertAction(title: "Logout", style: .default) { (alert: UIAlertAction!) -> Void in
                        if let token = UserDefaults.standard.value(forKey: defaultsKeys.fcmToken) {
                            var user = ""
                            if let userInformation = UserDefaults.standard.dictionary(forKey: "userInformation") {
                                user = userInformation["userID"] as! String
                                
                            }
                            let id:String = ""
                            self.ShowLoader()
                            APIFunctions.logoutAPICall(dictParams: ["uuid":user, "device_id":id.getUUID() ?? ""]) { (status, resObj) in
                                //            objBeaconDetected?.name ?? "1"
                                
                                self.HideLoader()
                                self.resetDefaults()
                                DrawerRootViewController.shared().logout()
                                //self.navigationController?.popViewController(animated: true)
                            }
                            
                        } else {
                            self.resetDefaults()
                            DrawerRootViewController.shared().logout()
                        }
                        
                        
                        alertController.dismiss(animated: true, completion: nil)
                    }
                    
                    alertController.addAction(firstAction)
                    self.present(alertController, animated: true, completion: nil)
                }
                print("")
            }
        }
        
    }
    
   
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    //MARK:- Table view methods
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
            return arrNotification.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellNotification")
        //cell?.textLabel?.text = arrNotification[indexPath.row].title
        
            let url = URL(string: arrNotification[indexPath.row].image_url?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? "")
            
            (cell?.subviews[0].subviews[0] as! UIImageView).kf.setImage(with: url, placeholder:UIImage.init(named: "loading") )
            (cell?.subviews[0].subviews[1] as! UILabel).text = arrNotification[indexPath.row].title
            //(cell?.subviews[0].subviews[2] as! UILabel).text = arrNotification[indexPath.row].send_timestamp
            
            let timeinterval : TimeInterval = Double(arrNotification[indexPath.row].send_timestamp ?? "0.0")!
            let dateFromServer = NSDate(timeIntervalSince1970:timeinterval)
            let dateFormater : DateFormatter = DateFormatter()
            dateFormater.dateFormat = "dd-MMM-yyyy hh:mm a"
        (cell?.subviews[0].subviews[2] as! UITextView).text = (arrNotification[indexPath.row].web_url ?? "") + "\n" +  (arrNotification[indexPath.row].description ?? "")
        
        
        (cell?.subviews[0].subviews[3] as! UILabel).text = dateFormater.string(from: dateFromServer as Date)
        
        return cell ?? UITableViewCell()
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableView.automaticDimension
    }
    

}
