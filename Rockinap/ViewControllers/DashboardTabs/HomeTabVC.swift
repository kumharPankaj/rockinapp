//
//  HomeTabVC.swift
//  Rockinap
//
//  Created by Pankaj Kumhar on 4/6/19.
//  Copyright © 2019 funndynamix. All rights reserved.
//

import UIKit

class HomeTabVC: UITabBarController,UITabBarControllerDelegate  {

    @IBOutlet weak var homeTabBar: UITabBar!
    static var sharedManager: HomeTabVC =
    {
        let tempSharedManager = HomeTabVC.init()
        
        return tempSharedManager
    }()
    
    class var shared: HomeTabVC
    {
        return sharedManager
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        
       
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let items = tabBar.items {
            for item in items {
                item.title = ""
            }
        }
        self.tabBar.tintColor = .gray
        self.title = ""
    }
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        print("Selected item", item.tag )
        if item.tag == 1{
//            let initialViewController = self.storyboard!.instantiateViewController(withIdentifier: "AwardsVC") as! AwardsVC
//            self.navigationController?.pushViewController(initialViewController, animated: true)
            let initialViewController = self.storyboard!.instantiateViewController(withIdentifier: "NotifcationVC") as! NotifcationVC
            self.navigationController?.pushViewController(initialViewController, animated: true)
        }
    }
    
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        print("Selected view controller", viewController)
        print("index", tabBarController.selectedIndex )
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
