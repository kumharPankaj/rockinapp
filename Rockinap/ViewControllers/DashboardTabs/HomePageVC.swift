//
//  HomePageVC.swift
//  Rockinap
//
//  Created by orionspica on 02/10/18.
//  Copyright © 2018 funndynamix. All rights reserved.
//

import UIKit
//import SQLite
import StoreKit
class HomePageVC: CommonViewController, CLLocationManagerDelegate, SKStoreProductViewControllerDelegate {

    @IBOutlet weak var imgViewGlobe: UIImageView!
    @IBOutlet weak var viewShppinOut: UIView!
    @IBOutlet weak var viewChillinOut: UIView!
    @IBOutlet weak var viewNightinOut: UIView!
    @IBOutlet weak var viewEatinOut: UIView!
    private var layer1:CAShapeLayer?
    private var layer2:CAShapeLayer?
    private var layer3:CAShapeLayer?
    private var layer4:CAShapeLayer?
    var selectedMenu: Menu?
    var isLinesDrawn = false
    var currentLocation: CLLocation!
    private var locationManager = CLLocationManager()
    override func viewDidLoad() {
        super.viewDidLoad()
        //openStoreProductWithiTunesItemIdentifier(identifier: "1476956035")
        self.isNavigationBarRequire()
        self.title = "Home"
        locationManager.delegate = self
        locationManager.requestAlwaysAuthorization()
        
        if( CLLocationManager.authorizationStatus() == .authorizedWhenInUse ||
            CLLocationManager.authorizationStatus() ==  .authorizedAlways){
            
            currentLocation = locationManager.location
            
        }
//        let db = try Connection(fileURL)
//
//        let users = Table("users")
//        let id = Expression<Int64>("id")
//        let name = Expression<String?>("name")
//        let email = Expression<String>("email")
//        
//        try db.run(users.create { t in
//            t.column(id, primaryKey: true)
//            t.column(name)
//            t.column(email, unique: true)
//        })
//
        
        
       let viewArray = Bundle.main.loadNibNamed("NavigationViews", owner: self, options: nil)
        let viewNAv = viewArray![0] as! NavigationViews
        viewNAv.frame  = CGRect.init(x: self.navigationController?.navigationBar.frame.origin.x ?? 0, y: -(UIApplication.shared.statusBarFrame.height), width: self.navigationController?.navigationBar.frame.width ?? self.view.frame.width, height: (self.navigationController?.navigationBar.frame.height)! + UIApplication.shared.statusBarFrame.height)
        //(viewArray?[0] as! NavigationViews).imgLogo.image = (viewArray?[0] as! NavigationViews).imgLogo.image?.maskWithColorNew(color: UIColor.red)
        viewNAv.btnMenu.addTarget(self, action: #selector(self.logoutUserClicked), for: .touchUpInside)
        self.navigationController?.navigationBar.addSubview(viewArray![0] as! UIView)
        
    }
   
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setView(view: viewShppinOut, hidden: false)
    }
    func setView(view: UIView, hidden: Bool) {
       
        UIView.transition(with: view, duration: 0.7, options: .transitionCrossDissolve, animations: {
            view.isHidden = hidden
        }) { (true) in
            UIView.transition(with: self.viewChillinOut, duration: 0.5, options: .transitionCrossDissolve, animations: {
                self.viewChillinOut.isHidden = hidden
            }) { (true) in
                UIView.transition(with: self.viewNightinOut, duration: 0.5, options: .transitionCrossDissolve, animations: {
                    self.viewNightinOut.isHidden = hidden
                }) { (true) in
                    UIView.transition(with: self.viewEatinOut, duration: 0.5, options: .transitionCrossDissolve, animations: {
                        self.viewEatinOut.isHidden = hidden
                    }) { (true) in
                        
                    }
                }
            }
        }
       
    }
    override func viewWillLayoutSubviews() {
        //if !isLinesDrawn {

        layer1?.removeFromSuperlayer()
        layer2?.removeFromSuperlayer()
        layer3?.removeFromSuperlayer()
        layer4?.removeFromSuperlayer()
        
        
        layer1 = self.drawLineForDashoard(xPosStart: Int(viewChillinOut.frame.origin.x + viewChillinOut.frame.width - 10), yPosStart: Int(viewChillinOut.frame.origin.y + (viewChillinOut.frame.height/2) - 10), xPosEnd: Int(imgViewGlobe.frame.origin.x + 20), yPosEnd: Int(viewChillinOut.frame.origin.y + (viewChillinOut.frame.height/2) - 10))

        layer2 = (self.drawLineForDashoard(xPosStart: Int(imgViewGlobe.frame.origin.x + 20), yPosStart: Int(viewChillinOut.frame.origin.y + (viewChillinOut.frame.height/2) - 10), xPosEnd: Int(imgViewGlobe.frame.origin.x + 50), yPosEnd: Int(viewChillinOut.frame.origin.y + (viewChillinOut.frame.height/2) + 20)))
        
       /* layer1 = self.drawLineForDashoard(xPosStart: Int(viewChillinOut.frame.origin.x + viewChillinOut.frame.width - 10), yPosStart: Int(viewChillinOut.frame.origin.y + (viewChillinOut.frame.height/2) - 10), xPosEnd: Int(viewChillinOut.frame.origin.x + viewChillinOut.frame.width + 40), yPosEnd: Int(viewChillinOut.frame.origin.y + (viewChillinOut.frame.height/2) - 10))
        layer2 = (self.drawLineForDashoard(xPosStart: Int(viewChillinOut.frame.origin.x + viewChillinOut.frame.width + 40), yPosStart: Int(viewChillinOut.frame.origin.y + (viewChillinOut.frame.height/2) - 10), xPosEnd: Int(imgViewGlobe.frame.origin.x + 55), yPosEnd: Int(viewChillinOut.frame.origin.y + (viewChillinOut.frame.height/2) + 20)))*/
        
        layer3 = self.drawLineForDashoard(xPosStart: Int(viewNightinOut.frame.origin.x + viewNightinOut.frame.width - 10), yPosStart: Int(viewNightinOut.frame.origin.y + (viewNightinOut.frame.height/2) + 5), xPosEnd: Int(imgViewGlobe.frame.origin.x + 20), yPosEnd: Int(viewNightinOut.frame.origin.y + (viewNightinOut.frame.height/2) + 5))
        layer4 = (self.drawLineForDashoard(xPosStart: Int(imgViewGlobe.frame.origin.x + 20), yPosStart: Int(viewNightinOut.frame.origin.y + (viewNightinOut.frame.height/2) + 5), xPosEnd: Int(imgViewGlobe.frame.origin.x + 54), yPosEnd: Int(viewNightinOut.frame.origin.y + (viewNightinOut.frame.height/2) - 20)))
        view.layer.addSublayer(layer1!)
        view.layer.addSublayer(layer2!)
        view.layer.addSublayer(layer3!)
        view.layer.addSublayer(layer4!)
        view.bringSubviewToFront(imgViewGlobe)
        //}
    }
    func updateUIfromMenuSelection()
    {
    }
    func openStoreProductWithiTunesItemIdentifier(identifier: String) {
        let storeViewController = SKStoreProductViewController()
        storeViewController.delegate = self
        
        let parameters = [ SKStoreProductParameterITunesItemIdentifier : identifier]
        storeViewController.loadProduct(withParameters: parameters) { [weak self] (loaded, error) -> Void in
            if loaded {
                // Parent class of self is UIViewContorller
                self?.present(storeViewController, animated: true, completion: nil)
            }
        }
    }
    
    func productViewControllerDidFinish(_ viewController: SKStoreProductViewController) {
        viewController.dismiss(animated: true, completion: nil)
    }
    @IBAction func btnCategoryClicked(_ sender: UIButton) {
        let outletVC = self.storyboard!.instantiateViewController(withIdentifier: "OutletListVC") as! OutletListVC
        switch sender.tag {
        case 1:
            selectedCategory = OutleCategory.ShopinOut
        case 2:
            selectedCategory = OutleCategory.ChilinOut
        case 3:
            selectedCategory = OutleCategory.NightinOut
        case 4:
            selectedCategory = OutleCategory.EatinOut
        default:
            print("default")
        }
        self.navigationController?.pushViewController(outletVC, animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
