//
//  ActivityVC.swift
//  Rockinap
//
//  Created by Pankaj Kumhar on 2/23/19.
//  Copyright © 2019 funndynamix. All rights reserved.
//

import UIKit

class ActivityVC: CommonViewController {

    @IBOutlet weak var tblViewActivity: UITableView!
    @IBOutlet weak var slectedTab: UITabBarItem!
    override func viewDidLoad() {
        super.viewDidLoad()
        let viewHeader = UIView()
        
        if self.navigationController != nil {
            viewHeader.frame  = CGRect.init(x: self.navigationController?.navigationBar.frame.origin.x ?? 0, y: (self.navigationController?.navigationBar.frame.origin.y)! + (self.navigationController?.navigationBar.frame.height)! , width: self.navigationController?.navigationBar.frame.width ?? self.view.frame.width, height: 30)
            let btnBAck = UIButton.init(frame: CGRect.init(x: 5, y: 0, width: 30, height: 30))
            btnBAck.setTitleColor(UIColor.lightGray, for: .normal)
            //btnBAck.setTitle("< Back", for: .normal)
            btnBAck.setBackgroundImage(UIImage.init(named: "back_nav")?.maskWithColor(color: UIColor.darkGray), for: .normal)
            btnBAck.addTarget(self, action: #selector(btnBackForTabClicked), for: .touchUpInside)
            viewHeader.addSubview(btnBAck)
            self.view.addSubview(viewHeader)
        }
        // Do any additional setup after loading the view.
    }
    
    @objc private func btnBackForTabClicked() {
        self.tabBarController?.selectedIndex = 0
        //HomeTabVC.shared.homeTabBar.selectedItem = HomeTabVC.shared.homeTabBar.items?[0]
    }
    //MARK:- Table view methods
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
       
        return 3
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellActivity")
        let lblTitle = cell?.viewWithTag(2) as! UILabel
        switch indexPath.row {
        case 0:
            lblTitle.text = "Offers"
        case 1:
            lblTitle.text = "Awards"
        case 2:
            lblTitle.text = "Buddies"
        default:
            print("")
        }
       
        return cell ?? UITableViewCell()
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 80
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath.row {
        case 0:
            //lblTitle.text = "Offers"
            let initialViewController = self.storyboard!.instantiateViewController(withIdentifier: "OffersListVC") as! OffersListVC
            initialViewController.offersGlobal = true
            self.navigationController?.pushViewController(initialViewController, animated: true)
        case 1:
            //lblTitle.text = "Notifications"
            let initialViewController = self.storyboard!.instantiateViewController(withIdentifier: "AwardsVC") as! AwardsVC
            self.navigationController?.pushViewController(initialViewController, animated: true)
//            let initialViewController = self.storyboard!.instantiateViewController(withIdentifier: "NotifcationVC") as! NotifcationVC
//            self.navigationController?.pushViewController(initialViewController, animated: true)
        case 2:
            //lblTitle.text = "Buddies"
            let initialViewController = self.storyboard!.instantiateViewController(withIdentifier: "InPremiseUserList") as! InPremiseUserList
            initialViewController.isFromBuddies = true
            self.navigationController?.pushViewController(initialViewController, animated: true)
        default:
            print("")
        }
        
        
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
