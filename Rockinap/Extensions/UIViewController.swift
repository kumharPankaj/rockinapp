//
//  UIViewControllerExtension.swift
//  SlideMenuControllerSwift
//
//  Created by Yuji Hato on 1/19/15.
//  Copyright (c) 2015 Yuji Hato. All rights reserved.
//

import UIKit
import MBProgressHUD
extension UIViewController: UIActionSheetDelegate
{
   
    func alert(message: String, title: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func addGradientColors_Nav()
    {
        extendedLayoutIncludesOpaqueBars = true
        self.navigationItem.setHidesBackButton(true, animated:true);
        var colors = [UIColor]()
        colors.append(UIColor.init().hexStringToUIColor(hex: ColorCodes.color_FCB315))
        //self.navigationController?.navigationBar.setGradientBackground(colors: colors)
        self.navigationController?.navigationBar.barTintColor =  UIColor.init().hexStringToUIColor(hex: ColorCodes.color_FCB315)
        self.navigationController?.navigationBar.tintColor = UIColor.white

        self.navigationController?.navigationBar.titleTextAttributes = [
            NSAttributedString.Key.foregroundColor: UIColor.white
        ]
       /* if #available(iOS 11.0, *)
        {
            self.navigationController?.navigationBar.prefersLargeTitles = true
            self.navigationController?.navigationBar.largeTitleTextAttributes = [
                NSAttributedString.Key.foregroundColor: UIColor.white
            ]
        }
        else {
            // Fallback on earlier versions
        }*/
    }
    
    func addCornerRadius(view:UIView)
    {
        view.layer.cornerRadius = 30
        view.layer.masksToBounds = true
    }
   
    
    func addLineToView(view : UIView, position : LINE_POSITION, color: UIColor, width: Double) {
        let lineView = UIView()
        lineView.backgroundColor = color
        lineView.translatesAutoresizingMaskIntoConstraints = false // This is important!
        view.addSubview(lineView)
        
        let metrics = ["width" : NSNumber(value: width)]
        let views = ["lineView" : lineView]
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[lineView]|", options:NSLayoutConstraint.FormatOptions(rawValue: 0), metrics:metrics, views:views))
        
        switch position {
        case .LINE_POSITION_TOP:
            view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[lineView(width)]", options:NSLayoutConstraint.FormatOptions(rawValue: 0), metrics:metrics, views:views))
            break
        case .LINE_POSITION_BOTTOM:
            view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[lineView(width)]|", options:NSLayoutConstraint.FormatOptions(rawValue: 0), metrics:metrics, views:views))
            break
        default:
            break
        }
    }
    
    func drawLineForDashoard(xPosStart: Int, yPosStart: Int, xPosEnd: Int, yPosEnd: Int) -> CAShapeLayer{
        let path = UIBezierPath()
        path.move(to: CGPoint(x:xPosStart, y:yPosStart))
        
        
        path.addLine(to: CGPoint(x:xPosEnd, y:yPosEnd))
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = path.cgPath
        shapeLayer.strokeColor = UIColor.darkGray.cgColor
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.lineWidth = 1
        
        
        return shapeLayer
        
    }
    func ShowLoader()
    {
        DispatchQueue.main.async
            {
                MBProgressHUD.showAdded(to: self.view, animated: true)
        }
    }
    
    func HideLoader()
    {
        DispatchQueue.main.async
            {
                MBProgressHUD.hide(for: self.view, animated: true)
        }
    }
    
    @objc func logoutUserClicked() {
        
        let alert = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "About Rockinap", style: .default , handler:{ (UIAlertAction)in
            let alertController = UIAlertController(title:"About Rockinap" , message: "Dear User \n Thank You for downloading.\n\nRockinap connects you to your favourite Cafes, Clubs, Pubs, Restaurants, Stores and Brands. It's a gateway to receive real time offers, freebies and entries. Play exciting games like Treasure Hunt and Quiz, win instant and time bound awards. Network with other customers and build your buddy lists. Have you ever asked your favourite café or a restaurant to give you a discount? It really doesn’t hurt to have Rockinap parked on your smart phone; it rather makes it more smarter.\nSo happy rockinapping….", preferredStyle:.alert)
            let firstAction = UIKit.UIAlertAction(title: "Ok", style: .default) { (alert: UIAlertAction!) -> Void in
                alertController.dismiss(animated: true, completion: nil)
            }
            alertController.addAction(firstAction)
            self.present(alertController, animated: true, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Disclaimer", style: .default , handler:{ (UIAlertAction)in
            let alertController = UIAlertController(title:"Dear user", message: "Rockinap uses Bluetooth and your location to help you to make the best use of the Rockinap’s functionalities. Rockinap compatible premises (cafes, pubs, restaurants and stores) use ibeacons within the premises that your Rockinap detects and allows you to avail all the functionalities and offerings that the Brand has to offer you. We strictly use your data that you give us in terms of your cell No and e-mail address; to enable the brand managers to communicate and notify you with offerings and awards. Rockinap only communicates through its app.\n\nBluetooth on your smart phone only helps to detect the ibeacon In-premises and does not capture any data but only helps to trigger offers and helps in the Treasure Hunt and Quiz games.\n\nBe rest assured we do not disclose any data to anyone nor we sell data.", preferredStyle:.alert)
            let firstAction = UIKit.UIAlertAction(title: "Ok", style: .default) { (alert: UIAlertAction!) -> Void in
                alertController.dismiss(animated: true, completion: nil)
            }
            alertController.addAction(firstAction)
            self.present(alertController, animated: true, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Logout", style: .default , handler:{ (UIAlertAction)in
            self.logoutAlert()
        }))
       
        alert.addAction(UIAlertAction(title: "Cancel", style: .destructive , handler:{ (UIAlertAction)in
            self.dismiss(animated: true, completion: nil)
        }))
        self.present(alert, animated: true, completion: {
            print("completion block")
        })
        
        
        
        
        
        
    }
    func logoutAlert() -> Void {
        let alertController = UIAlertController(title:"Alert!" , message: "Are you sure you want to logout?", preferredStyle:.alert)
        let firstAction = UIAlertAction(title: "Ok", style: .default) { (alert: UIAlertAction!) -> Void in
            if let token = UserDefaults.standard.value(forKey: defaultsKeys.fcmToken) {
                var user = ""
                if let userInformation = UserDefaults.standard.dictionary(forKey: "userInformation") {
                    user = userInformation["userID"] as! String
                    
                }
                let id:String = ""
                self.ShowLoader()
                
                if AppDelegate.shareddelegate().isUserInpremise {
                    var userID = ""
                    if let userInformation = UserDefaults.standard.dictionary(forKey: "userInformation") {
                        userID = userInformation["mobile_number"] as! String
                    }
                    SocketIOManager.sharedInstance.closeConnection()
                    APIFunctions.leaveOutlet(dictParams: ["outlet_id":AppDelegate.shareddelegate().liveBranchId, "mobile_number":userID]) { (status, obj) in
                            
                        APIFunctions.logoutAPICall(dictParams: ["uuid":user, "device_id":id.getUUID() ?? ""]) { (status, resObj) in
                            self.HideLoader()
                            self.resetDefaults()
                            DrawerRootViewController.shared().logout()
                            //self.navigationController?.popViewController(animated: true)
                        }
                    }
                } else {
                    APIFunctions.logoutAPICall(dictParams: ["uuid":user, "device_id":id.getUUID() ?? ""]) { (status, resObj) in
                        self.HideLoader()
                        self.resetDefaults()
                        DrawerRootViewController.shared().logout()
                        //self.navigationController?.popViewController(animated: true)
                    }
                }
                
                
                
            } else {
                self.resetDefaults()
                DrawerRootViewController.shared().logout()
            }
            
            
            alertController.dismiss(animated: true, completion: nil)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (alert: UIAlertAction!) -> Void in
            alertController.dismiss(animated: true, completion: nil)
        }
        alertController.addAction(firstAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    func resetDefaults() {
        
        UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
        UserDefaults.standard.synchronize()
    }
}
extension UINavigationBar {
    
    func setGradientBackground(colors: [UIColor]) {
        
        var updatedFrame = CGRect.init(x:0, y: 0, width: self.frame.width, height:  self.frame.height)
        updatedFrame.size.height += 20
        let gradientLayer = CAGradientLayer(frame: updatedFrame, colors: colors)
        
        setBackgroundImage(gradientLayer.creatGradientImage(), for: UIBarMetrics.default)
    }
}
extension CAGradientLayer {
    
    convenience init(frame: CGRect, colors: [UIColor]) {
        self.init()
        self.frame = frame
        self.colors = []
        for color in colors {
            self.colors?.append(color.cgColor)
        }
        startPoint = CGPoint(x: 0, y: 0.5)
        endPoint = CGPoint(x: 1, y: 0.5)
    }
    
    func creatGradientImage() -> UIImage? {
        
        var image: UIImage? = nil
        UIGraphicsBeginImageContext(bounds.size)
        if let context = UIGraphicsGetCurrentContext() {
            render(in: context)
            image = UIGraphicsGetImageFromCurrentImageContext()
        }
        UIGraphicsEndImageContext()
        return image
    }
    
}

extension CALayer {
    
    func addBorder(edges: [UIRectEdge], color: UIColor, thickness: CGFloat) {
        
        for edge in edges {
            let border = CALayer();
            switch edge {
            case UIRectEdge.top:
                border.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: thickness)
                break
            case UIRectEdge.bottom:
                border.frame = CGRect(x:0, y:self.frame.height - thickness, width:self.frame.width, height:thickness)
                break
            case UIRectEdge.left:
                border.frame = CGRect(x:0, y:0, width: thickness, height: self.frame.height)
                break
            case UIRectEdge.right:
                border.frame = CGRect(x:self.frame.width - thickness, y: 0, width: thickness, height:self.frame.height)
                break
            default:
                break
            }
            
            border.backgroundColor = color.cgColor;
            self.addSublayer(border)
        }
    }
    
}
