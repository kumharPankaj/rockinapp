//
//  TransactionDetails.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on August 30, 2018

import Foundation


class TransactionDetails : NSObject, NSCoding{

    var dataDetails : [DataDetail]!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        dataDetails = [DataDetail]()
        if let dataDetailsArray = dictionary["dataDetails"] as? [[String:Any]]{
            for dic in dataDetailsArray{
                let value = DataDetail(fromDictionary: dic)
                dataDetails.append(value)
            }
        }
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if dataDetails != nil{
            var dictionaryElements = [[String:Any]]()
            for dataDetailsElement in dataDetails {
                dictionaryElements.append(dataDetailsElement.toDictionary())
            }
            dictionary["dataDetails"] = dictionaryElements
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        dataDetails = aDecoder.decodeObject(forKey: "dataDetails") as? [DataDetail]
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if dataDetails != nil{
            aCoder.encode(dataDetails, forKey: "dataDetails")
        }
    }
}
struct SettingsModel : Codable {
    let error : Bool?
    let message : String?
    let data : [SettingObject]?
    
    enum CodingKeys: String, CodingKey {
        
        case error = "error"
        case message = "message"
        case data = "data"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        error = try values.decodeIfPresent(Bool.self, forKey: .error)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent([SettingObject].self, forKey: .data)
    }
    
}
struct SettingObject : Codable {
    let id : Int?
    let outlet_id : String?
    let setting_key : String?
    let value : Int?
    let created_at : String?
    let updated_at : String?
    
    enum CodingKeys: String, CodingKey {
        
        case id = "id"
        case outlet_id = "outlet_id"
        case setting_key = "setting_key"
        case value = "value"
        case created_at = "created_at"
        case updated_at = "updated_at"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        outlet_id = try values.decodeIfPresent(String.self, forKey: .outlet_id)
        setting_key = try values.decodeIfPresent(String.self, forKey: .setting_key)
        value = try values.decodeIfPresent(Int.self, forKey: .value)
        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
        updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
    }
    
}
