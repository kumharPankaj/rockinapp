//
//  DataDetail.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on August 30, 2018

import Foundation

class DataDetail : NSObject, NSCoding{

    var transactionammount : String!
    var transactionDate : String!
    var transactionid : String!
    var transactionimagepath : String!
    var transactionnote : String!
    var transactionstatus : String!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        transactionammount = dictionary["transactionammount"] as? String
        transactionDate = dictionary["transactionDate"] as? String
        transactionid = dictionary["transactionid"] as? String
        transactionimagepath = dictionary["transactionimagepath"] as? String
        transactionnote = dictionary["transactionnote"] as? String
        transactionstatus = dictionary["transactionstatus"] as? String
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if transactionammount != nil{
            dictionary["transactionammount"] = transactionammount
        }
        if transactionDate != nil{
            dictionary["transactionDate"] = transactionDate
        }
        if transactionid != nil{
            dictionary["transactionid"] = transactionid
        }
        if transactionimagepath != nil{
            dictionary["transactionimagepath"] = transactionimagepath
        }
        if transactionnote != nil{
            dictionary["transactionnote"] = transactionnote
        }
        if transactionstatus != nil{
            dictionary["transactionstatus"] = transactionstatus
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        transactionammount = aDecoder.decodeObject(forKey: "transactionammount") as? String
        transactionDate = aDecoder.decodeObject(forKey: "transactionDate") as? String
        transactionid = aDecoder.decodeObject(forKey: "transactionid") as? String
        transactionimagepath = aDecoder.decodeObject(forKey: "transactionimagepath") as? String
        transactionnote = aDecoder.decodeObject(forKey: "transactionnote") as? String
        transactionstatus = aDecoder.decodeObject(forKey: "transactionstatus") as? String
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if transactionammount != nil{
            aCoder.encode(transactionammount, forKey: "transactionammount")
        }
        if transactionDate != nil{
            aCoder.encode(transactionDate, forKey: "transactionDate")
        }
        if transactionid != nil{
            aCoder.encode(transactionid, forKey: "transactionid")
        }
        if transactionimagepath != nil{
            aCoder.encode(transactionimagepath, forKey: "transactionimagepath")
        }
        if transactionnote != nil{
            aCoder.encode(transactionnote, forKey: "transactionnote")
        }
        if transactionstatus != nil{
            aCoder.encode(transactionstatus, forKey: "transactionstatus")
        }
    }
}
