//
//  CommonModel.swift
//  Rockinap
//
//  Created by orionspica on 30/09/18.
//  Copyright © 2018 funndynamix. All rights reserved.
//

import UIKit

struct OutletModel: Codable {
    let auth : Bool?
    let outletList : [Outlets]?
    
    enum CodingKeys: String, CodingKey {
        
        case auth = "auth"
        case outletList = "data"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        auth = try values.decodeIfPresent(Bool.self, forKey: .auth)
        outletList = try values.decodeIfPresent([Outlets].self, forKey: .outletList)
    }
    
}
struct Outlets : Codable {
    let organisation_id : String?
    let name : String?
    let uoid : String?
    let description : String?
    let image_path : String?
    var category : String?
    let outlet_count : Int?
    
    enum CodingKeys: String, CodingKey {
        
        case organisation_id = "id"
        case name = "name"
        case uoid = "uoid"
        case description = "description"
        case image_path = "image_path"
        case category = "category"
        case outlet_count = "outlet_count"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        organisation_id = try values.decodeIfPresent(String.self, forKey: .organisation_id)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        uoid = try values.decodeIfPresent(String.self, forKey: .uoid)
        description = try values.decodeIfPresent(String.self, forKey: .description)
        image_path = try values.decodeIfPresent(String.self, forKey: .image_path)
        category = try values.decodeIfPresent(String.self, forKey: .category)
        outlet_count = try values.decodeIfPresent(Int.self, forKey: .outlet_count)
    }
    
}

struct ProductModel : Codable {
    let auth : Bool?
    let productList : [Products]?
    
    enum CodingKeys: String, CodingKey {
        
        case auth = "auth"
        case productList = "data"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        auth = try values.decodeIfPresent(Bool.self, forKey: .auth)
        productList = try values.decodeIfPresent([Products].self, forKey: .productList)
    }
    
}

struct Products : Codable {
    let pid : String?
    let name : String?
    let description : String?
    let award_message : String?
    let award_type : String?
    let imgae_url : String?
    let image_slider : [String]?
    let avg_rating: Double?
    
    enum CodingKeys: String, CodingKey {
        
        case pid = "pid"
        case name = "name"
        case description = "description"
        case award_message = "award_message"
        case award_type = "award_type"
        case imgae_url = "image_url"
        case image_slider = "image_slider"
        case avg_rating = "avg_rating"
       
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        pid = try values.decodeIfPresent(String.self, forKey: .pid)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        description = try values.decodeIfPresent(String.self, forKey: .description)
        award_message = try values.decodeIfPresent(String.self, forKey: .award_message)
        award_type = try values.decodeIfPresent(String.self, forKey: .award_type)
        imgae_url = try values.decodeIfPresent(String.self, forKey: .imgae_url)
        image_slider = try values.decodeIfPresent([String].self, forKey: .image_slider)
        avg_rating = try values.decodeIfPresent(Double.self, forKey: .avg_rating)
        
    }
    
}

struct AwardsModel : Codable {
    let auth : Bool?
    let awardList : [Awards]?
    
    enum CodingKeys: String, CodingKey {
        
        case auth = "auth"
        case awardList = "data"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        auth = try values.decodeIfPresent(Bool.self, forKey: .auth)
        awardList = try values.decodeIfPresent([Awards].self, forKey: .awardList)
    }
    
}

struct Awards : Codable {
    let offer_name : String?
    let offer_description : String?
    let offer_message : String?
    let image_url : String?
    let location : String?
    let offer_type : String?
    let offer_code : String?
    let outlet_id : String?
    let redeem_status : Int?
    let uuid : String?
    var offer_sent_on : String?
    var offer_redeem_expires_on : String?
    let ob_id : Int?
    let name : String?
    let organisation_id : String?
    let lat : String?
    let lon : String?
    let description : String?
    let image_path : String?
    let costperperson : String?
    let newcustomer : String?
    let category : String?
    var isExpanded = false
    let isAutoTriggerd : Int?
    enum CodingKeys: String, CodingKey {
        
        case offer_name = "offer_name"
        case offer_description = "offer_description"
        case offer_message = "offer_message"
        case image_url = "image_url"
        case location = "location"
        case offer_type = "offer_type"
        case offer_code = "offer_code"
        case outlet_id = "outlet_id"
        case redeem_status = "redeem_status"
        case uuid = "uuid"
        case offer_sent_on = "offer_sent_on"
        case offer_redeem_expires_on = "offer_redeem_expires_on"
        case ob_id = "ob_id"
        case name = "name"
        case organisation_id = "organisation_id"
        case lat = "lat"
        case lon = "lon"
        case description = "description"
        case image_path = "image_path"
        case costperperson = "costperperson"
        case newcustomer = "newcustomer"
        case category = "category"
        case isAutoTriggerd = "is_auto_triggered"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        offer_name = try values.decodeIfPresent(String.self, forKey: .offer_name)
        offer_description = try values.decodeIfPresent(String.self, forKey: .offer_description)
        offer_message = try values.decodeIfPresent(String.self, forKey: .offer_message)
        image_url = try values.decodeIfPresent(String.self, forKey: .image_url)
        location = try values.decodeIfPresent(String.self, forKey: .location)
        offer_type = try values.decodeIfPresent(String.self, forKey: .offer_type)
        offer_code = try values.decodeIfPresent(String.self, forKey: .offer_code)
        outlet_id = try values.decodeIfPresent(String.self, forKey: .outlet_id)
        redeem_status = try values.decodeIfPresent(Int.self, forKey: .redeem_status)
        uuid = try values.decodeIfPresent(String.self, forKey: .uuid)
        offer_sent_on = try values.decodeIfPresent(String.self, forKey: .offer_sent_on)
        offer_redeem_expires_on = try values.decodeIfPresent(String.self, forKey: .offer_redeem_expires_on)
        ob_id = try values.decodeIfPresent(Int.self, forKey: .ob_id)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        organisation_id = try values.decodeIfPresent(String.self, forKey: .organisation_id)
        lat = try values.decodeIfPresent(String.self, forKey: .lat)
        lon = try values.decodeIfPresent(String.self, forKey: .lon)
        description = try values.decodeIfPresent(String.self, forKey: .description)
        image_path = try values.decodeIfPresent(String.self, forKey: .image_path)
        costperperson = try values.decodeIfPresent(String.self, forKey: .costperperson)
        newcustomer = try values.decodeIfPresent(String.self, forKey: .newcustomer)
        category = try values.decodeIfPresent(String.self, forKey: .category)
        isAutoTriggerd = try values.decodeIfPresent(Int.self, forKey: .isAutoTriggerd)
        offer_sent_on = UTCToLocal(date: offer_sent_on ?? "1-1-2019 11:11:11")
        offer_redeem_expires_on = UTCToLocal1(date: offer_redeem_expires_on ?? "1-1-2019")
    }
    func UTCToLocal(date:String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        let dt: Date = dateFormatter.date(from: date)!
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "dd-MM-yyyy HH:mm:ss"
        
        return dateFormatter.string(from: dt)
    }
    func UTCToLocal1(date:String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        let dt: Date = dateFormatter.date(from: date)!
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "dd-MM-yyyy"
        
        return dateFormatter.string(from: dt)
    }
}
struct OutletDetailsModel : Codable {
    let auth : Bool?
    let outletDetailsList : [OutletsDetails]?
    
    enum CodingKeys: String, CodingKey {
        
        case auth = "auth"
        case outletDetailsList = "data"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        auth = try values.decodeIfPresent(Bool.self, forKey: .auth)
        outletDetailsList = try values.decodeIfPresent([OutletsDetails].self, forKey: .outletDetailsList)
    }
    
}
struct OutletsDetails : Codable {
    let name : String?
    let description : String?
    let image_url : String?
    let beacons : [String]?
    var category : String?
    let games_available : [Games_available]?
    
    enum CodingKeys: String, CodingKey {
        
        case name = "name"
        case description = "description"
        case image_url = "image_url"
        case beacons = "beacons"
        case category = "category"
        case games_available = "games_available"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        description = try values.decodeIfPresent(String.self, forKey: .description)
        image_url = try values.decodeIfPresent(String.self, forKey: .image_url)
        beacons = try values.decodeIfPresent([String].self, forKey: .beacons)
        category = try values.decodeIfPresent(String.self, forKey: .category)
        games_available = try values.decodeIfPresent([Games_available].self, forKey: .games_available)
    }
    
}
struct Games_available : Codable {
    let gid : String?
    let game_name : String?
    let game_desc : String?
    let game_logo : String?
    let status : String?
    
    enum CodingKeys: String, CodingKey {
        
        case gid = "gid"
        case game_name = "game_name"
        case game_desc = "game_desc"
        case game_logo = "game_logo"
        case status = "status"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        gid = try values.decodeIfPresent(String.self, forKey: .gid)
        game_name = try values.decodeIfPresent(String.self, forKey: .game_name)
        game_desc = try values.decodeIfPresent(String.self, forKey: .game_desc)
        game_logo = try values.decodeIfPresent(String.self, forKey: .game_logo)
        status = try values.decodeIfPresent(String.self, forKey: .status)
    }
    
}
//MARK:- Offers Model
struct OffersModel : Codable {
    let auth : Bool?
    let offersList : [Offers]?
    
    enum CodingKeys: String, CodingKey {
        
        case auth = "auth"
        case offersList = "data"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        auth = try values.decodeIfPresent(Bool.self, forKey: .auth)
        offersList = try values.decodeIfPresent([Offers].self, forKey: .offersList)
    }
    
}

struct Offers : Codable {
    let ob_id : Int?
    let offer_id : String?
    let offer_name : String?
    let offer_description : String?
    let offer_message : String?
    let image_url : String?
    let total_count : Int?
    let first_time_only : String?
    let beacon_name : String?
    let offer_hours : String?
    let location : String?
    let outlet_id : String?
    let product_offered : String?
    let discount_percent : String?
    let offer_type : String?
    let offer_starts_on : String?
    var expires_on : String?
    let created_on : String?
    var updated_on : String?
    let updated_by : String?
    let offer_code : String?
    let auto_trigger : String?
    let redeem_status : Int?
    let uuid : String?
    let mobile_number : String?
    var offer_sent_on : String?
    let offer_redeem_expires_on : String?
    let name : String?
    let organisation_id : String?
    let lat : String?
    let lon : String?
    let description : String?
    let image_path : String?
    let costperperson : String?
    let newcustomer : String?
    let category : String?
    let webLink : String?
    let isAutoTriggerd : Int?
    var isTapped = false
    enum CodingKeys: String, CodingKey {
        
        case ob_id = "ob_id"
        case offer_id = "offer_id"
        case offer_name = "offer_name"
        case offer_description = "offer_description"
        case offer_message = "offer_message"
        case image_url = "image_url"
        case total_count = "total_count"
        case first_time_only = "first_time_only"
        case beacon_name = "beacon_name"
        case offer_hours = "offer_hours"
        case location = "location"
        case outlet_id = "outlet_id"
        case product_offered = "product_offered"
        case discount_percent = "discount_percent"
        case offer_type = "offer_type"
        case offer_starts_on = "offer_starts_on"
        case expires_on = "expires_on"
        case created_on = "created_on"
        case updated_on = "updated_on"
        case updated_by = "updated_by"
        case offer_code = "offer_code"
        case auto_trigger = "auto_trigger"
        case redeem_status = "redeem_status"
        case uuid = "uuid"
        case mobile_number = "mobile_number"
        case offer_sent_on = "offer_sent_on"
        case offer_redeem_expires_on = "offer_redeem_expires_on"
        case name = "name"
        case organisation_id = "organisation_id"
        case lat = "lat"
        case lon = "lon"
        case description = "description"
        case image_path = "image_path"
        case costperperson = "costperperson"
        case newcustomer = "newcustomer"
        case category = "category"
        case webLink = "web_link"
        case isAutoTriggerd = "is_auto_triggered"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        ob_id = try values.decodeIfPresent(Int.self, forKey: .ob_id)
        offer_id = try values.decodeIfPresent(String.self, forKey: .offer_id)
        offer_name = try values.decodeIfPresent(String.self, forKey: .offer_name)
        offer_description = try values.decodeIfPresent(String.self, forKey: .offer_description)
        offer_message = try values.decodeIfPresent(String.self, forKey: .offer_message)
        image_url = try values.decodeIfPresent(String.self, forKey: .image_url)
        total_count = try values.decodeIfPresent(Int.self, forKey: .total_count)
        first_time_only = try values.decodeIfPresent(String.self, forKey: .first_time_only)
        beacon_name = try values.decodeIfPresent(String.self, forKey: .beacon_name)
        offer_hours = try values.decodeIfPresent(String.self, forKey: .offer_hours)
        location = try values.decodeIfPresent(String.self, forKey: .location)
        outlet_id = try values.decodeIfPresent(String.self, forKey: .outlet_id)
        product_offered = try values.decodeIfPresent(String.self, forKey: .product_offered)
        discount_percent = try values.decodeIfPresent(String.self, forKey: .discount_percent)
        offer_type = try values.decodeIfPresent(String.self, forKey: .offer_type)
        offer_starts_on = try values.decodeIfPresent(String.self, forKey: .offer_starts_on)
        expires_on = try values.decodeIfPresent(String.self, forKey: .expires_on)
        created_on = try values.decodeIfPresent(String.self, forKey: .created_on)
        updated_on = try values.decodeIfPresent(String.self, forKey: .updated_on)
        updated_by = try values.decodeIfPresent(String.self, forKey: .updated_by)
        offer_code = try values.decodeIfPresent(String.self, forKey: .offer_code)
        auto_trigger = try values.decodeIfPresent(String.self, forKey: .auto_trigger)
        redeem_status = try values.decodeIfPresent(Int.self, forKey: .redeem_status)
        uuid = try values.decodeIfPresent(String.self, forKey: .uuid)
        mobile_number = try values.decodeIfPresent(String.self, forKey: .mobile_number)
        offer_sent_on = try values.decodeIfPresent(String.self, forKey: .offer_sent_on)
        
        offer_redeem_expires_on = try values.decodeIfPresent(String.self, forKey: .offer_redeem_expires_on)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        organisation_id = try values.decodeIfPresent(String.self, forKey: .organisation_id)
        lat = try values.decodeIfPresent(String.self, forKey: .lat)
        lon = try values.decodeIfPresent(String.self, forKey: .lon)
        description = try values.decodeIfPresent(String.self, forKey: .description)
        image_path = try values.decodeIfPresent(String.self, forKey: .image_path)
        costperperson = try values.decodeIfPresent(String.self, forKey: .costperperson)
        newcustomer = try values.decodeIfPresent(String.self, forKey: .newcustomer)
        category = try values.decodeIfPresent(String.self, forKey: .category)
        webLink = try values.decodeIfPresent(String.self, forKey: .webLink)
        isAutoTriggerd = try values.decodeIfPresent(Int.self, forKey: .isAutoTriggerd)
        offer_sent_on = UTCToLocal(date: offer_sent_on ?? "2019-01-01 07:55:55")
        updated_on = timeStampToDateString(date: updated_on ?? "123456.9")
        
    }
    
    func UTCToLocal(date:String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        let dt: Date = dateFormatter.date(from: date)!
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "dd-MM-yyyy HH:mm:ss"
        
        return dateFormatter.string(from: dt)
    }
    func timeStampToDateString(date:String) -> String {
        let date = Date(timeIntervalSince1970: Double(date)!)
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = DateFormatter.Style.medium //Set time style
        dateFormatter.dateStyle = DateFormatter.Style.short //Set date style
        dateFormatter.dateFormat = "dd-MM-yyyy HH:mm:ss"
        dateFormatter.timeZone = TimeZone.current
        let localDate = dateFormatter.string(from: date)
        
        return localDate
    }
}

struct BrancheModel : Codable {
    let auth : Bool?
    let branchesList : [Branches]?
    
    enum CodingKeys: String, CodingKey {
        
        case auth = "auth"
        case branchesList = "data"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        auth = try values.decodeIfPresent(Bool.self, forKey: .auth)
        branchesList = try values.decodeIfPresent([Branches].self, forKey: .branchesList)
    }
    
}

struct Branches : Codable {
    let id : String?
    let organisationId : String?
    let name : String?
    let location : String?
    let gps_lat : String?
    let gps_long : String?
    let description : String?
    let image_path : String?
    let costperperson : String?
    let newcustomer : String?
     let created_on : String?
    let updated_on : String?
    enum CodingKeys: String, CodingKey {
        
        case id = "id"
        case organisationId = "organisation_id"
        case name = "name"
        case location = "location"
        case gps_lat = "gps_lat"
        case gps_long = "gps_long"
        case description = "description"
        case image_path = "image_path"
        case costperperson = "costperperson"
        case newcustomer = "newcustomer"
        case created_on = "created_on"
        case updated_on = "updated_on"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(String.self, forKey: .id)
        organisationId = try values.decodeIfPresent(String.self, forKey: .organisationId)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        location = try values.decodeIfPresent(String.self, forKey: .location)
        gps_lat = try values.decodeIfPresent(String.self, forKey: .gps_lat)
        gps_long = try values.decodeIfPresent(String.self, forKey: .gps_long)
        description = try values.decodeIfPresent(String.self, forKey: .description)
        image_path = try values.decodeIfPresent(String.self, forKey: .image_path)
        costperperson = try values.decodeIfPresent(String.self, forKey: .costperperson)
        newcustomer = try values.decodeIfPresent(String.self, forKey: .newcustomer)
        created_on = try values.decodeIfPresent(String.self, forKey: .created_on)
        updated_on = try values.decodeIfPresent(String.self, forKey: .updated_on)
    }
    
}
struct ErrorModel : Codable {
    let error : Bool?
    let message : String?
    
    enum CodingKeys: String, CodingKey {
        
        case error = "error"
        case message = "message"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        error = try values.decodeIfPresent(Bool.self, forKey: .error)
        message = try values.decodeIfPresent(String.self, forKey: .message)
    }
    
}

struct QuizQuessModel: Codable{
    let auth : Bool?
    let data : [quizData]?
    
    enum CodingKeys: String, CodingKey {
        
        case auth = "auth"
        case data = "data"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        auth = try values.decodeIfPresent(Bool.self, forKey: .auth)
        data = try values.decodeIfPresent([quizData].self, forKey: .data)
    }
}
struct quizData : Codable {
    let game_id : String?
    let quizList : [QuesObj]?
    
    enum CodingKeys: String, CodingKey {
        
        case game_id = "game_id"
        case quizList = "quiz"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        game_id = try values.decodeIfPresent(String.self, forKey: .game_id)
        quizList = try values.decodeIfPresent([QuesObj].self, forKey: .quizList)
    }
    
}

struct QuesObj : Codable {
    let question : String?
    let option_a : String?
    let option_b : String?
    let option_c : String?
    let option_d : String?
    let answer : String?
    
    enum CodingKeys: String, CodingKey {
        case question = "question"
        case option_a = "option_a"
        case option_b = "option_b"
        case option_c = "option_c"
        case option_d = "option_d"
        case answer = "answer"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        question = try values.decodeIfPresent(String.self, forKey: .question)
        option_a = try values.decodeIfPresent(String.self, forKey: .option_a)
        option_b = try values.decodeIfPresent(String.self, forKey: .option_b)
        option_c = try values.decodeIfPresent(String.self, forKey: .option_c)
        option_d = try values.decodeIfPresent(String.self, forKey: .option_d)
        answer = try values.decodeIfPresent(String.self, forKey: .answer)
    }
    
}


//MARK:- THModel Model
struct THModel : Codable {
    let auth : Bool?
    let data : [THData]?
    
    enum CodingKeys: String, CodingKey {
        
        case auth = "auth"
        case data = "data"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        auth = try values.decodeIfPresent(Bool.self, forKey: .auth)
        data = try values.decodeIfPresent([THData].self, forKey: .data)
    }
    
}
struct THData : Codable {
    let gameList : [THGame]?
    
    enum CodingKeys: String, CodingKey {
        
        case gameList = "treasurehunt"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        gameList = try values.decodeIfPresent([THGame].self, forKey: .gameList)
    }
    
}
// Mark:- Treasure Hunt Game Model
struct THGame : Codable {
    
    let game_id       : Int?
    let beacon_name       : String?
    let question      : String?
    let description   : String?
    let clue          : String?
    let clue_distance : Int?
    let winning_msg   : String?
    let loosing_msg   : String?
    let created_on     : String?
    let updated_on         : String?
    let outlet_id         : String?
    
    enum CodingKeys: String, CodingKey {
        
        case game_id        = "id"
        case beacon_name        = "beacon_name"
        case question       = "question"
        case description    = "description"
        case clue           = "clue"
        case clue_distance  = "clue_distance"
        case winning_msg    = "winning_msg"
        case loosing_msg    = "loosing_msg"
        case created_on      = "created_on"
        case updated_on          = "updated_on"
        case outlet_id          = "outlet_id"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        game_id         = try values.decodeIfPresent(Int.self, forKey: .game_id)
        beacon_name         = try values.decodeIfPresent(String.self, forKey: .beacon_name)
        question        = try values.decodeIfPresent(String.self, forKey: .question)
        description  = try values.decodeIfPresent(String.self, forKey: .description)
        clue               = try values.decodeIfPresent(String.self, forKey: .clue)
        clue_distance    = try values.decodeIfPresent(Int.self, forKey: .clue_distance)
        winning_msg      = try values.decodeIfPresent(String.self, forKey: .winning_msg)
        loosing_msg = try values.decodeIfPresent(String.self, forKey: .loosing_msg)
        created_on = try values.decodeIfPresent(String.self, forKey: .created_on)
        updated_on = try values.decodeIfPresent(String.self, forKey: .updated_on)
        outlet_id = try values.decodeIfPresent(String.self, forKey: .outlet_id)
        
    }
    
}

//MARK:- Online User Model
/*struct OnlineUserModel : Codable {
    let onlineUserList : [OnlineUser]?
    
    enum CodingKeys: String, CodingKey {
        
        case onlineUserList = "data"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        //onlineUserList = try values.decodeIfPresent([OnlineUser].self, forKey: .onlineUserList)
    }
    
}*/
// Mark:- Online User Model
class OnlineUser : NSObject {
    
    var uuid  = String()
    var name          = String()
    var outlet_id     = String()
    var socket_id     = String()
    var dnd_status:     Int?
    var msgCount:       Int?
    var outletName: String?
    
    override init() {
        super.init()
    }
    
    init(fromDictionary dict: NSDictionary) {
        
        self.uuid = dict["uuid"] as? String ?? ""
        self.name = dict["name"] as? String ?? ""
        self.outlet_id = dict["uoid"] as? String ?? ""
        self.socket_id = dict["socket_id"] as? String ?? ""
        self.dnd_status = dict["dnd_status"] as? Int ?? 0
        self.msgCount = dict["msgCount"] as? Int ?? 0
       
    }
    
    /*enum CodingKeys: String, CodingKey {
        
        case access_token    = "access-token"
        case name            = "dnd_status"
        case outlet_id       = "name"
        case socket_id       = "outlet_id"
        case dnd_status      = "socket_id"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        access_token     = try values.decodeIfPresent(String.self, forKey: .access_token)
        name             = try values.decodeIfPresent(String.self, forKey: .name)
        outlet_id     = try values.decodeIfPresent(String.self, forKey: .outlet_id)
        socket_id           = try values.decodeIfPresent(String.self, forKey: .socket_id)
        dnd_status        = try values.decodeIfPresent(Int.self, forKey: .dnd_status)
        
        
    }*/
    
}
//MARK:- Offers Model
struct NotificationModel : Codable {
    let auth : Bool?
    let notificationList : [Notification]?
    
    enum CodingKeys: String, CodingKey {
        
        case auth = "error"
        case notificationList = "data"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        auth = try values.decodeIfPresent(Bool.self, forKey: .auth)
        notificationList = try values.decodeIfPresent([Notification].self, forKey: .notificationList)
    }
    
}
// Mark:- Treasure Hunt Game Model
struct Notification : Codable {
    
    let description         : String?
    let id                  : Int?
    let image_url           : String?
    let web_url           : String?
    let notification_target : String?
    let send_timestamp      : String?
    let title               : String?
    let user_id             : String?
    
    
    enum CodingKeys: String, CodingKey {
        
        case description          = "description"
        case id                   = "id"
        case image_url            = "image_url"
        case notification_target  = "notification_target"
        case send_timestamp       = "send_timestamp"
        case title                = "title"
        case user_id              = "user_id"
        case web_url              = "web_link"
       
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        description             = try values.decodeIfPresent(String.self, forKey: .description)
        id                      = try values.decodeIfPresent(Int.self, forKey: .id)
        image_url            = try values.decodeIfPresent(String.self, forKey: .image_url)
        notification_target        = try values.decodeIfPresent(String.self, forKey: .notification_target)
        send_timestamp           = try values.decodeIfPresent(String.self, forKey: .send_timestamp)
        title               = try values.decodeIfPresent(String.self, forKey: .title)
        user_id             = try values.decodeIfPresent(String.self, forKey: .user_id)
        web_url             = try values.decodeIfPresent(String.self, forKey: .web_url)
        
       
        
    }
    
}
