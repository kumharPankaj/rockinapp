//
//  Common.swift
//  Rockinap
//
//  Created by orionspica on 02/10/18.
//  Copyright © 2018 funndynamix. All rights reserved.
//

import UIKit
import Firebase

struct Constants
{
    struct refs
    {
        static let databaseRoot = Database.database().reference()
        static let databaseChats = databaseRoot.child("Rockinap")
    }
}
var textFieldRealYPosition: CGFloat = 0.0
let ACCEPTABLE_CHAR_EMAIL = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789- (),.@"
let ACCEPTABLE_CHAR = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789- (),"
var selectedCategory = ""
class Common {
    private static var sharedManager: Common =
    {
        let tempSharedManager = Common.init()
        
        return tempSharedManager
    }()
    
    class var shared: Common
    {
        return sharedManager
    }
    


}

public typealias WebCallbackHandler = (_ status :Bool,  _ data:AnyObject?)  -> Void

extension UIColor {
    //var color1 = hexStringToUIColor("#d3d3d3")
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        if ((cString.count) != 6) {
            return UIColor.black
        }
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    convenience init?(hexString: String) {
        /*       guard let hex = hexString.hex else {
         return nil
         }
         self.init(hex: hex)*/
        let hexString:String = hexString.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        
        let scanner = Scanner(string: hexString as String)
        
        if (hexString.hasPrefix("#")) {
            scanner.scanLocation = 1
        }
        var color:UInt32 = 0
        scanner.scanHexInt32(&color)
        
        let mask = 0x000000FF
        let r = Int(color >> 16) & mask
        let g = Int(color >> 8) & mask
        let b = Int(color) & mask
        
        let red   = CGFloat(r) / 255.0
        let green = CGFloat(g) / 255.0
        let blue  = CGFloat(b) / 255.0
        
        self.init(red:red, green:green, blue:blue, alpha:1)
    }
}

struct ColorCodes  {
    
    static  let color_FCB315 = "#FCB315"
    static  let  color_c3a863 = "#c3a863"
    static  let  color_000000 = "#000000"
    static  let  color_e0e0e0 = "#e0e0e0"
    static let  color_5380D5 = "#5380D5"
    static let color_Out_Premise_Landing_Blue = "299FC0"
}
struct OutleCategory {
    static  let NightinOut = "nightin out"
    static  let  EatinOut = "eatin out"
    static  let  ChilinOut = "chilin out"
    static  let  ShopinOut = "shopin out"
}
enum API_PARAM_KEYS: String {
    case CATEGORY = "category"
    case ORGANISATION_ID = "organisation_id"
}
enum LINE_POSITION {
    case LINE_POSITION_TOP
    case LINE_POSITION_BOTTOM
}
extension UIImage {
    func trim(trimRect :CGRect) -> UIImage {
        if CGRect(origin: CGPoint.zero, size: self.size).contains(trimRect) {
            if let imageRef = self.cgImage?.cropping(to: trimRect) {
                return UIImage(cgImage: imageRef)
            }
        }
        
        UIGraphicsBeginImageContextWithOptions(trimRect.size, true, self.scale)
        self.draw(in: CGRect(x: -trimRect.minX, y: -trimRect.minY, width: self.size.width, height: self.size.height))
        let trimmedImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        guard let image = trimmedImage else { return self }
        
        return image
    }
    
    public func maskWithColor(color: UIColor) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale)
        let context = UIGraphicsGetCurrentContext()!
        let rect = CGRect(origin: CGPoint.zero, size: size)
        color.setFill()
        self.draw(in: rect)
        context.setBlendMode(.sourceIn)
        context.fill(rect)
        let resultImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return resultImage
    }
    func resize(_ image: UIImage,factor:Float) -> UIImage {
        var actualHeight = Float(image.size.height)
        var actualWidth = Float(image.size.width)
        let maxHeight: Float = 10*factor
        let maxWidth: Float = 10*factor
        var imgRatio: Float = actualWidth / actualHeight
        let maxRatio: Float = maxWidth / maxHeight
        let compressionQuality: Float = 0.0
        //50 percent compression
        if actualHeight > maxHeight || actualWidth > maxWidth {
            if imgRatio < maxRatio {
                //adjust width according to maxHeight
                imgRatio = maxHeight / actualHeight
                actualWidth = imgRatio * actualWidth
                actualHeight = maxHeight
            }
            else if imgRatio > maxRatio {
                //adjust height according to maxWidth
                imgRatio = maxWidth / actualWidth
                actualHeight = imgRatio * actualHeight
                actualWidth = maxWidth
            }
            else {
                actualHeight = maxHeight
                actualWidth = maxWidth
            }
        }
        let rect = CGRect(x: 0.0, y: 0.0, width: CGFloat(actualWidth), height: CGFloat(actualHeight))
        UIGraphicsBeginImageContext(rect.size)
        image.draw(in: rect)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        let imageData = img!.jpegData(compressionQuality: CGFloat(compressionQuality))
        UIGraphicsEndImageContext()
        return UIImage(data: imageData!) ?? UIImage()
    }
    public func maskWithColorNew(color: UIColor) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale)
        let context = UIGraphicsGetCurrentContext()!
        let rect = CGRect(origin: CGPoint.zero, size: size)
        let rectSizeNew = CGSize.init(width: size.width/2, height: size.height)
        let rectNew = CGRect(origin: CGPoint.zero, size: rectSizeNew)
        color.setFill()
        self.draw(in: rect)
        context.setBlendMode(.sourceIn)
        context.fill(rectNew)
        let resultImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return resultImage
    }
}
extension UIView {
    
    // OUTPUT 1
    func dropShadow(scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.5
        layer.shadowOffset = CGSize(width: -1, height: 1)
        layer.shadowRadius = 1
        
        layer.shadowPath = UIBezierPath(rect: bounds).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    
    // OUTPUT 2
    func dropShadow(color: UIColor, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 1, scale: Bool = true,cellWidth: Int) {
        self.layoutSubviews()
        layer.masksToBounds = false
        layer.shadowColor = color.cgColor
        layer.shadowOpacity = opacity
        layer.shadowOffset = offSet
        layer.shadowRadius = radius
        self.setNeedsLayout()
        self.layoutIfNeeded()
        let constant = Int(radius)
        layer.shadowPath = UIBezierPath.init(roundedRect: CGRect.init(x: -constant/2, y: -constant/2, width: cellWidth - 5, height: Int(self.frame.height) + constant*2), cornerRadius: 10).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
        
   
        
    }
    
    func viewBottomShadow() {
        // Shadow and Radius
        layer.shadowColor = UIColor.darkGray.cgColor
        layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        layer.shadowOpacity = 1.0
        layer.shadowRadius = 0.0
        layer.masksToBounds = false
    }
    
    func addRatingStar(uptoCount : Double) -> NSMutableAttributedString {
        let starCount = NSMutableAttributedString()
        
        for i in stride(from: 1.0, to: 6.0, by: 1.0) {
            if uptoCount < i {
                let (wholePart, fractionalPart) = modf(uptoCount)
                let imgAttachment = NSTextAttachment()
                if i == (wholePart + 1) && (fractionalPart != 0.0){
                    imgAttachment.image = UIImage.init(named: "star_raing")?.maskWithColor(color: UIColor.white).maskWithColorNew(color: UIColor.orange)
                }
                else{
                    imgAttachment.image = UIImage.init(named: "star_raing")?.maskWithColor(color: UIColor.init(white: 1, alpha: 0.5))
                }
                let attachmentString:NSAttributedString = NSAttributedString(attachment: imgAttachment)
                starCount.append(attachmentString)
            }
            else{
                let imgAttachment = NSTextAttachment()
                imgAttachment.image = UIImage.init(named: "star_raing")?.maskWithColor(color: UIColor.orange)
                let attachmentString:NSAttributedString = NSAttributedString(attachment: imgAttachment)
                starCount.append(attachmentString)
            }
        }
        
        return starCount
    }
}
public extension UIImage
{
    public convenience init?(color: UIColor, size: CGSize = CGSize(width: 1, height: 1))
    {
        let rect = CGRect(origin: .zero, size: size)
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
        color.setFill()
        UIRectFill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        guard let cgImage = image?.cgImage else { return nil }
        self.init(cgImage: cgImage)
    }
    
    func getImageWithColor(color: UIColor) -> UIImage
    {
        let tintableImage = self.withRenderingMode(.alwaysTemplate)
        return tintableImage
    }
}
struct defaultsKeys {
    static var socialMediaType = "socialMediaType"
    static var fcmToken = "fcmToken"
    static var localDbPath = "dbPath"
    static var keepSigned = "SignStatus"
    
}
