//
//  AppDelegate.swift
//  Rockinap
//
//  Created by orionspica on 30/09/18.
//  Copyright © 2018 funndynamix. All rights reserved.
//

import UIKit
import Firebase
import GoogleSignIn
import UserNotifications
@objcMembers
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var transactionDetails:TransactionDetails?
    var menuItems:[Menu]?
    var tempNavigationViewController: UINavigationController?
    let gcmMessageIDKey = "gcm.message_id"
    var isUserInpremise = false
    var liveBranchId = ""
    var currentUserId = ""
    var detectedBeaconName = ""
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
       FirebaseApp.configure()
        GIDSignIn.sharedInstance().clientID = "988301453825-3933i1i78btbpbtbdsbqfkp8lso0lc39.apps.googleusercontent.com"
        //getJsonObjet()
        getMenuJsonObjet()
        self.configurePushNotification(application)
        
        
        
        tempNavigationViewController = DrawerRootViewController.shared().loadViewControllerFromStoryBoard("RootNavigatorVC") as? UINavigationController
        DrawerRootViewController.shared().updateNavigationBarStatus(true)
        
        if ((UserDefaults.standard.bool(forKey: defaultsKeys.keepSigned))) {
            if let userInformation = UserDefaults.standard.dictionary(forKey: "userInformation") {
                
                let VC = DrawerRootViewController.shared().setupDrawerController()
                self.window?.rootViewController = VC
            }
            else
            {
                self.window?.rootViewController = tempNavigationViewController
            }
        } else {
             self.window?.rootViewController = tempNavigationViewController
        }
        /*if let userInformation = UserDefaults.standard.dictionary(forKey: "userInformation") {
            
            let VC = DrawerRootViewController.shared().setupDrawerController()
            self.window?.rootViewController = VC
        }
        else
        {
            self.window?.rootViewController = tempNavigationViewController
        }*/
        
        self.window?.makeKeyAndVisible()
        
        //RootNavigatorVC
        return FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
    }

    func AddLoginAsRootVC()
    {
        self.window?.rootViewController =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "RootNavigatorVC") as! UINavigationController
    }
    
    func AddHomeAsRootVC()
    {
        self.window?.rootViewController =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HomeNavigationController") as! UINavigationController
    }
    
    class func shareddelegate() -> AppDelegate
    {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    func getViewController(identifre:String! = "") -> UIViewController
    {
        return  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: identifre)
        
    }
    
    
    func getJsonObjet()
    {
        if let path = Bundle.main.path(forResource: "tranactiondetails", ofType: "txt") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                transactionDetails = TransactionDetails.init(fromDictionary: jsonResult as! [String : Any])
            }
            catch
            {
                // handle error
            }
        }
    }
    
    
    func getMenuJsonObjet()
    {
        if let path = Bundle.main.path(forResource: "menu", ofType: "txt") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonDecoder = JSONDecoder()
                menuItems = try jsonDecoder.decode([Menu].self, from: data)
            }
            catch
            {
                // handle error
            }
        }
    }
    
    
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        
        if isUserInpremise {
            var userID = ""
            if let userInformation = UserDefaults.standard.dictionary(forKey: "userInformation") {
                userID = userInformation["mobile_number"] as! String
            }
            SocketIOManager.sharedInstance.closeConnection()
            APIFunctions.leaveOutlet(dictParams: ["outlet_id":liveBranchId, "mobile_number":userID]) { (status, obj) in
                    
            }
        }
        
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        if isUserInpremise {
            addCurrentUserCall()
        }
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
        
    }
    private func addCurrentUserCall()  {
        if let userInformation = UserDefaults.standard.dictionary(forKey: "userInformation") {
            let userID = userInformation["mobile_number"] as! String
            let uuid = userInformation["userID"] as! String
            APIFunctions.addInCurrentUser(dictParams: ["uoid":liveBranchId,"uuid": uuid, "mobile_number":userID, "beacon_name":detectedBeaconName]) { (status, obj) in
               
            }
        }
    }
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


    //MARK: - Open URl Method for Facebook,Google Login
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        
        GIDSignIn.sharedInstance().handle(url, sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String, annotation: options[UIApplication.OpenURLOptionsKey.annotation])
        FBSDKApplicationDelegate.sharedInstance().application(app, open: url, sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String, annotation: options[UIApplication.OpenURLOptionsKey.annotation])
        
        return true
    }
    
    func setNotificationToken(_ status:String)
    {
        let usD =  UserDefaults.standard
        usD.set(status, forKey: "NotificationsToken")
        usD.synchronize()
    }
    
    var notificationsToken:String?
    {
        let usD =  UserDefaults.standard
        return usD.string(forKey: "NotificationsToken")
    }
    
    func configurePushNotification(_ application:UIApplication)   {
        
        Messaging.messaging().delegate = self
        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        
        let token = Messaging.messaging().fcmToken
        print("FCM token: \(token ?? "")")
        DrawerRootViewController.shared().fcm_token = token
        if((token) != nil)
        {
            self.setNotificationToken(token!)
        }
        
        
    }
    
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String)
    {
        print("Firebase didRefreshRegistrationToken: \(fcmToken)")
        DrawerRootViewController.shared().fcm_token = fcmToken
        self.setNotificationToken(fcmToken)
        
    }
    func application(_ application: UIApplication,
                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Messaging.messaging().apnsToken = deviceToken
        let token = Messaging.messaging().fcmToken
        DrawerRootViewController.shared().fcm_token = token
        
        print("didRegisterForRemoteNotificationsWithDeviceToken: \(token)")
        guard token != nil else {
            return
        }
        callRefreshToken(token: token ?? "")
        self.setNotificationToken(token!)
        //self.refToken.child("token").setValue(token)
    }
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Unable to register for remote notifications: \(error.localizedDescription)")
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        Messaging.messaging().appDidReceiveMessage(userInfo)
        
        let userInfotemp = userInfo as? NSDictionary
        //handlePushNotification(userInfotemp)
        
        completionHandler(UIBackgroundFetchResult.newData)
    }
    
    // [START receive_message]
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        Messaging.messaging().appDidReceiveMessage(userInfo)
        
        let userInfotemp = userInfo as? NSDictionary
        //handlePushNotification(userInfotemp)
    }
    private func callRefreshToken(token: String) {
        let id:String = ""
        if ((UserDefaults.standard.bool(forKey: defaultsKeys.keepSigned))) {
            if let userInformation = UserDefaults.standard.dictionary(forKey: "userInformation") {
                APIFunctions.refresNotificationToken(dictParams: ["refresh_token":token,
                                                                  "uuid":userInformation["userID"] as! String,
                                                                  "device_type":"ios",
                                                                  "device_id":id.getUUID() ?? "",
                                                                  "mobile_number":userInformation["mobile_number"] as! String]) { (status, resObj) in
                                                                    
                                                                    
                }
            }
        } else {
            APIFunctions.refresNotificationToken(dictParams: ["refresh_token":token,
                                                              "device_type":"ios",
                                                              "device_id":id.getUUID() ?? ""]){ (status, resObj) in
                                                                
                                                                
            }
            
                                                                
        }
        

    }
}

// [START ios_10_message_handling]
@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter,  willPresent notification: UNNotification, withCompletionHandler   completionHandler: @escaping (_ options:   UNNotificationPresentationOptions) -> Void) {
        
        let userInfo = notification.request.content.userInfo
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        Messaging.messaging().appDidReceiveMessage(userInfo)
        
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey]
        {
            print("Message ID: \(messageID)")
        }
        completionHandler([.alert, .badge, .sound])
        
        // custom code to handle push while app is in the foreground
        //        handleNotificationPush()
        print("Handle push from foreground\(notification.request.content.userInfo)")
    }
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        let userInfotemp = userInfo as? NSDictionary
        //handlePushNotification(userInfotemp)
         completionHandler()
    }
   
    
}
extension AppDelegate : MessagingDelegate {
    // [START refresh_token]
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        
        UserDefaults.standard.set(fcmToken, forKey: defaultsKeys.fcmToken)
        UserDefaults.standard.synchronize()
        callRefreshToken(token: fcmToken)
        /*UserDefaults.standard.set(fcmToken, forKey: defaultsKeys.fcmToken)
         UserDefaults.standard.synchronize()*/
        // TODO: If necessary send token to application server.
        // Note: This callback is fired at each app startup and whenever a new token is generated.
    }
    // [END refresh_token]
    // [START ios_10_data_message]
    // Receive data messages on iOS 10+ directly from FCM (bypassing APNs) when the app is in the foreground.
    // To enable direct data messages, you can set Messaging.messaging().shouldEstablishDirectChannel to true.
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("Received data message: \(remoteMessage.appData)")
    }
    // [END ios_10_data_message]
}
